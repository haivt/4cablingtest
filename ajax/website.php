<?php
include("ajax/config.php");
header('Content-Type:text/html; charset=UTF-8');
session_start();
date_default_timezone_set('Europe/London');
include_once("ajax/includes/api.class.php");
set_include_path("ajax/includes/");
include_once("ajax/includes/Zend/Soap/Client.php");

$api = new api("http://".$base_url."/api/soap/?wsdl", "justcables", "bfsc7w6y", $cache_folder = "ajax/cache/", false);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="initial-scale=0.65, maximum-scale=0.65, user-scalable=no" name="viewport" >
<title>Just Cables</title>
<link rel="stylesheet" type="text/css" href="/ajax/skin/justcables/css/style.css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script src="/ajax/js/jquery.json-2.2.min.js" type="text/javascript"></script>
<script src="/ajax/js/jquery.url.js" type="text/javascript"></script>
<script src="/ajax/js/jquery.ddslick.min.js" type="text/javascript"></script>
<script type="text/javascript">
var base_url = '<?php echo $base_url; ?>';
var categories = <?php echo json_encode($api->getCategoryWithDetails()) ?>; 

</script>
<script src="/ajax/js/main.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">
    	<a id="anchor-home"></a>
    	<div id="header">
            <div id="cart" style="display: none;">
                <div class="cart-content">
                    <div class="cart-table">
                        <div class="row cart-table-head">
                            <div class="co1"> &nbsp; </div>
                            <div class="co2">Product Details</div>
                            <div class="co2">Price</div>
                            <div class="co2">Quantity</div>
                            <div class="co1">Subtotal</div>
                            <div class="co2">Remove</div>
                        </div>
                        <div id="cartcontent">
                        </div>
                        <div class="row cart-table-buttons">
                            <div class="co1"> &nbsp; </div>
                            <div class="co2"> &nbsp; </div>
                            <div class="co2"> &nbsp; </div>
                            <div class="co2"> <input type="button" value="Update Cart" id="cart-update" style="width:73%;" /> </div>
                            <div class="co3"></div>
                        </div>
                        <div class="row cart-table-subtotal">
                            <div class="co3"> &nbsp; </div>
                            <div class="co2">Total</div>
                            <div class="co2">Tax</div>
                            <div class="co3">Grand Total</div>
                        </div>
                        <div id="cartcontent-sub">
                            <div class="row">
                                <div class="co3"> &nbsp; </div>
                                <div class="co2" id="cart_total">$199.98</div>
                                <div class="co2" id="cart_tax">$9.99</div>
                                <div class="co3" id="cart_grand_total">$219.98</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="co6"> &nbsp; </div>
                            <div class="co3"><input type="button" value="Checkout Securely" id="cart-checkout" style="width:100%;" /></div>
                            <div class="co1"> &nbsp; </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="account" style="display: none;">
                <div class="account-content">
                   <div class="account-table">
                        <div class="row account-table-head">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter">New Customers</div>
                            <div class="co2-m aligncenter">Registered Customers</div>
                            <div class="co2-m aligncenter">Newsletter</div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                        <div class="row account-table-buttons">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter"> <input type="button" value="Create an account" id="account-create" style="width:100%;" /> </div>
                            <div class="co2-m aligncenter"><input id="account-email" name="account-email" type="text" value="Email" style="width:98%;" /> </div>
                            <div class="co2-m aligncenter"> <input id="newsletter-name" name="newsletter-name" type="text" value="Name" style="width:98%;" /> </div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                        <div class="row account-table-buttons">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter"> &nbsp; </div>
                            <div class="co2-m aligncenter"><input id="account-password" name="account-password" type="password" value="" style="width:98%;" /> </div>
                            <div class="co2-m aligncenter"> <input id="newsletter-email" name="newsletter-email" type="text" value="Email" style="width:98%;" /> </div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                        <div class="row account-table-buttons">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter"> &nbsp; </div>
                            <div class="co2-m aligncenter"> <input type="button" value="Submit" id="do-account-login" style="width:100%;" />  </div>
                            <div class="co2-m aligncenter"> <input type="button" value="Submit" id="newsletter-signup" style="width:100%;" /> </div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                    </div>
                </div>
			</div>
            <div id="newaccount" style="display: none;">
                <div class="account-content">
                   <div class="account-table">
                        <div class="row account-table-head">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter">&nbsp;</div>
                            <div class="co2-m aligncenter">Personal Information</div>
                            <div class="co2-m aligncenter">Account Information</div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                        <div class="row account-table-buttons">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter">Please Complete This Information </div>
                            <div class="co2-m aligncenter"><input id="new-account-first-name" name="new-account-first-name" type="text" value="First Name" style="width:98%;" /> </div>
                            <div class="co2-m aligncenter"><input id="new-account-password" name="new-account-password" type="password" value="" style="width:98%;" /> </div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                        <div class="row account-table-buttons">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter">To Register For An Account</div>
                            <div class="co2-m aligncenter"><input id="new-account-last-name" name="new-account-last-name" type="text" value="Last Name" style="width:98%;" /> </div>
                            <div class="co2-m aligncenter"><input id="new-account-password-v" name="new-account-password-v" type="password" value="" style="width:98%;" /> </div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                        <div class="row account-table-buttons">
                            <div class="co2"> &nbsp; </div>
                            <div class="co2-m aligncenter"> &nbsp; </div>
                            <div class="co2-m aligncenter"><input id="new-account-email" name="new-account-email" type="text" value="Email" style="width:98%;" /> </div>
                            <div class="co2-m aligncenter"><input type="button" value="Submit" id="new-account-submit" style="width:100%;" /> </div>
                            <div class="co2"> &nbsp; </div>
                        </div>
                    </div>
                </div>
			</div>
            <div id="search" style="display: none;">
                <div class="search-content">
	                search-content
                </div>
			</div>
            <div class="responsive-block-lt-320" style="display:none">
            	<div class="header-close-bar" style="display:none">
                    <a class="logo-wrapper" href="#/home" style="margin-left:85px;">
                        <img id="logo" src="ajax/skin/justcables/images/logo.png" width="100%" />
                    </a>
                    <a class="headerclose">
                        <span class="triangle"></span>
                        <span class="label">
                            <span class="title">Close</span>	                        
                            <span class="small-triangle"></span>
                        </span>
                    </a>
                </div>
            	<div class="header-top-bar">
                    <a class="logo-wrapper" href="#/home">
                        <img id="logo" src="ajax/skin/justcables/images/logo.png" width="100%" />
                    </a>
                    
                    <a class="topback" style="visibility:hidden">
                        <span class="triangle"></span>
                        <span class="label">
                            <span class="title">Back</span>	                        
                            <span class="small-triangle"></span>
                        </span>
                    </a>
                    <div class="menu">
                        <?php /* ?>
                        <a class="headerIcon search-toggle">SEARCH</a>
                        <a class="headerIcon account-toggle">SIGN IN</a>
                        <?php */ ?>
                        <a class="headerIcon cart-toggle">VIEW CART</a>
                    </div>
                </div>
            </div>
    
            <div class="responsive-block-bt-320-600" style="display:none">
            	<div class="header-close-bar" style="display:none">
                    <a class="logo-wrapper" href="#/home" style="margin-left:85px;">
                        <img id="logo" src="ajax/skin/justcables/images/logo.png" width="100%" />
                    </a>
                    <a class="headerclose">
                        <span class="triangle"></span>
                        <span class="label">
                            <span class="title">Close</span>	                        
                            <span class="small-triangle"></span>
                        </span>
                    </a>
                </div>
            	<div class="header-top-bar">
                    <a class="logo-wrapper" href="#/home">
                        <img id="logo" src="ajax/skin/justcables/images/logo.png" width="100%" />
                    </a>
                    <div class="menu">
                    
                        <a class="headerIcon floatright account-toggle" id="topSignIn">
                            <span class="icon">
                                <span class="label">
                                    <span class="title">Sign In</span>	                        
                                    <span class="small-revtriangle"></span>
                                </span>
                                <span class="revtriangle"></span>
                            </span>
                        </a>
            
            
                        <a class="headerIcon floatright cart-toggle" id="topCart">
                            <span class="icon">
                                <span class="label">
                                    <span class="title">View Cart</span>	                        
                                    <span class="small-revtriangle"></span>
                                </span>
                                <span class="revtriangle"></span>
                            </span>
                        </a>
                        
                        <a class="headerIcon floatright footer-toggle" id="bottomFooter">
                            <span class="icon">
                                <span class="label">
                                    <span class="title">Contact</span>	                        
                                    <span class="small-revtriangle"></span>
                                </span>
                                <span class="revtriangle"></span>
                            </span>
                        </a>

                    
                        <a class="topback" style="visibility:hidden">
                            <span class="triangle"></span>
                            <span class="label">
                                <span class="title">Back</span>	                        
                                <span class="small-triangle"></span>
                            </span>
                        </a>
						<?php /* ?>
                        <a class="headerIcon search-toggle">SEARCH</a>
                        <a class="headerIcon account-toggle">SIGN IN</a>
                        <a class="headerIcon cart-toggle" href="#cart">View Cart</a>
                        <?php */ ?>

                        
                    </div>
                </div>
            </div>
        	<div class="responsive-block-gt-600">
            	<div class="header-close-bar" style="display:none">
                    <a class="logo-wrapper" href="#/home" style="margin-left:85px;">
                        <img id="logo" src="ajax/skin/justcables/images/logo.png" width="100%" />
                    </a>
                    <a class="headerclose">
                        <span class="triangle"></span>
                        <span class="label">
                            <span class="title">Close</span>	                        
                            <span class="small-triangle"></span>
                        </span>
                    </a>
                </div>
            	<div class="header-top-bar">
                    <a class="headerIcon floatleft search-toggle" id="topSearch">
                        <span class="icon">
                            <span class="label">
                                <span class="title">Search</span>
                                <span class="small-revtriangle"></span>
                            </span>
                            <span class="revtriangle"></span>
                        </span>
                    </a>
        
                    <a class="logo-wrapper" href="#/home">
                        <img id="logo" src="ajax/skin/justcables/images/logo.png" width="100%" />
                    </a>
                    
                        <a class="topback" style="visibility:hidden">
                            <span class="triangle"></span>
                            <span class="label">
                                <span class="title">Back</span>	                        
                                <span class="small-triangle"></span>
                            </span>
                        </a>
                    
                    
                    <a class="headerIcon floatright account-toggle" id="topSignIn">
                        <span class="icon">
                            <span class="label">
                                <span class="title">Sign In</span>	                        
                                <span class="small-revtriangle"></span>
                            </span>
                            <span class="revtriangle"></span>
                        </span>
                    </a>
        
        
                    <a class="headerIcon floatright cart-toggle" id="topCart">
                        <span class="icon">
                            <span class="label">
                                <span class="title">View Cart</span>	                        
                                <span class="small-revtriangle"></span>
                            </span>
                            <span class="revtriangle"></span>
                        </span>
                    </a>
                    
                    <a class="headerIcon floatright footer-toggle" id="bottomFooter">
                        <span class="icon">
                            <span class="label">
                                <span class="title">Contact</span>	                        
                                <span class="small-revtriangle"></span>
                            </span>
                            <span class="revtriangle"></span>
                        </span>
                    </a>
                </div>
			</div>
        </div>
        <div class="content" id="homecontent">
        	<div class="maincategories">
            </div>
        <?php 
			/*
			<div class="main-category">
                <div class="image">
                    <img src="ajax/skin/justcables/images/category/cable-labels.png">
                </div>
                <a href="#/category/cable-labels/" class="url"><img width="100%" class="overlay" src="skin/justcables/images/product-overlay.png"></a>
                <div class="title">Audio Cables</div>
            </div>
			*/
		?>
        </div>
        <div class="content" id="subcategory">
            <a id="anchor-subcategory"></a>
            <div class="category-name" id="category-label">Category Name</div>
            <div id="subcategories-container">
            <?php
			/*
            for($i = 1; $i <= 6; $i++)
            {
                ?>
                <div class="sub-category">
                    
                    <div class="image">
                        <img src="skin/justcables/images/category/patch-lead.png" />
                    </div>
                    <a class="url" href="#/sub-category/<?php echo "sub-category-".$i ?>/"><img src="ajax/skin/justcables/images/product-overlay-2.png" class="overlay" width="100%" /></a>
                    <div class="title"><?php echo "Sub Category ".$i ?></div>
                </div>
                <?
            }
			*/
            ?>
            </div>
        </div>
        <div class="content" id="products">
            <a id="anchor-products-in-category"></a>
            <div class="category-name" id="sub-category-label">Category Name</div>
            <div id="filters">
                <div class="filter">
                    <select id="color" name="color" style='background-color: #1454A2;'>
                        <option value="">Colour</option>
                    </select>
                </div>
                <div class="filter">
                    <select id="price" name="price" style='background-color: #1454A2;'>
                        <option value="">Price</option>
                    </select>
                </div>
                <div class="filter">
                    <select id="manufacturer" name="manufacturer" style='background-color: #1454A2;'>
                        <option value="">Manufacturer</option>
                    </select>
                </div>
            </div>
            <div id="products-scroll-container">
            
                
                <input type="hidden" name="current-slide" id="current-slide" value="s_1" />
                <div class="slide-scroll">
                    <div class="slide-content" id="products-container">
                        <div class="slider" id="s_1">[ 1 ]</div>
                        <div class="slider" id="s_2">[ 2 ]</div>
                        <div class="slider" id="s_3">[ 3 ]</div>
                        <div class="slider" id="s_4">[ 4 ]</div>
                        <div class="slider" id="s_5">[ 5 ]</div>
                    </div>
                </div>
                
            
            	 <?php
                /*
                <div id="products-container">
                for($i = 1; $i <= 8; $i++)
                {
                    ?>
                    <div class="product-in-category">
                        <div class="image">
                            <img src="ajax/skin/justcables/images/product/1.jpg" />
                        </div>
                        <a class="url" href="#/product/<?php echo "product-".$i ?>/"><img src="ajax/skin/justcables/images/product-overlay-3.png" class="overlay" width="100%" /></a>
                        <div class="title"><?php echo "Product ".$i ?></div>
                        <div class="price"><?php echo '$<span class="ammount">'.$i.'.99</span> <span>ex GST</span>' ?></div>
                        <a class="addtocartC" href="#/addtocart/<?php echo "product-".$i ?>/">
                            <span class="addtocart">Add To Cart</span>
                        </a>
                    </div>
                    <?
                }
                </div>
                 */
                ?>
            </div>
            <input type="hidden" name="current-slide" id="current-slide" value="s_1" />
            <a class="controller" id="slide-prev"> &nbsp; </a>
            <a class="controller" id="slide-next"> &nbsp; </a>

        </div>
        <div class="content" id="product">
        <a id="anchor-products-page"></a>
			<div class="product">
            	<div class="product-left">
                
                    <div class="product-image">
                        <div class="image">
                            <img src="" width="100%">
                        </div>
                        <a class="zoom"><img width="100%" class="overlay" src="ajax/skin/justcables/images/product-overlay-productpage.png"></a>
                    </div>
					<div class="selector" id="qty">
                    	<a class="left"></a>
                        <div class="current">Quantity <strong class="amount">1</strong></div>
                    	<a class="right"></a>
                    </div>
                    <?php /* ?>
                	<ul class="product-config">
	                    <li>Quantity</li>
	                    <li>Length</li>
	                    <li>Colour</li>
	                    <li>Gender</li>
                    </ul>
                    <?php */ ?>
                </div>
            	<div class="product-right">
	                <h3 class="product-title">Product Title</h3>
                	<div class="product-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque augue neque, egestas non fermentum non, molestie ac nunc. Duis lobortis scelerisque nisi, non sodales mauris sollicitudin a. Donec enim tortor, faucibus a suscipit eget, egestas eget massa. Cras sem dolor, rutrum sed tristique sed, bibendum ac augue. Etiam porta fringilla nisi, a rutrum neque auctor a. Aliquam sagittis rhoncus ipsum ac dapibus. Maecenas sed mauris a nulla pretium sodales eu nec lacus. Aenean ut ante nisl. Quisque accumsan interdum rutrum. Suspendisse felis est, pulvinar vel bibendum eget, placerat non leo. Curabitur neque dui, gravida blandit vehicula et, tristique feugiat elit. </div>
                	<div class="product-price">$<span class="ammount">99.99</span> <span class="ext">ex GST</span></div>
                	<a class="product-add-to-cart">
                    	<span>Add To Cart</span>
                    </a>
					<input type="hidden" class="product_id" id="current_product_id" value="0" />
                </div>
    	    </div>
        </div>
        
        <div id="product-related" class="relatedcontent" style="display:none">
        	<div class="related-products-container">
            </div>
        </div>
        
        <div id="footer" style="display:none">
        
            <div id="footer-content">
                <div class="footer-left">
                    <h3>
                        Free Next Day<br />
                        Australian Metropolitan Delivery
                        <hr class="black-line" />
                    </h3>
                    <div class="footer-methods">
                        <img src="ajax/skin/justcables/images/footer-methods.png" width="100%" />
                    </div>
                    <div class="footer-email">
                        <hr class="black-line" />
                    </div>
                    <?php /* ?>
                    <a class="footer-logo-wrapper" href="#/home">
                        <img id="logo" src="ajax/skin/justcables/images/logo.png" width="100%" />
                    </a>
                    <?php */ ?>
                </div>
                <div class="footer-right">
                    <h3>
                        Get In Touch
                        <hr class="black-line" />
                    </h3>
                    <form id="get-in-contact" name="get-in-contact" action="#/get-in-contact" method="post">
                        <label for="get-in-contact-name" class="lbl5">
                            <span class="title">Name</span>
                            <input type="text" name="get-in-contact-name" id="get-in-contact-name" />
                        </label>
                        <label for="get-in-contact-email" class="lbl5">
                            <span class="title">Email</span>
                            <input type="text" name="get-in-contact-email" id="get-in-contact-email" />
                        </label>
                        <label for="get-in-contact-message" class="lbl10">
                            <span class="title">Message</span>
                            <textarea name="get-in-contact-email" id="get-in-contact-email" style="height:80px;"></textarea>
                        </label>
                        <input id="submit-get-in-contact" type="button" class="submit" value="Submit" />
                    </form>
                </div>
            </div>
            <div id="footer-bar">
                <ul class="footer-links">
                    <li><a href="#/cms/about-us/" class="cms-link">About Us</a></li>
                    <li><a href="#/cms/orders-and-returns/" class="cms-link">Orders &amp; Returns</a></li>
                    <li><a href="#/cms/customer-service/" class="cms-link">Shipping</a></li>
                    <li><a href="#/cms/terms-and-conditions/" class="cms-link">Terms</a></li>
                    <li><a href="#/cms/privacy-policy-cookie-restriction-mode/" class="cms-link">Privacy</a></li>
                    <li><a href="#/cms/enable-cookies/" class="cms-link">Cookies</a></li>
                </ul>
            </div>
            
            <a class="headerIcon floatright footer-toggle" id="bottomFooters">
                <span class="icon">
                    <span class="label">
                        <span class="title">Close</span>	                        
                        <span class="small-revtriangle"></span>
                    </span>
                    <span class="revtriangle"></span>
                </span>
            </a>
            
        </div>
        <a id="anchor-cms"></a>
    </div>
    <div style="display:none" id="bottom"></div>
    <div id="ajax-content" style="display:none">
    	<div class="cms-container">
		    <div class="title"> &nbsp; </div>
            <div class="close">[ close ]</div>
	    	<div class="cms"></div>
        </div>
    </div>
    <div id="ajax-over" style="display:none">
    	<div style="width:100px; display:block; margin:0 auto; padding-top:25%; color:#FFF; font-size:36px; font-weight:bold">Loading...</div>
    </div>
</body>
</html>