<?php
ini_set('soap.wsdl_cache_enabled',0);
ini_set('soap.wsdl_cache_ttl',0);

include("config.php");
// header('Content-Type:text/html; charset=UTF-8');
// session_start();
date_default_timezone_set('Europe/London');
include_once("includes/api.class.php");
set_include_path("includes/");
include_once("includes/Zend/Soap/Client.php");

$api = new api("https://www.4cabling.com.au/api/soap/?wsdl", "justcables", "bfsc7w6y", $cache_folder = "../ajax/cache/", false);

// #/sub-category/coax/
// echo "<pre>";

if(isset($_GET['method']))
{
	$method = $_GET['method'];
}
else if(isset($_POST['method']))
{
	$method = $_POST['method'];
}

if($method == "products")
{
	if(isset($_POST['url']))
	{
		$u = $_POST['url'];
	}
	else if (isset($_GET['url']))
	{
		$u = $_GET['url'];
	}
	
	if(isset($_POST['category_id']))
	{
		$category_id = $_POST['category_id'];
	}
	else if (isset($_GET['category_id']))
	{
		$category_id = $_GET['category_id'];
	}
	
	if(isset($_POST['user_group']))
	{
		$user_group = $_POST['user_group'];
	}
	else if (isset($_GET['user_group']))
	{
		$user_group = $_GET['user_group'];
	}
	
	
	
	$str = explode("/", $u);
	$url_key = $str[2];
	$products = $api->getProducts($category_id, $user_group);

	$new = array();
	foreach($products as $p)
	{
		if($p["price"] > 0)
		{
			$new[] = $p;
		}
	}

	echo json_encode($new);
}
else if($method == "related")
{
	if(isset($_POST['product_id']))
	{
		$product_id = $_POST['product_id'];
	}
	else if (isset($_GET['product_id']))
	{
		$product_id = $_GET['product_id'];
	}
	
	if(isset($_POST['sku']))
	{
		$sku = $_POST['sku'];
	}
	else if (isset($_GET['sku']))
	{
		$sku = $_GET['sku'];
	}
	
	// Include Magento application
	require_once ( "../app/Mage.php" );
	umask(0);

	// Initialize Magento
	$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';
	$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';
	
	Mage::app($mageRunCode, $mageRunType);
	$_newProduct = Mage::getModel('catalog/product')->load($product_id);
	
	
	$products = $api->getRelatedProducts($product_id, $_newProduct->getSku());
	
	$related = array();
	foreach($products as $p)
	{
		$_newProduct = Mage::getModel('catalog/product')->load($p['product_id'])->getData();
		$related[] = $_newProduct;
	}
	
	echo json_encode($related);

//	echo "<pre>".print_r($_POST, true);
}
else if($method == "new-account")
{
	echo "<pre>".print_r($_POST, true);
}
else if($method == "login")
{
	echo "<pre>".print_r($_POST, true);
}
else if($method == "mage")
{
	// Include Magento application
	require_once ( "../app/Mage.php" );
	umask(0);

	// Initialize Magento
	$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';
	$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';
	
	Mage::app($mageRunCode, $mageRunType);
	
	Mage::getSingleton('core/session', array('name'=>'frontend'));
	/*
	echo "<pre>";
	var_dump(array(
		"Mage::getSingleton('checkout/cart')->getItemsCount()" =>
		Mage::getSingleton('checkout/cart')->getItemsCount()
	)); // returns number of items (w/o qty)
	var_dump(array(
		"Mage::helper('checkout/cart')->getSummaryCount()" =>
		Mage::helper('checkout/cart')->getSummaryCount()
	)); // returns number according to configuration
	var_dump(array(
		"Mage::getSingleton('customer/session')->isLoggedIn()" =>
		Mage::getSingleton('customer/session')->isLoggedIn()
	)); // returns bool true|false
	*/
	
	if(isset($_GET['action'])) {	$action = $_GET['action']; }
	else if(isset($_POST['action'])) {	$action = $_POST['action'];	}
	
	if(isset($_GET['do'])){	$do = $_GET['do']; }
	else if(isset($_POST['do'])) { $do = $_POST['do']; }
	
	
	// "frontend" for frontend session or "adminhtml" for admin session
	// Mage::getSingleton("core/session", array("name" => "frontend"));
	
	if($action == "cart")
	{
		Mage::getSingleton('checkout/cart')->getItemsCount();
		$cart = Mage::getSingleton('checkout/cart');
		
		if($do == "add")
		{
			$product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId())
			->load( $_POST['data']['product_id'] );

			try{
				$cart->addProduct($product, array('qty' => $_POST['data']['qty']));
				// save the cart
				$cart->save();
				// very straightforward, set the cart as updated
				Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			}
			catch(Mage_Core_Exception $e)
			{
				echo json_encode( array("response" => "exception" ) );
				exit;
			}
			

			echo json_encode( array("response" => "ok") );
		}
		else if($do == "remove")
		{
			
			$cartHelper = Mage::helper('checkout/cart');
			$items = $cartHelper->getCart()->getItems();
			$i = 0;
			foreach ($items as $item) {
				if( $i == $_POST['line'])
				{
					$cartHelper->getCart()->removeItem( $item->getItemId() )->save();
				}
				$i++;
			}
			
			Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			
			echo json_encode( array("response" => "ok") );
		}
		else if($do == "update")
		{
			$cartHelper = Mage::helper('checkout/cart');
			$items = $cartHelper->getCart()->getItems();
			$i = 0;
			foreach ($items as $item) {
				$item->setQty( $_POST['newData'][$i]["qty"] )->save();
				// $cartHelper->getCart()->updateItem( array( $item->getId() => array('qty' => $_POST['newData'][$i]["qty"]) ) )->save();
				$i++;
			}
			
			$cartHelper->getQuote()->setTotalsCollectedFlag(false)->collectTotals()->save();
			
			Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			echo json_encode( array("response" => "ok") );
		}
		else if($do == "view")
		{
			
			$session= Mage::getSingleton('checkout/session');
			$retval = array();
			foreach($session->getQuote()->getAllItems() as $item)
			{
				$retval[] = array(
				   "product_id" => $item->getProductId(),
				   "product_sku" => $item->getSku(),
				   "product_name" => $item->getName(),
				   "product_price" => $item->getPrice(),
				   "product_qty" => $item->getQty()
			   );
			}
			
			
			$totalItemsInCart = Mage::helper('checkout/cart')->getItemsCount(); //total items in cart
			$totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals(); //Total object
			$subtotal = round($totals["subtotal"]->getValue()); //Subtotal value
			$grandtotal = round($totals["grand_total"]->getValue()); //Grandtotal value
			if(isset($totals['discount']) && $totals['discount']->getValue()) {
				$discount = round($totals['discount']->getValue()); //Discount value if applied
			} else {
				$discount = '';
			}
			if(isset($totals['tax']) && $totals['tax']->getValue()) {
				$tax = round($totals['tax']->getValue()); //Tax value if present
			} else {
				$tax = '';
			}
			
			$cart = array(
				"item_count" => $totalItemsInCart,
				"items" => $retval,
				"totals" => array(
					"subtotal" => $subtotal,
					"grandtotal" => $grandtotal,
					"discount" => $discount,
					"tax" => $tax,
				)
			);
			
			echo json_encode($cart);
			
			/*			
			foreach($items as $key => $value)
			{
			   // call the Magento catalog/product model
			   $product = Mage::getModel('catalog/product')
								 // set the current store ID
								 ->setStoreId(Mage::app()->getStore()->getId())
								 // load the product object
								 ->load($key);
			
			   // start adding the product
			   $cart->addProduct($product, array('qty' => $value));
			   // save the cart
			   $cart->save();
			
			   // very straightforward, set the cart as updated
			   Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			}
			*/
			
		}
	}
	else if($action == "cms")
	{
		
		$cmsPage = Mage::getModel('cms/page')->load( $_POST['id'] , 'identifier');
		if( $cmsPage->getId() )
		{
			echo json_encode( 
				array(
					"title" =>  $cmsPage->getTitle(),
					"content" => $cmsPage->getContent(),
				) 
			);
		}
		else
		{
			echo json_encode( array("excetiopn" => "true") );
		}
	}

	/*
	$session = Mage::getSingleton("customer/session");
	
	if($session->isLoggedIn())
	{
		echo "Logged in";
	}else{
		echo "Not logged in";
	}
	*/	
}



function returnCart()
{
	
}

// print_r($_POST);

// print_r($products);
?>