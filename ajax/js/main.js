// JavaScript Document
var sectionAnchors = Array("", "home", "subcategory", "products-in-category", "products-page");
var cart = Array();
var currentSection = null;
var productCache = Array();
var productRelatedCache = Array();
var init_load_product = "false";
var jquery_url = null;

var rtime = new Date(1, 1, 2000, 12,00,00);
var timeout = false;
var resizedelta = 200;
var Profiler = Array();


jQuery(window).resize(function(height) {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, resizedelta);
    }
});

function isiPhone(){
    return (
        (navigator.platform.indexOf("iPhone") != -1) ||
        (navigator.platform.indexOf("iPod") != -1)
    );
}

function resizeend() {
    if (new Date() - rtime < resizedelta) {
        setTimeout(resizeend, resizedelta);
    } else {
        timeout = false;
        // console.log('Done resizing');
		
		contentsetup();
		doinit();
		// console.trace();
    }               
}


// on document ready
jQuery(document).ready(function() {
	

	jQuery("#qty").css({"display":"none"});
	jQuery(".product-right .product-add-to-cart").css({"display":"none"});
	jQuery(".product-right .product-price").css({"display":"none"});

	contentsetup();
	
	// setup Content
	setupContent();
	// setup hooks
	setupHooks();
	
	// load cart
	generateCart();
	doinit();
	
	lastresize = new Date().getTime();
});

function timer()
{
	
}

function doinit()
{
	// display URL section
	var url = jQuery.url(); 
	jquery_url = url;
	var section = url.fsegment(1);
	// console.log(url.fsegment(1));
	
	if(section == "home")
	{
		scrollToAnchor("home");
	}
	else if(section == "cart")
	{
		sectionToggle("cart");	
	}
	else if(section == "category")
	{
		selectMainCategory( url )
		if(url.fsegment(3) == "sub-category")
		{
			selectSubCategory( url )
			if(url.fsegment(5) == "product")
			{
				// set loaded product
				// selectProduct( url, jQuery(".product-in-category").find(".product_parent").val() );
				scrollToAnchor("products-page");
				init_load_product = "true";
				jquery_url = url;
			}
			else
			{
				scrollToAnchor("products-in-category");
			}
		}
		else
		{
			scrollToAnchor("subcategory");
		}
	}
	else if(section == "sub-category")
	{
		// find the parent category
	}
	else if(section == "cms")
	{
		// scrollToAnchor("cms");
		showCMS( url.fsegment(2) )
	}
	
	// initial setup
	contentsetup();
}

function resizeScroll()
{
	// display URL section
	var url = jQuery.url(); 
	var section = url.fsegment(1);
	// console.log(url.fsegment(1));
	
	if(section == "home")
	{
		scrollToAnchor("home");
	}
	else if(section == "category")
	{
		if(url.fsegment(3) == "sub-category")
		{
			if(url.fsegment(5) == "product")
			{
				scrollToAnchor("products-page");
			}
			else
			{
				scrollToAnchor("products-in-category");
			}
		}
		else
		{
			scrollToAnchor("subcategory");
		}
	}
}

function selectMainCategory( jurl )
{
	jQuery.each( jQuery(".main-category"), function(e){
		var url = jQuery(this).find(".url");
		if(url.attr("href") == jQuery(location).attr("hash"))
		{
			// set category title
			jQuery("#category-label").html( jQuery(this).find(".title").html() )
		}
	});
	setSubCategories( jurl.fsegment(2) );
}

function selectSubCategory( jurl )
{
	var category_id = 0;
	var hash = "#/"+jurl.fsegment(1)+"/"+jurl.fsegment(2)+"/"+jurl.fsegment(3)+"/"+jurl.fsegment(4)+"/";
	
	// console.log( hash );
	jQuery.each( jQuery(".sub-category"), function(e){
		var url = jQuery(this).find(".url");
		if(url.attr("href") == hash )
		{
			var title =  jQuery(this).find(".title").html();
//			var breadcrumb = '<a href>';
			jQuery("#sub-category-label").html( title )
			category_id = jQuery(this).find(".category_id").attr("value")
			// jQuery("#products-container").html('<div class="products-loading">Loading products...</div>');
			 jQuery("#products-container").html("");
		}
	});

	// load products in category
	loadProducts( hash, category_id );
}

function selectProduct( jurl, products, category_id )
{
	var hash = "#/"+jurl.fsegment(1)+"/"+jurl.fsegment(2)+"/"+jurl.fsegment(3)+"/"+jurl.fsegment(4)+"/"+jurl.fsegment(5)+"/"+jurl.fsegment(6);
	jQuery.each( jQuery(".product-in-category"), function(e){
		var url = jQuery(this).find(".url");
		if(url.attr("href") == hash )
		{
			
			jQuery(".product-title").html( jQuery(this).find(".title").html() );
			jQuery(".product-price").find(".ammount").html( jQuery(this).find(".ammount").html() );
			jQuery(".product-image").find(".image").html( '<img width="100%" src="'+jQuery(this).find("img").attr("src")+'">' );
			
			var product_id = jQuery(this).find(".product_id").attr("value");
			var category_id = jQuery(this).find(".product_parent").attr("value");
		
			jQuery("#current_product_id").val( product_id );
			
			jQuery.each( products , function(e){
				if(jQuery(this).attr("product_id") == product_id)
				{
					jQuery(".product-description").html( jQuery(this).attr("description") );
				}
			});
			
		}
	});
	
	// contentsetup();
	// scrollToAnchor("products-page");
}

function setupContent()
{
//	console.log(categories);

	// main page categories
	var html = "";
	jQuery.each(categories, function(cat){
		
		// console.log(jQuery(this));
		html+= '            <div class="main-category">';
		html+= '                <div class="image">\n';
		html+= '                    <img src="'+getImage(jQuery(this).attr("image"), 'category')+'">\n';
		html+= '                </div>\n';
		html+= '                <a href="#/category/'+jQuery(this).attr("url_key")+'/" class="url"><img width="100%" class="overlay" src="ajax/skin/justcables/images/product-overlay.png"></a>\n';
		html+= '                <div class="title">'+jQuery(this).attr("name")+'</div>\n';
		html+= '            </div>\n';
	});
	
	jQuery(".maincategories").html(html);
}

function findCategory( key )
{
	var retval = null;
	jQuery.each(categories, function(e){
		
		var c = jQuery(this)[0];
		if(c.url_key == key)
		{
			retval = c;
		}
	});
	return retval;
}

function findSubCategory( children, key )
{
	var retval = null;
	jQuery.each(children, function(e){
		
		var c = jQuery(this)[0];
		if(c.url_key == key)
		{
			retval = c;
		}
	});
	return retval;
}

function setSubCategories( url_key )
{
	//  #/category/audio-cables/
	
	var cat = findCategory( url_key )
	var html = "";
	jQuery.each( jQuery(cat).attr("children"), function(e){
		html+= '			<div class="sub-category">\n';
		html+= '                <div class="image">\n';
		html+= '                    <img src="'+getImage(jQuery(this).attr("image"),'category')+'">\n';
		html+= '                </div>\n';
		html+= '                <a href="#/category/'+url_key+'/sub-category/'+jQuery(this).attr("url_key")+'/" class="url"><img width="100%" class="overlay" src="ajax/skin/justcables/images/product-overlay-2.png"></a>\n';
		html+= '                <div class="title">'+jQuery(this).attr("name")+'</div>\n';
		html+= '                <input type="hidden" class="category_id" id="category_'+jQuery(this).attr("category_id")+'" value="'+jQuery(this).attr("category_id")+'" />\n';
		html+= '            </div>\n';
	});
	
	jQuery("#subcategories-container").html(html);

	// re-set sub category hooks!
	
	jQuery(".sub-category").each(function(e){
		jQuery(this).find(".url").bind("click", function(e){
			
			// here
			var newurl = jQuery(this).attr("href");
			
			document.location.hash = newurl;
			doinit();
			
			/*
			var scategorylabel = jQuery(this).parent().find(".title").html();
			jQuery("#sub-category-label").html( scategorylabel )
			jQuery("#products-container").html('<div class="products-loading">Loading products...</div>');
			// load products in category
			// when finished scroll to products-in-category
			
			// console.log( url );
			loadProducts( url, jQuery(this).parent().find(".category_id").attr("value") );
			
			// scroll to
			scrollToAnchor("products-in-category");
			*/
		})
	});
}

function loadProducts(realurl, category_id)
{
	var user_group = 0;
	
	if( productCache[category_id] )
	{
		generateProducts(realurl, productCache[category_id], category_id, 64);
	}
	else
	{
		jQuery.ajax({
			"type": "POST",
			"url": "ajax/api.php",
			"dataType": "json",
			"data": { "method": "products", "url": realurl, "category_id": category_id, "user_group": user_group },
			beforeSend: function(){
				AjaxPreloader("start");
			}
		}).done(function( products ) {
			AjaxPreloader("end");
			productCache[category_id] = products;
			// console.log( products.length )
			generateProducts(realurl, products, category_id, 64)
		});	
	
	}
}

function getCurrentSlide()
{
	var current = jQuery("#current-slide").val();
	return current;
}

function showSlideController(element, type)
{
	var slide_pages = jQuery(".slide").length;
	var slide = getCurrentSlide().split("_")[1];

	// console.log("showSlideController["+element+"] type["+type+"] slide_id["+slide+"]")
	
	if( getSection() == 3)
	{
		if(type == "left" && slide > 1 || type == "right" && slide < slide_pages )
		{
			jQuery("#"+element).css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0});
	
		}
		else
		{
			jQuery("#"+element).css({opacity: 1.0, visibility: "visible"}).animate({opacity: 0.0});
		}
	}
}

function generateProducts(realurl, products, category_id, howmany)
{
	jQuery("#current-slide").val("s_1");
	var products_per_page = 6;
	var slide_pages = Math.ceil( products.length / products_per_page );
	
	/*
	var max = products.length;
	if(products.length > howmany)
	{
		max = howmany;
	}
	*/
	
	var slides = Array();
	var slide = "";
	var html = "";	
	var cnt = 0;
	var slideCnt = 0;
	for(var i = 0; i < products.length; i++)
	{
		if(cnt == 0)
		{
			slideCnt++;
			html+= '			<div class="slide" id="s_'+slideCnt+'">';
		}
		
		var product = products[i];		
		var price = parseFloat(jQuery(product).attr("price"));		
		html+='					<div class="product-in-category">\n';
		html+='                    <div class="image">\n';
		html+='                        <img src="http://'+base_url+'/media/catalog/product/'+jQuery(product).attr("image")+'" />\n';
		html+=' 	                   <div class="info">\n';
		html+=' 	                	   <div class="title">'+jQuery(product).attr("name")+'</div>\n';
		html+='	    	            	   <div class="price">$<span class="ammount">'+price.toFixed(2)+'</span> <span>ex GST</span></div>\n';
		html+=' 	                   </div>\n';
		html+='                    </div>\n';
		html+='                    <a class="url" href="'+realurl+'product/'+jQuery(product).attr("product_id")+'"><img src="ajax/skin/justcables/images/product-overlay-3-Circle.png" class="overlay" width="100%" /></a>\n';
		html+='                    <a class="addtocartC">\n'; // href="#/addtocart/'+jQuery(product).attr("product_id")+'/"
		html+='                    		<span class="addtocart">Add To Cart</span>\n';
		html+='                			<input type="hidden" class="product_parent" id="product_parent_'+jQuery(product).attr("product_id")+'" value="'+category_id+'" />\n';
		html+='                			<input type="hidden" class="product_id" id="product_id_'+jQuery(product).attr("product_id")+'" value="'+jQuery(product).attr("product_id")+'" />\n';
		html+='                    </a>\n';
		html+='                </div>\n';
		

		cnt++;
		if(cnt == products_per_page)
		{
			html+= '			</div>'
			cnt = 0;
		}
	}
	
	if(cnt > 0)
	{
			html+= '			</div>'
	}
	
	jQuery("#products-container").html( html );
	
	
	
	jQuery(".product-in-category").each(function(e){
		// set product title link
		jQuery(this).find(".url").bind("click", function(e){

			jQuery(".product-title").html( jQuery(this).parent().find(".title").html() );
			jQuery(".product-price").find(".ammount").html( jQuery(this).parent().find(".ammount").html() );
			jQuery(".product-image").find(".image").html( '<img width="100%" src="'+jQuery(this).parent().find("img").attr("src")+'">' );
			
			var product_id = jQuery(this).parent().find(".product_id").attr("value");
			var category_id = jQuery(this).parent().find(".product_parent").attr("value");

			jQuery("#current_product_id").val( product_id );
			
			jQuery.each( productCache[category_id] , function(e){
				if(jQuery(this).attr("product_id") == product_id)
				{
					jQuery(".product-description").html( jQuery(this).attr("description") );
				}
			});
			
			scrollToAnchor("products-page");
		})
		
		// set add to cart action
		jQuery(this).find(".addtocartC").bind("click", function(e){
			// scroll to
			// scrollToAnchor("products-page");
			
			// console.log("add to cart ["++"]");
			// console.log(  );
			
			var product = {
				"product_id": jQuery(this).parent().find(".product_id").val() ,
				"name": jQuery(this).parent().find(".title").html(),
				"qty":1,
				"price": jQuery(this).parent().find(".ammount").html()
			};
			
			addToCart( product )
			
			/*
			if(jQuery("#cart").css("display") == "none")
			{
				sectionToggle("cart");
			}
			*/
			// generateCart()
		})
		
		// set hover action
		jQuery(this).hover(function(e){
			// over	
			var src = "ajax/skin/justcables/images/product-overlay-3-CircleYellow-2.png";
			jQuery(this).find(".overlay").attr("src", src);
			
		},function(e){
			// out
			var src = "ajax/skin/justcables/images/product-overlay-3-Circle.png";
			jQuery(this).find(".overlay").attr("src", src);
		})
		
	});
	
	if(init_load_product == "true")
	{
		init_load_product = "false";
		// load initial product!
		selectProduct( jquery_url, products, jQuery(".product-in-category").find(".product_parent").val() );
	}
	
	contentsetup()
	
	// jQuery("#products-scroll-container").css({"width":jQuery("#product").width()})
	// jQuery("#products-container").css({"width": jQuery(".product-in-category").width() * jQuery(".product-in-category").length })

	setCustomIphoneCss();
	
}

function sectionToggle( section )
{
	jQuery("#"+section).toggle(function(){
		jQuery(this).animate({height:"hide"},200, "", sectionToggled(section));
	  },function(){
		jQuery(this).animate({height:  200 },200, "", sectionToggled(section));
	  });
}

function sectionToggleBottom( section, newHeight )
{
	// console.log("sectionToggleBottom section["+section+"] newHeight["+newHeight+"]")
	if(newHeight == null)
	{
		newHeight = 314;
	}

	jQuery("#"+section).toggle(function(){
		jQuery(this).animate({height:"hide"},200, "", sectionToggledB(section));
	  },function(){
		jQuery(this).animate({height: newHeight },200, "", sectionToggledB(section));
	  });
}


function sectionToggledB(section)
{
	
	currentSection = section;
	/*
	if(jQuery("#"+section).css("display") == "block")
	{
		jQuery(".header-close-bar").css({display:"block"});
		jQuery(".header-top-bar").css({display:"none"});
		/// var newval = jQuery("#"+section).height() + 100 ;
	}
	else
	{
		jQuery(".header-close-bar").css({display:"none"});
		jQuery(".header-top-bar").css({display:"block"});
		// var newval = 70;
	}
	*/
	// jQuery("#homecontent").animate({"padding-top": newval },200);
}


function sectionToggled(section)
{
	
	currentSection = section;
	if(jQuery("#"+section).css("display") == "block")
	{
		jQuery(".header-close-bar").css({display:"block"});
		jQuery(".header-top-bar").css({display:"none"});
		var newval = jQuery("#"+section).height() + 100 ;
	}
	else
	{
		jQuery(".header-close-bar").css({display:"none"});
		jQuery(".header-top-bar").css({display:"block"});
		var newval = 70;
	}
	jQuery("#homecontent").animate({"padding-top": newval },200);
}

function doClose()
{
	sectionToggle(currentSection)
}

function showCMS(id)
{
	jQuery("#ajax-content").css({"display":"block"});

	jQuery.ajax({
		"type": "POST",
		"url": "ajax/api.php",
		"dataType": "json",
		"data": { "method": "mage", "action":"cms", "id":id },
		beforeSend: function(){
			AjaxPreloader("start");
		}
	}).done(function( msg ) {
		AjaxPreloader("end");
		jQuery("#ajax-content .cms-container .title").html( msg.title );
		jQuery("#ajax-content .cms-container .cms").html( msg.content );
	});	
}


function setupHooks()
{
	jQuery(".cms-link").each(function(e){
		jQuery(this).bind("click", function(e){
			// cartToggle();
			// sectionToggle("cart");
			var id = jQuery(this).attr("href").split("/")[2];
			showCMS( id );
		})
	});
	
	jQuery("#ajax-content").bind("click", function(e){
		jQuery("#ajax-content").css({"display":"none"});
	}).children().click(function(e) {
		return false;
	});
	
	jQuery("#ajax-content .cms-container .close").bind("click", function(e){
		jQuery("#ajax-content").css({"display":"none"});
	});
		
	jQuery(".cart-toggle").each(function(e){
		jQuery(this).bind("click", function(e){
			// cartToggle();
			sectionToggle("cart");
		})
	});
	
	jQuery(".footer-toggle").each(function(e){
		jQuery(this).bind("click", function(e){
			// cartToggle();
			sectionToggleBottom("footer", 314);
		})
	});
	
	jQuery("#cart-update").each(function(e){
		jQuery(this).bind("click", function(e){
			updateCartQuantities();
		})
	});
	
	jQuery(".account-toggle").each(function(e){
		jQuery(this).bind("click", function(e){
			// accountToggle();
			sectionToggle("account");
		})
	});
		
	jQuery(".search-toggle").each(function(e){
		jQuery(this).bind("click", function(e){
			sectionToggle("search");
		})
	});
	
	jQuery("#account-create").bind("click", function(e){
		sectionToggle("account");
		sectionToggle("newaccount");
	})


	jQuery("#do-account-login").bind("click", function(e){
		if( jQuery("#account-email").val().length > 5 && jQuery("#account-password").val().length)
		{
			jQuery.ajax({
				"type": "POST",
				"url": "ajax/api.php",
				"dataType": "json",
				"data": { "method": "login", "user":jQuery("#account-email").val(), "password":jQuery("#account-password").val() },
				beforeSend: function(){
					AjaxPreloader("start");
				}
			}).done(function( msg ) {
				AjaxPreloader("end");
				// console.log ( msg );
			});	
		}
	})

	jQuery("#new-account-submit").bind("click", function(e){


		var fields = Array("new-account-first-name", "new-account-last-name", "new-account-email", "new-account-password", "new-account-password-v");
		var failed = 0;
		for(var i = 0; i < fields.length; i++)
		{
			if(jQuery("#"+fields[i]).val().length == 0)
			{
				failed++;
			}
		}
		
		if( jQuery("#new-account-password").val().length >= 8 && jQuery("#new-account-password").val() == jQuery("#new-account-password-v").val())
		{
			if(failed == 0)
			{
				// console.log("new account ok!")
				
				var userdata = {
					"new-account-first-name": jQuery("#new-account-first-name").val(),
					"new-account-last-name": jQuery("#new-account-last-name").val(),
					"new-account-email": jQuery("#new-account-email").val(),
					"new-account-password": jQuery("#new-account-password").val()
				};
				
				jQuery.ajax({
					"type": "POST",
					"url": "ajax/api.php",
					"dataType": "json",
					"data": { "method": "new-account", "userdata":userdata },
					beforeSend: function(){
						AjaxPreloader("start");
					}
				}).done(function( msg ) {
					AjaxPreloader("end");
					// console.log ( msg );
				});	
				
				
			}
			else
			{
				// console.log("failed["+failed+"]")
			}
		}
		else
		{
			// console.log("password missmatch ["+jQuery("#new-account-password").attr("value").length+"] ["+jQuery("#new-account-password").attr("value")+"/"+jQuery("#new-account-password-v").attr("value")+"]");
		}
		
		

	})


	jQuery("#cart-checkout").bind("click", function(e){

		// console.log( cart );
		if(cart.item_count > 0)
		{
			document.location= "http://"+base_url+"/checkout/onepage/";
		}
		else
		{
			alert("Cart Empty!");
		}
	})
		
	jQuery(".headerclose").bind("click", function(){
		doClose();
	})
		
	jQuery(".main-category").each(function(e){
		jQuery(this).find(".url").bind("click", function(e){
			// scroll to
			// console.log( jQuery(this).attr("href") );
			// debugz( "Scroll to[<strong>"+ jQuery(this).attr("href")+"</strong>]" );
			
			var categorylabel = jQuery(this).parent().find(".title").html();
			jQuery("#category-label").html( categorylabel )
			
			var url = jQuery(this).attr("href").split("/");
			var url_key = url[2];
			
			setSubCategories( url_key );
			scrollToAnchor("subcategory");
		})
	});

	/*	
	jQuery(".sub-category").each(function(e){
		jQuery(this).find(".url").bind("click", function(e){
			
			var scategorylabel = jQuery(this).parent().find(".title").html();
			jQuery("#sub-category-label").html( scategorylabel )
			
			// scroll to
			scrollToAnchor("products-in-category");
		})
	});
	*/
	/*
	jQuery(".product-in-category").each(function(e){
		// set product title link
		jQuery(this).find(".url").bind("click", function(e){

			jQuery(".product-title").html( jQuery(this).parent().find(".title").html() );
			jQuery(".product-price").find(".ammount").html( jQuery(this).parent().find(".ammount").html() );
			
			
			scrollToAnchor("products-page");
		})
		
		// set add to cart action
		jQuery(this).find(".addtocartC").bind("click", function(e){
			// scroll to
			// scrollToAnchor("products-page");
			
			// console.log("add to cart ["++"]");
			// console.log(  );
			
			var product = {
				name: jQuery(this).parent().find(".title").html(),
				"qty":1,
				"price": jQuery(this).parent().find(".ammount").html()
			};
			
			addToCart( product )
			
			if(jQuery("#cart").css("display") == "none")
			{
				cartToggle();
			}
			
			generateCart()
		})
		
		// set hover action
		jQuery(this).hover(function(e){
			// over	
			var src = "skin/justcables/images/product-overlay.png";
			jQuery(this).find(".overlay").attr("src", src);
			
		},function(e){
			// out
			var src = "skin/justcables/images/product-overlay-3.png";
			jQuery(this).find(".overlay").attr("src", src);
		})
		
	});
	*/
	
	
	jQuery(".product-add-to-cart").each(function(e){
		jQuery(this).bind("click", function(e){
			// open up cart and add the current product in it!
			
			var product = {
				"product_id": jQuery("#current_product_id").val() ,
				"name": jQuery(this).parent().find(".product-title").html(),
				"qty": parseInt( jQuery("#qty").find(".amount").html() ),
				"price": jQuery(this).parent().find(".ammount").html()
			};
			
			addToCart( product )
			
			/*
			if(jQuery("#cart").css("display") == "none")
			{
				sectionToggle("cart");
			}
			*/
			// generateCart()
			
		})
	});
	
	
	jQuery(".logo-wrapper").each(function(e){
		jQuery(this).bind("click", function(e){
			// scroll to
			scrollToAnchor("home");
		})
	});
	
	jQuery(".topback").bind("click", function(e){
		// scroll to
		
		// get current url and determine previous one
		var zurl = jQuery.url();
		// console.log( zurl.fsegment().length );
		var newurl = "/";
		if(zurl.fsegment().length == 2)
		{
			newurl = "/home/";
		}
		else
		{
			for(var i = 1; i < zurl.fsegment().length - 1; i++)
			{
				newurl+= zurl.fsegment(i)+"/";
			}
		}
		
		document.location.hash = newurl;
		doinit();
	})
	
	
	jQuery("#slide-prev").bind("click",function(e){
		var left = jQuery(".slide-content").scrollLeft();
		var current = jQuery("#current-slide").val().split("_")[1];
		var next = parseInt(current)-1
		
		if(!jQuery("#s_"+next).length )
		{
			next = parseInt(jQuery(".slide-content").find(".slide").length);
		}
		
		scrollWidthToElement('.slide-scroll', '.slide-content', "#s_"+next);
		jQuery("#current-slide").val("#s_"+next);

	})

	
	jQuery("#slide-next").bind("click",function(e){
		// console.log("#slide-next clicked!");
		var left = jQuery(".slide-content").scrollLeft();
		var current = jQuery("#current-slide").val().split("_")[1];
		var next = parseInt(current)+1

		if(!jQuery("#s_"+next).length )
		{
			next = 1;
		}


		// console.log("left["+left+"]");
		// console.log("current["+current+"]");
		// console.log("next["+next+"]");

		scrollWidthToElement('.slide-scroll', '.slide-content', "#s_"+next);
		jQuery("#current-slide").val("#s_"+next);
	})
	
	
	jQuery.each(jQuery(".selector"), function(e){
		
		// find id
		var id = jQuery(this).attr("id");
		
		// console.log("selector["+id+"]")
		var left = jQuery(this).find(".left");
		var right = jQuery(this).find(".right");
		
		left.bind("click",function(e){
			setSelectorAmount("#"+id, "minus");
		})
		
		right.bind("click",function(e){
			setSelectorAmount("#"+id, "plus");
		})
	});
	
}


function setSelectorAmount(sel, way)
{
	var amount = parseInt(jQuery(sel).find(".amount").html());
	if(way == "minus")
	{
		var newam = amount - 1;
		if(newam > 0)
		{
			jQuery(sel).find(".amount").html(newam)
		}
	}
	else
	{
		var newam = amount + 1;
		if(newam < 1000)
		{
			jQuery(sel).find(".amount").html(newam)
		}
	}
}

// window resize handler
/*
jQuery(window).resize(function(height) {

	if(Modernizr.touch)
	{
		e.preventDefault();
	}
	
	// debugz("window resize[contentsetup]");
	contentsetup();
});
*/

/*
jQuery("#wrapper").scroll(function() {
	// debugz("scroll event[setupScrolling]");
	setupScrolling()
});
*/
/*
if(jQuery.support.cssFloat == false)
{
	jQuery(window).scroll(function() {
		setupScrolling()
	});
}
*/

function setupScrolling()
{
//	console.log("setupScrolling()");
	var backgroundColors = Array("", "#FFF", "#FCEE21", "#0A70CC", "#1454A2", "#FCEE21" );
	var buttonColors = Array( "", "#FCEE21", "#FFF", "#FCEE21", "#FCEE21", "#FCEE21" );
	var textColors = Array( "", "#000", "#0A70CC", "#000", "#000", "#000", "#000" );
	var backColor = Array( "", "#FCEE21", "#0A70CC", "#1454A2", "#FDEE21", "#CCCCCC" );
	var backColorText = Array( "", "#000", "#FFF", "#FFF", "#000", "#000" );
	var backtextColors = Array( "", "#FFF", "#FFF", "#FFF", "#000", "#000", "#000" );

	var footerTextColors = Array( "", "#000", "#FFF", "#FFF", "#000", "#000", "#000" );
	var footerBgColors = Array( "", "#FCEE21", "#0A70CC", "#1454A2", "#FDEE21", "#CCCCCC" );


	var section = getSection();
	// set header background color
	jQuery("#header").css({"background-color": backgroundColors[section]});
	// set triangle background colors
	jQuery("#header").find(".revtriangle").css({"border-color": buttonColors[section]+" transparent transparent transparent"});

	// console.log(".triangle new color["+backColor[section]+"]" )
	jQuery(".topback").find(".triangle").css({"border-color": "transparent transparent "+backColor[section]+" transparent"});
	
	jQuery(".headerclose").find(".triangle").css({"border-color": "transparent transparent "+backColor[section]+" transparent"});
	jQuery(".headerclose").find(".title").css({"color": backColorText[section]});
	jQuery(".headerclose").find(".small-triangle").css({"border-color": "transparent transparent "+backColorText[section]+" transparent"});

	
	// set triangle content colors
	jQuery("#header").find(".revtriangle").parent().find(".label").css({"color": textColors[section]});
	jQuery("#header").find(".revtriangle").parent().find(".small-revtriangle").css({"border-color": textColors[section]+" transparent transparent transparent"} );
	jQuery(".topback").find(".label").css({"color": backColorText[section]});
	jQuery(".topback").find(".small-triangle").css({"border-color": "transparent transparent "+backColorText[section]+" transparent"});
	
	// console.log("back color["+backColorText[section]+"]");
	
	// set footer close button colors
	jQuery("#footer .headerIcon").find(".revtriangle").css({"border-color": footerBgColors[section]+" transparent transparent transparent" });
	jQuery("#footer .headerIcon").find(".title").css({"color": footerTextColors[section]});
	jQuery("#footer .headerIcon").find(".small-revtriangle").css({"border-color": footerTextColors[section]+" transparent transparent transparent" });

	
	if(section > 1)
	{
		jQuery(".topback").css({"visibility":"visible"});
		// jQuery("#footer").css({"visibility":"hidden"});
	}
	else
	{
		jQuery(".topback").css({"visibility":"hidden"});
		// jQuery("#footer").css({"visibility":"visible"});
	}




	jQuery("#slide-prev").css({"visibility":"hidden"});
	jQuery("#slide-next").css({"visibility":"hidden"});
	
	showSlideController("slide-prev", "left");
	showSlideController("slide-next", "right");
	

	if(section == 4)
	{
		jQuery("#qty").css({"display":"block"});
		jQuery(".product-right .product-add-to-cart").css({"display":"block"});
		jQuery(".product-right .product-price").css({"display":"block"});
		
		if(jQuery("#product-related").css("display") != "block")
		{
			// console.log("section 4.. display related products");
			loadRelatedProducts();
			
		}
	}
	else
	{
		if(jQuery("#product-related").css("display") == "block")
		{
			// console.log("section not 4.. hide related products");
			sectionToggleBottom( "product-related", jQuery(window).height() / 100 * 15 );
		}
	}
	// jQuery("#footer").css({"visibility":"visible"});
	
	/*
	hide footer bar
	*/

	// debugz()
}


function loadRelatedProducts()
{
//	console.log("loadRelatedProducts");
	// read product from id
	var url = jQuery.url();
	var productId = 0;
	if(url.fsegment(1) == "category")
	{
		productId = url.fsegment(6);
	}
	
	if(productId > 0)
	{
		if( productRelatedCache[productId] )
		{
			generateRelatedProducts(productRelatedCache[productId], productId);
		}
		else
		{
			jQuery.ajax({
				"type": "POST",
				"url": "ajax/api.php",
				"dataType": "json",
				"data": { "method": "related", "product_id": productId },
				beforeSend: function(){
					AjaxPreloader("start");
				}
			}).done(function( products ) {
				AjaxPreloader("end");
				productRelatedCache[productId] = products;
				// console.log( products.length )
				generateRelatedProducts( products, productId );
			});	
		
		}
	}
}


function getImage(image, type)
{
	if(image == "no_selection")
	{
		// return placehodler
		return 'http://'+base_url+'/ajax/skin/justcables/images/placeholder.jpg';
	}
	else
	{
		return 'http://'+base_url+'/media/catalog/'+type+'/'+image;
	}
	return image;
}

function generateRelatedProducts(products, parent_id)
{
	// console.log("generateRelatedProducts parent["+parent_id+"]");
	// console.log(products);
	
	var html = "";
	jQuery.each(products,function(e){
		
		html+='		<div class="related-product">\n';
		html+='         <div class="image">\n';
		html+='             <img src="'+getImage(jQuery(this).attr("image"), "product")+'">\n';  
		html+='         </div>\n';
		html+='         <a href="#/related-product/'+jQuery(this).attr("entity_id")+'" class="url"><img width="100%" class="overlay" src="ajax/skin/justcables/images/related-product-overlay-CircleGrey.png"></a>\n';
		html+='         <div class="title">'+jQuery(this).attr("name")+'</div>\n';
		html+='     </div>\n';
	});
	
	jQuery(".related-products-container").html(html);
	
	jQuery.each(jQuery(".related-product"),function(e){
		jQuery(this).hover(function(e){
			// over	
			var src = "ajax/skin/justcables/images/related-product-overlay-CircleYellow.png";
			jQuery(this).find(".overlay").attr("src", src);
			
		},function(e){
			// out
			var src = "ajax/skin/justcables/images/related-product-overlay-CircleGrey.png";
			jQuery(this).find(".overlay").attr("src", src);
		})
	})
	
	sectionToggleBottom( "product-related", jQuery(window).height() / 100 * 15  );
	
	contentsetup();
}

var old_add = null;
var old_add_times = 0;

function debugz( add )
{
	var old = jQuery("#debug").html();
	output = add+"<br />" + old;	
	jQuery("#debug").html( output );

/*
	console.log("old_add["+old_add+"] ["+old_add_times+"]");
	console.log("add["+add+"]");
	
	var old = jQuery("#debug").html();
	
	if(old_add == add)
	{
		old_add_times++;
	}
	else
	{
		output = add+"<br />" + old;
		jQuery("#debug").html( output );
		old_add = add;
	}
*/	
}

function getSection()
{
	var scrolled = jQuery("#wrapper").scrollTop();
	var contentheight = jQuery(".content").height() + 90 -50;
	
	if(scrolled == 0){ scrolled = 1; }
	return Math.ceil( scrolled / contentheight );
}


function contentsetup()
{
	var ww = jQuery(window).width();
	var wh = jQuery(window).height();
	
	var zoom = document.documentElement.clientWidth / window.innerWidth;

	jQuery("#wrapper").find("[class^=responsive-]").each(function(e){
	
		var str = jQuery(this).attr("class").split("-");
		var action 		= str[1];
		var compare 	= str[2];
		var width 		= str[3];
		var width2 		= str[4];

		// do the comparison
		if(compare == "lt" && ww <= width)
		{
			jQuery(this).css({display:action});
		}
		else if (compare == "gt" && ww > width)
		{
			jQuery(this).css({display:action});
		}
		else if (compare == "bt" && ww <= width2 && ww > width)
		{
			jQuery(this).css({display:action});
		}
		else
		{
			jQuery(this).css({display:"none"});
		}

	});

	jQuery(".wrapper-content").css({ "margin-top": "90px" });

	var contentwidth = "90%";
	var paddingleft = "5%";
	var paddingright = "5%";
	
	if(ww > 600 && ww < 1000)
	{
		contentwidth = "90%";
		paddingleft = "5%";
		paddingright = "5%";
	}
	
	jQuery("#wrapper").find("[class^=content]").each(function(e){
			jQuery(this).css({
				"width": contentwidth,
				"height": wh - jQuery("#header").height() - 30,
				"padding-left":paddingleft,
				"padding-right":paddingright,
				"padding-top":jQuery("#header").height() ,
				"padding-bottom":jQuery("#header").height() ,
			});
	});
	
	// contentwidth
	
	jQuery("#ajax-content .cms-container").css({
		"width": contentwidth,
		"height": (wh/10)*8,
		"margin-top": wh/10 
	});
	
	jQuery("#ajax-content .cms").css({
		"height": jQuery("#ajax-content .cms-container").height() - jQuery("#ajax-content .cms-container .title").height() * 2,
		"width": jQuery("#homecontent").width() - 10,
	});
	
	// resize mainpage and sub categories
	var width = jQuery("#homecontent").width();
	var height = jQuery("#homecontent").height() - jQuery("#header").height() - 30;

	var newpr = height / 100;
	var newWidth = newpr * 150;
	if(newWidth < width)
	{
		var cssWidth = newWidth;
	}
	else if ( width > height )
	{
		var cssWidth = "auto";
	}
	else if(newWidth >= width)
	{
		var cssWidth = "auto";
	}

	jQuery(".maincategories").css({"width": cssWidth , "height": height });
	jQuery("#subcategories-container").css({"width": cssWidth , "height": height });
	jQuery(".controller").css({"margin-top": jQuery(".slide-scroll").height() / 2})
	
	jQuery("#products").css({
		"width": jQuery(window).width() - jQuery("#slide-prev").width()*2 ,
		"padding-left":  jQuery("#slide-prev").width(),
		"padding-right":  jQuery("#slide-prev").width(),
	});
	
	
	jQuery("#products-scroll-container").css({"width": jQuery(".maincategories").width() });

	var slide_pages = Math.ceil( jQuery(".slide-content").find(".product-in-category").length / 6 );
	
	jQuery(".slide-scroll").css({"height": height + 50});
	jQuery("#products-container").css({"width": (jQuery(".slide-scroll").width()+5) * slide_pages })
	

	//setup product width / height
	var pcnt = jQuery(".slide-scroll").width() / 100;
	var pwidth = pcnt * 27;
	var new_slide_width = ( pwidth + pcnt*2 ) * 3;

	jQuery.each( jQuery(".product-in-category"), function(e){
		jQuery(this).css({ "width": pwidth, "margin":  pcnt * 1 });
	});
	
	// setup slide scroll width
	jQuery.each( jQuery(".slide"), function(e){
		jQuery(this).css({"width": new_slide_width });
	});
	
	
	var p10 = jQuery(window).height() / 10;
	var p15 = jQuery(window).height() / 100 * 15;
	
	jQuery("#products-scroll-container").css({"width": new_slide_width });
	jQuery(".product").css({"width": jQuery(".maincategories").width(),"height": jQuery(".maincategories").height() - p10 });
	

	jQuery("#product-related").css({"height": p15, "margin-top": p10 });

	// product
	var rel_item_pcnt = jQuery("#product .product").width() / 100;
	var rel_item_width = rel_item_pcnt * 13;
	var rel_item_full_width = rel_item_pcnt * 15;

	jQuery.each( jQuery(".related-product"), function(e){
		jQuery(this).css({ "width": rel_item_width, "margin":  rel_item_pcnt * 1 });
	});

	jQuery(".related-products-container").css({"height": p10, "width": rel_item_full_width * jQuery(".related-product").length + rel_item_pcnt,"margin-top": ((rel_item_width + rel_item_pcnt*2) / 2)  * -1 })

	jQuery("#sub-category-label").css({"width": jQuery(".slide-scroll").width() });


	// setup iphone differences
	


	var botline = parseInt(jQuery("#product-related").height()) + parseInt(jQuery("#product-related").css("margin-top")) + 20;
	
	var qheight = botline - jQuery("#qty").height() / 2;
	var cartheight = botline - jQuery(".product-right .product-add-to-cart").height() / 2;
	var rightwidth = jQuery(".product-right").width() / 100 ;
	jQuery("#qty").css({"position":"absolute", "bottom": qheight , "margin-left": ( jQuery(".product-left").width() - jQuery("#qty").width()  ) / 2 })
	jQuery(".product-right .product-add-to-cart").css({
		"position":"absolute", 
		"bottom": cartheight, 
		// "margin-left": ( jQuery(".product-right").width() - jQuery("#qty").width()  ) / 2, 
		"width":rightwidth*97,
		"padding":rightwidth
	})
	
	jQuery(".product-right .product-price").css({
		"position":"absolute", 
		"bottom": cartheight + jQuery(".product-right .product-add-to-cart").height(),
	});

	var pdheight = parseInt(jQuery(".maincategories").height()) - parseInt(jQuery(".product-right .product-title").height()) - parseInt(jQuery(".product-right .product-price").height()) - cartheight;

	jQuery(".product-right .product-description").css({
		"height": pdheight
	});
	
	setCustomIphoneCss();
}

function setCustomIphoneCss()
{
	// console.log("setCustomIphoneCss()")
	if(isiPhone()){
		// listing
		jQuery(".product-in-category .title").css({"font-size":"8px"})
		jQuery(".product-in-category .price").css({"font-size":"8px", "width":"50%"})
		
		// product page
		jQuery(".product-right .product-price").css({"font-size":"24px"})
		jQuery(".product-right .product-price .ext").css({"font-size":"12px"})
		jQuery(".product").css({"height": jQuery(".maincategories").height()})

		
	}
}

function addToCart( product )
{
	jQuery.ajax({
		"type": "POST",
		"url": "ajax/api.php",
		"dataType": "json",
		"data": { "method": "mage", "action":"cart", "do":"add", "data": product },
		beforeSend: function(){
			AjaxPreloader("start");
		}
	})
	.done(function( cartdata ) {
		AjaxPreloader("end");
		
		if(jQuery("#cart").css("display") == "none")
		{
			sectionToggle("cart");
		}
		
		generateCart();
	});
}


function removeFromCart( line )
{
	jQuery.ajax({
		"type": "POST",
		"url": "ajax/api.php",
		"dataType": "json",
		"data": { "method": "mage", "action":"cart", "do":"remove", "line": line },
		beforeSend: function(){
			AjaxPreloader("start");
		}
	})
	.done(function( cartdata ) {
		AjaxPreloader("end");
		
		if(jQuery("#cart").css("display") == "none")
		{
			sectionToggle("cart");
		}
		generateCart();
	});
}


function updateCartQuantities( )
{
	//
	var newData = Array();
	jQuery.each( jQuery("#cartcontent .row"), function(e){
		var cartline = { 
			"line_id": jQuery(this).find(".cart_item_id").val(),
			"qty": jQuery(this).find(".cart-qty").val(),
		};
		newData.push( cartline );
	});
	
	
	jQuery.ajax({
		"type": "POST",
		"url": "ajax/api.php",
		"dataType": "json",
		"data": { "method": "mage", "action":"cart", "do":"update", "newData": newData },
		beforeSend: function(){
			AjaxPreloader("start");
		}
	})
	.done(function( cartdata ) {
		AjaxPreloader("end");
		
		if(jQuery("#cart").css("display") == "none")
		{
			sectionToggle("cart");
		}
		generateCart();
	});
}



function generateCart()
{
	// get cart contents
	jQuery.ajax({
		"type": "POST",
		"url": "ajax/api.php",
		"dataType": "json",
		"data": { "method": "mage", "action":"cart", "do":"view"},
		beforeSend: function(){
			AjaxPreloader("start");
		}
	}).done(function( cartdata ) {
		AjaxPreloader("end");
		cart = cartdata;
		doGenCart(cartdata);
	});	
}

function doGenCart(cartdata)
{
	var total = cartdata.totals.subtotal;
	var tax = cartdata.totals.tax;
	var grand_total = cartdata.totals.grandtotal;

	var html = '';
	
	if(cartdata.items.length > 0)
	{
		jQuery(cartdata.items).each(function(e){
			html+= cartLine( e, jQuery(this) )
		});
	}
	else
	{
		html = '<div class="row">Your cart is empty!</div>';
	}

	jQuery("#cartcontent").html( html )
	
	jQuery(".removeItem").each(function(e){
		jQuery(this).bind("click", function(e){
			removeFromCart( jQuery(this).parent().parent().find(".cart_item_id").val() );
		})
	});
	
	
	jQuery("#cart_total").html( "$"+displayPrice( total ) );
	jQuery("#cart_tax").html( "$"+displayPrice( tax ) );
	jQuery("#cart_grand_total").html( "$"+displayPrice( grand_total ) );
}

function displayPrice( price )
{
	return parseFloat(Math.round(price * 100) / 100).toFixed(2);	
}

function cartLine( e, product )
{
	return '<div class="row">'+
		'<div class="co1"> &nbsp; <input type="hidden" name="cart_item_'+e+'" class="cart_item_id" value="'+e+'" /> </div>'+
		'<div class="co2">'+product.attr("product_name")+'</div>'+
		'<div class="co2">$'+displayPrice( product.attr("product_price") ) +'</div>'+
		'<div class="co2"><input type="text" class="cart-qty" value="'+product.attr("product_qty")+'" /></div>'+
		'<div class="co1">$'+displayPrice( product.attr("product_price")*product.attr("product_qty"))+'</div>'+
		'<div class="co2"> <a href="#/cart/remove/" class="removeItem"> <img src="ajax/skin/justcables/images/remove-icon.png" /> </a> </div>'+
	'</div>';
}


function AjaxPreloader(type)
{
	if(type == "start")
	{
		jQuery("#ajax-over").css({"display":"block"});
	}
	else
	{
		jQuery("#ajax-over").css({"display":"none"});
	}
}

function scrollToAnchor(aid){
	// console.log("scrollToAnchor["+aid+"]")
	// debugz( "scrollToAnchor("+aid+")" );
	var aTag = jQuery("a[id='anchor-"+ aid +"']");
	scrollHeightToElement('#wrapper', aTag );
}

function getActiveHeaderBar()
{
	var headers = Array(".responsive-block-lt-320", ".responsive-block-bt-320-600", ".responsive-block-gt-600");
	var headerTopBar = null
	for(var i=0; i < headers.length; i++){
		if(  jQuery(headers[i]).css("display") == "block" )
		{
			headerTopBar = headers[i];
		}
	}
	return jQuery(headerTopBar);
}

function scrollHeightToElement(container, zlink){
	
	HideContent(zlink); 
	
	if(jQuery("#cart").css("display") != "block" && jQuery("#search").css("display") != "block" && jQuery("#account").css("display") != "block")
	{
		var containerScroll = jQuery(container).scrollTop() - getActiveHeaderBar().height() - 30;
		var thisoffset = jQuery( zlink ).offset().top + containerScroll;
		jQuery(container).animate({scrollTop: thisoffset},'slow', null, function(){ 
			doneScrolling(); 
			setupScrolling(); 
		} );
	}
}

function scrollWidthToElement(scrollContainer, relativeContainer, zlink){
	
	// setupScrolling(); 
	
	// console.log("scrollWidthToElement scrollContainer["+scrollContainer+"] scrollContainer["+relativeContainer+"] zlink["+zlink+"]")
	var containerScroll = jQuery(scrollContainer).scrollLeft();
	// console.log(containerScroll)
	var thisoffset = ( jQuery( zlink ).offset().left ) + containerScroll - jQuery(".slide-scroll").position().left;
	// console.log(thisoffset)
	
	jQuery(scrollContainer).animate({scrollLeft: thisoffset},'slow',null, function(){ doneScrolling() } );
}

function doneScrolling()
{
	showSlideController("slide-prev", "left");
	showSlideController("slide-next", "right");
	
	// setTimeout( contentsetup() ,3000);
}

function HideContent(zlink)
{
	if(zlink.attr("id") != "anchor-products-page")
	{
		// hide product page elements
		jQuery("#qty").css({"display":"none"});
		jQuery(".product-right .product-add-to-cart").css({"display":"none"});
		jQuery(".product-right .product-price").css({"display":"none"});
	}
	// console.log(zlink);
}