<?php
    header('Content-type: text/css; charset: UTF-8');
    header('Cache-Control: must-revalidate');
    header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 3600) . ' GMT');
    $url = $_REQUEST['url'];
?>

.block_footer_social .social li a,
.block_footer_social .social li a:hover,
.form-search .input-text,
.form-search .button span,
button.button span,
.searchautocomplete .nav-submit-button .button,
.searchautocomplete .nav,
#subscribe-form .wrap-news .button span{behavior: url(<?php echo $url; ?>css/css3.htc);position:relative;}

.block_footer_social .social li a:hover,
.block_footer_social .social li a{border-radius:16px;}
button.button span,
.form-search .input-text{border-radius:2px;}
.form-search .button span,
.searchautocomplete .nav-submit-button .button,
#subscribe-form .wrap-news .button span{border-radius:0 2px 2px 0;}
.searchautocomplete .nav{2px 0 0 2px;}