<?php
/**
 * Abandoned Carts Alerts Pro
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Cartalert
 * @version      3.2.0
 * @license:     OgsKKa0Ma8ACwdAOgyDJq9fAWDGD2UM3W2DCc174eK
 * @copyright:   Copyright (c) 2013 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();

$quote = $this->getTable('sales/quote');

$installer->run("

ALTER TABLE `$quote` ADD `allow_alerts` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' ;

");

$installer->endSetup();