<?php
/**
 * Abandoned Carts Alerts Pro
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Cartalert
 * @version      3.2.0
 * @license:     OgsKKa0Ma8ACwdAOgyDJq9fAWDGD2UM3W2DCc174eK
 * @copyright:   Copyright (c) 2013 AITOC, Inc. (http://www.aitoc.com)
 */
$installer = $this;

$installer->startSetup();


$installer->run("

ALTER TABLE `".$installer->getTable('adjcartalert/cartalert')."` ADD `customer_group_id` INT(10) UNSIGNED DEFAULT NULL;

");

$installer->endSetup();