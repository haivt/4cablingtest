<?php
class Nowlive_Extrafee_Model_Observer
{
	public function invoiceSaveAfter(Varien_Event_Observer $observer)
	{
		$invoice = $observer->getEvent()->getInvoice();
		if ($invoice->getBaseExtrafeeAmount()) {
			$order = $invoice->getOrder();
			$order->setExtrafeeAmountInvoiced($order->getExtrafeeAmountInvoiced() + $invoice->getExtrafeeAmount());
			$order->setBaseExtrafeeAmountInvoiced($order->getBaseExtrafeeAmountInvoiced() + $invoice->getBaseExtrafeeAmount());
		}
		return $this;
	}
	public function creditmemoSaveAfter(Varien_Event_Observer $observer)
	{
		/* @var $creditmemo Mage_Sales_Model_Order_Creditmemo */
		$creditmemo = $observer->getEvent()->getCreditmemo();
		if ($creditmemo->getExtrafeeAmount()) {
			$order = $creditmemo->getOrder();
			$order->setExtrafeeAmountRefunded($order->getExtrafeeAmountRefunded() + $creditmemo->getExtrafeeAmount());
			$order->setBaseExtrafeeAmountRefunded($order->getBaseExtrafeeAmountRefunded() + $creditmemo->getBaseExtrafeeAmount());
		}
		return $this;
	}
	public function updatePaypalTotal($evt){
		$cart = $evt->getPaypalCart();
		$cart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_SUBTOTAL,$cart->getSalesEntity()->getExtrafeeAmount());
	}
	
	public function setFee($evt)
	{
		$quote = $evt->getQuote();
		// Mage::Log( "Observer: setFee[". $quote->getShippingJumpTheQ()."]" );
		if( $quote->getShippingJumpTheQ() == "true")
		{
			$quote->setExtrafeeAmount( Mage::getStoreConfig('nowlive/extrafee/jump_price') );
			$quote->setBaseExtrafeeAmount( Mage::getStoreConfig('nowlive/extrafee/jump_price') );
		}
	}
}