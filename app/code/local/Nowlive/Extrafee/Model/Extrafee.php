<?php
class Nowlive_Extrafee_Model_Extrafee extends Varien_Object{
	const FEE_AMOUNT = 0;

	public static function getExtrafee(){
		// return self::FEE_AMOUNT;
		return Mage::getStoreConfig('nowlive/extrafee/jump_price');
	}
	
	public static function canApply($address){
		
		if( Mage::getStoreConfig('nowlive/extrafee/enabled') )
		{
			// check quote for JumpTheQ
			$quote = Mage::getSingleton('checkout/session')->getQuote();
			if( $quote->getShippingJumpTheQ() == "true" )
			{
				return true;
			}
		}
		return false;
	}
}