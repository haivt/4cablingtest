<?php
class Nowlive_Extrafee_Model_Sales_Quote_Address_Total_Extrafee extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	protected $_code = 'extrafee';

	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		parent::collect($address);
		// Mage::Log( "[".$address->getId()."] : collect [".$address->getAddressType()."] [".$address->getExtrafeeAmount()."] START " );

		// Mage::Log( print_r( $address->debug(), true) );
		
		if($address->getAddressType() == "billing")
		{
			return $this;
		}
		
		// $this->_setAmount(0);
		// $this->_setBaseAmount(0);

		$quote = $address->getQuote();

		if(Nowlive_Extrafee_Model_Extrafee::canApply($address)) {
			// Mage::Log( "[".$address->getId()."] : collect canApply Yes!" );
			
			//$exist_amount = $quote->getExtrafeeAmount();
			//$balance = $fee - $exist_amount;
			// 			$balance = $fee;

			$fee = Nowlive_Extrafee_Model_Extrafee::getExtrafee();


			//$this->_setAmount($balance);
			//$this->_setBaseAmount($balance);

			$address->setExtrafeeAmount($fee);
			$address->setBaseExtrafeeAmount($fee);
				
			// $quote->setExtrafeeAmount($balance);
			// $quote->setBaseExtrafeeAmount($balance);

			$address->setGrandTotal($address->getGrandTotal() + $address->getExtrafeeAmount());
			$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseExtrafeeAmount());
		}
		
		// Mage::Log( "[".$address->getId()."] : collect [".$address->getAddressType()."] [".$address->getExtrafeeAmount()."] END " );

		
	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		// Mage::Log( "[".$address->getId()."] : fetch [".$address->getAddressType()."] [".$address->getExtrafeeAmount()."] " );
		
		// $quote = $address->getQuote();
		
		if($address->getAddressType() == "billing")
		{
			return $this;
		}
		
		if( Mage::getStoreConfig('nowlive/extrafee/enabled') )
		{
			$quote = $address->getQuote();
			
			if( $quote->getShippingJumpTheQ() == "true")
			{
				// $amt = Mage::getStoreConfig('nowlive/extrafee/jump_price');
				$amt = $address->getExtrafeeAmount();
				$address->addTotal(array(
						'code'=>$this->getCode(),
						'title'=>Mage::helper('extrafee')->__('Jump The Queue'),
						'value'=> $amt
				));
			}
		
		}
		
		return $this;
	}
}