<?php
class Nowlive_Extrafee_Model_Sales_Order_Total_Creditmemo_Extrafee extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
	{
		$order = $creditmemo->getOrder();
		$feeAmountLeft = $order->getExtrafeeAmountInvoiced() - $order->getExtrafeeAmountRefunded();
		$basefeeAmountLeft = $order->getBaseExtrafeeAmountInvoiced() - $order->getBaseExtrafeeAmountRefunded();
		if ($basefeeAmountLeft > 0) {
			$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $feeAmountLeft);
			$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $basefeeAmountLeft);
			$creditmemo->setExtrafeeAmount($feeAmountLeft);
			$creditmemo->setBaseExtrafeeAmount($basefeeAmountLeft);
		}
		return $this;
	}
}
