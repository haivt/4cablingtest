<?php
class Nowlive_Extrafee_Model_Sales_Order_Total_Invoice_Extrafee extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Invoice $invoice)
	{
		$order = $invoice->getOrder();
		$feeAmountLeft = $order->getExtrafeeAmount() - $order->getExtrafeeAmountInvoiced();
		$baseFeeAmountLeft = $order->getBaseExtrafeeAmount() - $order->getBaseExtrafeeAmountInvoiced();
		if (abs($baseFeeAmountLeft) < $invoice->getBaseGrandTotal()) {
			$invoice->setGrandTotal($invoice->getGrandTotal() + $feeAmountLeft);
			$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseFeeAmountLeft);
		} else {
			$feeAmountLeft = $invoice->getGrandTotal() * -1;
			$baseFeeAmountLeft = $invoice->getBaseGrandTotal() * -1;

			$invoice->setGrandTotal(0);
			$invoice->setBaseGrandTotal(0);
		}
			
		$invoice->setExtrafeeAmount($feeAmountLeft);
		$invoice->setBaseExtrafeeAmount($baseFeeAmountLeft);
		return $this;
	}
}
