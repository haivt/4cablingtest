<?php

$installer = $this;

$installer->startSetup();

$installer->run("
		ALTER TABLE  `".$this->getTable('sales/quote')."` ADD  `extrafee_amount` DECIMAL( 10, 2 ) NOT NULL;
		ALTER TABLE  `".$this->getTable('sales/quote')."` ADD  `base_extrafee_amount` DECIMAL( 10, 2 ) NOT NULL;
    ");

$installer->endSetup(); 