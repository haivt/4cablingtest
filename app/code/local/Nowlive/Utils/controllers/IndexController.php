<?php
class Nowlive_Utils_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    }
	
	public function closeBannerAction()
	{
		$session = Mage::getSingleton('customer/session');
		$session->setBannerState("closed");
		echo json_encode(array("response" => "ok"));
	}
}