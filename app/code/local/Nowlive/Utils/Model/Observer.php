<?php
class Nowlive_Utils_Model_Observer
{

	/**
	 * Attach posted shipping options to quote object
	 * @param   Varien_Event_Observer $observer
	 * @return  Nowlive_Utils_Model_Observer
	 */
	public function setShippingOptions($observer)
	{
		if( Mage::getStoreConfig('nowlive/utils/enabled') )
		{
			if($observer->getEvent()->getRequest()->isPost()) {
				
				$quote = $observer->getEvent()->getQuote();
				$method = $observer->getEvent()->getRequest()->getPost('shipping_method', '');
				if( substr($method , 0 , strlen('premiumzone_Courier_Delivery_') ) == 'premiumzone_Courier_Delivery_' )
				{
					$quote->setShippingJumpTheQ( $observer->getEvent()->getRequest()->getPost('shipping_jump_the_q', 'false') );
					$quote->setShippingFreightAuthority( $observer->getEvent()->getRequest()->getPost('shipping_freight_authority', 'false') );
					
					if($observer->getEvent()->getRequest()->getPost('shipping_jump_the_q', 'false') == "true")
					{
						$this->_addProductToCart("FRUSH");
					}
					else
					{
						$this->_removeProductInCart("FRUSH");
					}

					if($observer->getEvent()->getRequest()->getPost('shipping_freight_authority', 'false') == "true")
					{
						$this->_addProductToCart("FAUTHORITY");
					}
					else
					{
						$this->_removeProductInCart("FAUTHORITY");
					}
				}
				else
				{
					$quote->setShippingJumpTheQ( 'false' );
					$quote->setShippingFreightAuthority( 'false' );
					
					$this->_removeProductInCart("FRUSH");
					$this->_removeProductInCart("FAUTHORITY");
				}
				
				$quote->collectTotals()->save();
			}
		}
		return $this;
	}
	
	public function updateItems($observer)
	{
		// Mage::Log("updateItems");
		return $this->setRules($observer);
	}
	
	/*
		>$200 Order Add Free Product SKU into Cart Automatically 
	*/
	public function setRules($observer) 
	{
		// Old product SKU
		$prev_product_sku = "GWP.FEB"; 
		
		// New / Current Product SKU
		$product_sku = "GWP.MAR"; 
		$over_price = 150;
		
		// check and remove previous offer product if present in the cart
		if($this->_checkProductInCart($prev_product_sku))
		{
			$this->_removeProductInCart($prev_product_sku);
		}

		// comment or remove the next line to enable the functionality
		// return $this;
		
		$quote = $observer->getEvent()->getCart()->getQuote();		
		
		// $this->_removeProductInCart($product_sku);
		

		// if subtotal is greater than 149 add that item
		if($quote->getSubtotal() > $over_price && !$this->_checkProductInCart("FRUSH") )
		{
			if(!$this->_checkProductInCart($product_sku))
			{
				$this->_addProductToCart($product_sku);
			}
		}
		else if($quote->getSubtotal() > ($over_price + 14.25) && $this->_checkProductInCart("FRUSH") )
		{
			if(!$this->_checkProductInCart($product_sku))
			{
				$this->_addProductToCart($product_sku);
			}
		}
		else if( $this->_checkProductInCart($product_sku) && $this->_checkProductInCart("FRUSH") && $quote->getSubtotal() < ($over_price + 14.25) )
		{
			$this->_removeProductInCart($product_sku);
		}
		else
		{
			// remove it
			$this->_removeProductInCart($product_sku);
		}

		$quote->save();
		return $this;
	}
	
	public function _addProductToCart($product_sku, $force_insertion = false)
	{
		$product_add = Mage::getModel('catalog/product');
		$product_id = $product_add->getIdBySku($product_sku);
		if (!$this->_checkProductInCart($product_sku))
		{
			if ($product_id)
			{
				try {
					$product_add->load($product_id);
					$cart = Mage::getSingleton('checkout/cart');
					$cart->addProduct($product_add, array('qty' => 1));
				} catch (Exception $e) {
					Mage::Log("_addProductToCart error => ".$e->getMessage());
				}
			}
		}
	}
	
	public function _checkProductInCart($sku)
	{
		$cartHelper = Mage::helper('checkout/cart');
		$items = $cartHelper->getCart()->getItems();
	
		foreach ($items as $_item)
		{
			$productId = $_item->getProductId();
			$products = Mage::getModel('catalog/product')->load($productId);

			$this->cartException = '1000000013702|2';
			
			$cart_ex_array = explode('|',$this->cartException);
			$dontTopUp = true;
	
			if ($_item->getQty() == $cart_ex_array[1] && $cart_ex_array[0] == $products->getSku())
			{
				$dontTopUp = false;
			} else {
				//cannot process...
			}
	
			//Mage::Log("_checkProductInCart->[".$sku."] now [".$products->getSku()."]");
			if($products->getSku() == $sku && $dontTopUp){
				return true;
			}
		}
		return false;
	}
	
	public function _removeProductInCart($product_sku)
	{
		if ($product_sku)
		{
			$product_add = Mage::getModel('catalog/product');
			$productId = $product_add->getIdBySku($product_sku);
	
			$cartHelper = Mage::helper('checkout/cart');
			$items = $cartHelper->getCart()->getItems();
	
			foreach ($items as $item) {
				if ($item->getProduct()->getId() == $productId) 
				{
					$itemId = $item->getItemId();
					$cartHelper->getCart()->removeItem($itemId);
				}
			}
		}
			
	}
}
?>
