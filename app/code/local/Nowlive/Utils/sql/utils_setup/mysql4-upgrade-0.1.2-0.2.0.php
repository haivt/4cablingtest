<?php

$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('utils')};
CREATE TABLE {$this->getTable('utils')} (
  `utils_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` varchar(255) NOT NULL default '',
  `redeem` smallint(6) NOT NULL default '0',
  `understand`smallint(6) NOT NULL default '0',
  `terms` smallint(6) NOT NULL default '0',
  `points` int(11) NOT NULL default '0',
  `eta` int(11) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`utils_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 