<?php

    $installer = $this;
    $installer->startSetup();
    
    $newFields = array(
		'shipping_freight_authority' => array(
            'type'              => 'text',
            'label'                => 'Shipping - Freight Authority'        
        ),
        'shipping_jump_the_q' => array(
            'type'              => 'text',
            'label'                => 'Shipping - Jump the Queue'        
        )
    );
    
    $entities = array('order', 'quote');
    
    $setup = new Mage_Sales_Model_Mysql4_Setup('core_setup');
    foreach($newFields as $attributeName => $attributeDefs) 
	{
        foreach ($entities as $entity) 
		{
            $setup->addAttribute($entity, $attributeName, array(
                'position'          => 1,
                'type'              => $attributeDefs['type'],
                'label'             => $attributeDefs['label'],
                'global'            => 1,
                'visible'           => 1,
                'required'          => 0,
                'user_defined'      => 1,
                'searchable'        => 0,
                'filterable'        => 0,
                'comparable'        => 0,
                'visible_on_front'  => 1,
                'visible_in_advanced_search' => 0,
                'unique'            => 0,
                'is_configurable'   => 0,
                'position'          => 1,
            ));                
        }
    }
    
	// Mage::Log("nowlive:: setup finished");
    $installer->endSetup();
