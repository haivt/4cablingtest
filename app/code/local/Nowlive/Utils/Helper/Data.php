<?php
class Nowlive_Utils_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getMethodLabel($product_sku)
	{
		if( Mage::getStoreConfig('nowlive/utils/enabled') )
		{
			$var = "";
			
			if($product_sku == "FAUTHORITY")
			{
				$var = "freight";
			}
			else if($product_sku == "FRUSH")
			{
				$var = "jumptheq";
			}
			
			if( Mage::getStoreConfig('nowlive/utils/'.$var.'_product_title') )
			{
				$product_add = Mage::getModel('catalog/product');
				$product_id = $product_add->getIdBySku($product_sku);
				$product = Mage::getModel('catalog/product')->load($product_id);
				return $product->getName();
			}
			else
			{
				return Mage::getStoreConfig('nowlive/utils/'.$var.'_product_other');
			}
		}
	}
	
	public function customerHasRedeemRequest($customer_id)
	{
		$date = date('Y', time()).date('m', time())."00";
		
		$collection = Mage::getModel('utils/utils')
						->getCollection()
						->addFieldToFilter("customer_id", array ("eq" => $customer_id ) )
						// points field actually holds the timestamp.. ?!!?
						->addFieldToFilter('points', array("gt" => $date ) );

		// echo $collection->getSelect();				

		$items = 0;
		foreach($collection as $item)
		{
			$items++;
		}
		
		if($items == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}