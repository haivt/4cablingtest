<?php
class Nowlive_Banner_Block_Adminhtml_Banner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
	  $transition_options = array(
		  array(
			  'value'     => 'linear',
			  'label'     => Mage::helper('banner')->__('linear'),
		  ),
		  array(
			  'value'     => 'swing',
			  'label'     => Mage::helper('banner')->__('swing'),
		  ),
		  array(
			  'value'     => 'jswing',
			  'label'     => Mage::helper('banner')->__('jswing'),
		  ),
		  array(
			  'value'     => 'easeInQuad',
			  'label'     => Mage::helper('banner')->__('easeInQuad'),
		  ),
		  array(
			  'value'     => 'easeInCubic',
			  'label'     => Mage::helper('banner')->__('easeInCubic'),
		  ),
		  array(
			  'value'     => 'easeInQuart',
			  'label'     => Mage::helper('banner')->__('easeInQuart'),
		  ),
		  array(
			  'value'     => 'easeInQuint',
			  'label'     => Mage::helper('banner')->__('easeInQuint'),
		  ),
		  array(
			  'value'     => 'easeOutSine',
			  'label'     => Mage::helper('banner')->__('easeOutSine'),
		  ),
		  array(
			  'value'     => 'easeInExpo',
			  'label'     => Mage::helper('banner')->__('easeInExpo'),
		  ),
		  array(
			  'value'     => 'easeInCirc',
			  'label'     => Mage::helper('banner')->__('easeInCirc'),
		  ),
		  array(
			  'value'     => 'easeInElastic',
			  'label'     => Mage::helper('banner')->__('easeInElastic'),
		  ),
		  array(
			  'value'     => 'easeInBack',
			  'label'     => Mage::helper('banner')->__('easeInBack'),
		  ),
		  array(
			  'value'     => 'easeInBounce',
			  'label'     => Mage::helper('banner')->__('easeInBounce'),
		  ),
		  array(
			  'value'     => 'easeOutQuad',
			  'label'     => Mage::helper('banner')->__('easeOutQuad'),
		  ),
		  array(
			  'value'     => 'easeOutCubic',
			  'label'     => Mage::helper('banner')->__('easeOutCubic'),
		  ),
		  array(
			  'value'     => 'easeOutQuart',
			  'label'     => Mage::helper('banner')->__('easeOutQuart'),
		  ),
		  array(
			  'value'     => 'easeOutQuint',
			  'label'     => Mage::helper('banner')->__('easeOutQuint'),
		  ),
		  array(
			  'value'     => 'easeOutSine',
			  'label'     => Mage::helper('banner')->__('easeOutSine'),
		  ),
		  array(
			  'value'     => 'easeOutExpo',
			  'label'     => Mage::helper('banner')->__('easeOutExpo'),
		  ),
		  array(
			  'value'     => 'easeOutCirc',
			  'label'     => Mage::helper('banner')->__('easeOutCirc'),
		  ),
		  array(
			  'value'     => 'easeOutBack',
			  'label'     => Mage::helper('banner')->__('easeOutBack'),
		  ),
		  array(
			  'value'     => 'easeOutBounce',
			  'label'     => Mage::helper('banner')->__('easeOutBounce'),
		  ),
		  array(
			  'value'     => 'easeInOutQuad',
			  'label'     => Mage::helper('banner')->__('easeInOutQuad'),
		  ),
		  array(
			  'value'     => 'easeInOutCubic',
			  'label'     => Mage::helper('banner')->__('easeInOutCubic'),
		  ),
		  array(
			  'value'     => 'easeInOutQuart',
			  'label'     => Mage::helper('banner')->__('easeInOutQuart'),
		  ),
		  array(
			  'value'     => 'easeInOutQuint',
			  'label'     => Mage::helper('banner')->__('easeInOutQuint'),
		  ),
		  array(
			  'value'     => 'easeInOutSine',
			  'label'     => Mage::helper('banner')->__('easeInOutSine'),
		  ),
		  array(
			  'value'     => 'easeInOutExpo',
			  'label'     => Mage::helper('banner')->__('easeInOutExpo'),
		  ),
		  array(
			  'value'     => 'easeInOutCirc',
			  'label'     => Mage::helper('banner')->__('easeInOutCirc'),
		  ),
		  array(
			  'value'     => 'easeInOutElastic',
			  'label'     => Mage::helper('banner')->__('easeInOutElastic'),
		  ),
		  array(
			  'value'     => 'easeInOutBack',
			  'label'     => Mage::helper('banner')->__('easeInOutBack'),
		  ),
		  array(
			  'value'     => 'easeInOutBounce',
			  'label'     => Mage::helper('banner')->__('easeInOutBounce'),
		  ),
	  );
	  
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('banner_settings', array('legend'=>Mage::helper('banner')->__('Banner Settings')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('banner')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('url', 'text', array(
          'label'     => Mage::helper('banner')->__('Click / Post URL'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'url',
      ));

      $fieldset->addField('sort_order', 'text', array(
          'label'     => Mage::helper('banner')->__('Order'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'sort_order',
      ));
	  
      $eventElem = $fieldset->addField('type', 'select', array(
          'label'     => Mage::helper('banner')->__('Banner Type'),
          'name'      => 'type',
          'class'     => 'required-entry',
          'required'  => true,
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('Image'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('banner')->__('Custom CMS'),
              ),
          ),
		  'onchange'=>'setFormOptions(this)',
      ));

      $eventElem->setAfterElementHtml('<script>
          function setFormOptions(selectElem){
			  
			  if(selectElem.getValue() == 1)
			  {
				  // image
				  $("filename_1").enable();
				  $("filename_2").enable();
				  $("filename_3").enable();
				  $("filename_4").enable();
				  $("customcontent").disable();
			  }
			  else if(selectElem.getValue() == 2)
			  {
				  // custom code
				  $("filename_1").disable();
				  $("filename_2").disable();
				  $("filename_3").disable();
				  $("filename_4").disable();  
				  $("customcontent").enable();
			  }
          }
      </script>
	  ');

	
		$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		$fieldset->addField('start_date', 'date', array(
		  'name'   => 'start_date',
		  'label'  => Mage::helper('banner')->__('Start Date'),
		  'title'  => Mage::helper('banner')->__('Start Date'),
		  'image'  => $this->getSkinUrl('images/grid-cal.gif'),
		  'input_format' => $dateFormatIso,
		  'format'       => $dateFormatIso,
		  'time' => true
		));
		
		$eventElem = $fieldset->addField('end_date', 'date', array(
		  'name'   => 'end_date',
		  'label'  => Mage::helper('banner')->__('End Date'),
		  'title'  => Mage::helper('banner')->__('End Date'),
		  'image'  => $this->getSkinUrl('images/grid-cal.gif'),
		  'input_format' => $dateFormatIso,
		  'format'       => $dateFormatIso,
		  'time' => true
		));
		
		
	    $eventElem->setAfterElementHtml('<br />Current Server Date / Time:'.date("Y-m-d h:i:s A").'');

		
	
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('banner')->__('Status'),
          'name'      => 'status',
          'class'     => 'required-entry',
          'required'  => true,
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('banner')->__('Disabled'),
              ),
          ),
      ));
     

      $fieldset = $form->addFieldset('banner_data', array('legend'=>Mage::helper('banner')->__('Banner Data')));

      $fieldset->addField('filename_1', 'file', array(
          'label'     => Mage::helper('banner')->__('Image - 1120x120'),
          'required'  => false,
          'name'      => 'filename_1',
	  ));
	  
	  $this->displayImage($fieldset, "filename_1");
	  
      $fieldset->addField('filename_2', 'file', array(
          'label'     => Mage::helper('banner')->__('Image - 1024x768'),
          'required'  => false,
          'name'      => 'filename_2',
	  ));

	  $this->displayImage($fieldset, "filename_2");

      $fieldset->addField('filename_3', 'file', array(
          'label'     => Mage::helper('banner')->__('Image - 800x600'),
          'required'  => false,
          'name'      => 'filename_3',
	  ));

	  $this->displayImage($fieldset, "filename_3");


      $fieldset->addField('filename_4', 'file', array(
          'label'     => Mage::helper('banner')->__('Image - 320x480'),
          'required'  => false,
          'name'      => 'filename_4',
	  ));
	  
	  $this->displayImage($fieldset, "filename_4");


      $fieldset->addField('transition_in', 'select', array(
          'label'     => Mage::helper('banner')->__('Transition'),
          'name'      => 'transition_in',
          'values'    => $transition_options
      ));
	  
      $fieldset->addField('transition_time', 'text', array(
          'label'     => Mage::helper('banner')->__('Transition Time'),
          'name'      => 'transition_time',
      ));
	  
      $fieldset->addField('transition_element', 'select', array(
          'label'     => Mage::helper('banner')->__('Transition Element'),
          'name'      => 'transition_element',
          'class'     => 'required-entry',
          'required'  => true,
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('width'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('banner')->__('height'),
              ),
			  
              array(
                  'value'     => 3,
                  'label'     => Mage::helper('banner')->__('opacity'),
              ),
          ),
      ));
     
	  
      $eventElem = $fieldset->addField('customcontent', 'editor', array(
          'name'      => 'customcontent',
          'label'     => Mage::helper('banner')->__('Content'),
          'title'     => Mage::helper('banner')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
	  
	  $eventElem->setAfterElementHtml('<br /><strong>use #DST_URL# as link or post action.</strong><script>setFormOptions($("type"));</script>');

     
      if ( Mage::getSingleton('adminhtml/session')->getBannerData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBannerData());
          Mage::getSingleton('adminhtml/session')->setBannerData(null);
      } elseif ( Mage::registry('banner_data') ) {
          $form->setValues(Mage::registry('banner_data')->getData());
      }
      return parent::_prepareForm();
  }
  
  public function displayImage($fieldset, $field)
  {

  	  $bannerId = $this->getRequest()->getParam('id');
	  if($bannerId)
	  {
	  	  $thisModel = Mage::getModel('banner/banner')->load($bannerId);	  	  
		  $method = "get".$this->getMethodFromFieldname($field);
	  	  $file = $thisModel->$method();
	  	  $mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."topbanner/";
	  	  
		  if($file)
		  {
				$fieldset->addField('img_'.$field, 'note', array(
					'label'		=> Mage::helper('banner')->__('Current Image'),
					'required' 	=> false,
					'text' 		=> '<img src="'.$mediaUrl.$file.'" style="'.$this->getStyle($field).'"/><br />'.Mage::helper('banner')->__('Image available at').' <a href="'.$mediaUrl.$file.'">'.$mediaUrl.$file.'</a>',			  	
				));
		  }
	  }
  }
  
  public function getStyle($fieldname)
  {
	  $bits = explode("_", $fieldname);
	  $id = $bits[count($bits)-1];
	  
	  $styles = array(
		  1 => "width:1120px; height:120px; display:block",
		  2 => "width:1024px; height:768px; display:block",
		  3 => "width:800px; height:600px; display:block",
		  4 => "width:320px; height:480px; display:block",
	  );
	  return $styles[$id];	  
  }
  
  public function getMethodFromFieldname($fieldname)
  {
	  $bits = explode("_", $fieldname);
	  $retval = "";
	  foreach($bits as $b)
	  {
		  if(is_numeric($b))
		  {
			  $retval.="_".$b;
		  }
		  else
		  {
			  $retval.=ucfirst($b);
		  }
	  }
	  return $retval;
  }
}