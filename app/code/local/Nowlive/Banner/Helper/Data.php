<?php
class Nowlive_Banner_Helper_Data extends Mage_Core_Helper_Abstract
{
	function formatMysqlDate($date)
	{
		// d/m/y h:i:s
		// 1/04/2013 05:38 AM
		$td = explode(" ", $date);
		$d = explode("/", $td[0]);
		$newDate = $d[1]."/".$d[0]."/".$d[2]." ".$td[1]." ".$td[2];
		
		// $time = strtotime($date);
		$time = strtotime($newDate);
		return date("Y-m-d H:i:s", $time);
	}
}