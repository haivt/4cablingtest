<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('banner')};
CREATE TABLE {$this->getTable('banner')} (
  `banner_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `type` smallint(6) NOT NULL default '0',
  `filename_1` varchar(255) NOT NULL default '',
  `filename_2` varchar(255) NOT NULL default '',
  `filename_3` varchar(255) NOT NULL default '',
  `filename_4` varchar(255) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  `customcontent` text NOT NULL,
  `transition_in` varchar(255) NOT NULL default '',
  `transition_time` int(11) NOT NULL default '0',
  `transition_element` varchar(255) NOT NULL default '',
  `sort_order` int(11) NOT NULL default '0',
  `status` smallint(6) NOT NULL default '0',
  `start_date` datetime NULL,
  `end_date` datetime NULL,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 