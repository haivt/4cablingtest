<?php

$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE `".$this->getTable('loyalty_transactions')."` ADD `extras` varchar (255) NULL AFTER `extra` ;
");

$installer->endSetup();