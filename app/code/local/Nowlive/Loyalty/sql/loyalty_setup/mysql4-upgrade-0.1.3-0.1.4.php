<?php

$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE `".$this->getTable('loyalty_transactions')."` CHANGE `type` `info` smallint( 6 ) NOT NULL;
ALTER TABLE `".$this->getTable('loyalty_transactions')."` ADD  `type` smallint ( 6 ) NOT NULL AFTER `customer_id` ;
");

$installer->endSetup();