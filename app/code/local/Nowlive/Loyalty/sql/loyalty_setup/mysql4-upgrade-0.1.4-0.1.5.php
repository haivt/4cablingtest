<?php

$installer = $this;
$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('loyalty_referal')};
CREATE TABLE {$this->getTable('loyalty_referal')} (
  `ref_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) unsigned NOT NULL,
  `refering_email` varchar(255) NOT NULL,
  PRIMARY KEY (`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");


$installer->endSetup();