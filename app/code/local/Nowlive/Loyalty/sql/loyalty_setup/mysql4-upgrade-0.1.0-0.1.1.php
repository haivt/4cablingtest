<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('loyalty_transactions')};
CREATE TABLE {$this->getTable('loyalty_transactions')} (
  `transaction_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) unsigned NOT NULL,
  `type` smallint(6) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `status` smallint(6),
  `extra` LONGTEXT NULL,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('loyalty_index')};
CREATE TABLE {$this->getTable('loyalty_index')} (
  `index_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`index_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");


$installer->endSetup(); 