<?php

$installer = $this;
$installer->startSetup();
$installer->run("
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;

	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `base_loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;

	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `loyalty_amount_invoiced` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_loyalty_amount_invoiced` DECIMAL( 10, 2 ) NOT NULL;

	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `base_loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;

	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `loyalty_amount_refunded` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_loyalty_amount_refunded` DECIMAL( 10, 2 ) NOT NULL;
	
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `base_loyalty_amount` DECIMAL( 10, 2 ) NOT NULL;
");

$installer->endSetup();