<?php

$installer = $this;
$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('loyalty_giftcard')};
CREATE TABLE {$this->getTable('loyalty_giftcard')} (
  `giftcard_id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) unsigned NOT NULL,
  `type` smallint(6) unsigned NOT NULL,
  `edata` LONGTEXT NULL,
  `status` smallint(6),
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`giftcard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();