<?php
class Nowlive_Loyalty_WestfieldController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/loyalty?id=15 
    	 *  or
    	 * http://site.com/loyalty/id/15 	
    	 */
    	/* 
		$loyalty_id = $this->getRequest()->getParam('id');

  		if($loyalty_id != null && $loyalty_id != '')	{
			$loyalty = Mage::getModel('loyalty/loyalty')->load($loyalty_id)->getData();
		} else {
			$loyalty = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($loyalty == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$loyaltyTable = $resource->getTableName('loyalty');
			
			$select = $read->select()
			   ->from($loyaltyTable,array('loyalty_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$loyalty = $read->fetchRow($select);
		}
		Mage::register('loyalty', $loyalty);
		*/




			// $msg = Mage::getSingleton('core/sesssion')->getMessagesBlock()->getMessages(true);              
			
			
			$this->loadLayout();     

			// $this->getLayout()->getMessagesBlock()->addMessages($msg);             

			$this->_initLayoutMessages('core/session');
			$this->renderLayout();
			
			
		}
		else
		{
			Mage::getSingleton('core/session')->addError($this->__('Restricted access, please login.'));
			$this->_redirect('/');	
		}
    }
	
	public function dorequestAction()
	{
		// echo "<pre>";
		// print_r(Mage::getSingleton('core/session')->getData());
		// die();
		$post = $this->getRequest()->getPost();
		$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
		
		if($customer_id > 0 && $post["terms"] == 1)
		{
		
			$helper = Mage::helper('loyalty');
			$availableOptions = $helper->getWestfieldOptions();
			$pts = $helper->getMyPointsSetup();
			$PointsAvailable = $pts["available"];
			
			$qtykeys = array();
			foreach($availableOptions as $ga)
			{
				$k = "wf_qty_".$ga["pts"];
				if(isset($post[$k]) && $post[$k] > 0)
				{
					$qtykeys[] = array( "key" => $ga["pts"], "val" => $post[$k] );
				}
			}
	
			$PointsSpent = 0;
			foreach($qtykeys as $item)
			{
				$PointsSpent+= $item["key"] * $item["val"];
			}
			
			if($PointsSpent < $PointsAvailable)
			{
				// ok do requests!
				
				$giftcard_ids = array();
				
				// add giftcard record
				$data = array(
					"customer_id" => $customer_id,
					"type" => 1,
					"edata" => serialize($giftcard_ids),
					"status" => 1,
				);
				
				$model = Mage::getModel('loyalty/giftcard');		
				$model->setData($data);
				
				if ($model->getCreatedTime() == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())->setUpdateTime(now());
				}
				else {
					$model->setUpdateTime(now());
				}	

				$model->save();

				$extra_id = $model->getGiftcardId();
				$html = '<table width="100%" border="1" cellpadding="0" cellspacing="0">'."\n";
				$html.= '	<tr>'."\n";
				$html.= '		<td align="center" style="width:10%; padding:2px"><strong>Quantity</strong></td>'."\n";
				$html.= '		<td align="left" style="padding:2px"><strong>Label</strong></td>'."\n";
				$html.= '		<td align="center" style="width:10%; padding:2px"><strong>Points Total</strong></td>'."\n";
				$html.= '	</tr>'."\n";

				foreach($qtykeys as $item)
				{
					$PointsSpent = $item["key"] * $item["val"];
					$label = $item["val"]." x ".$helper->getWestfieldLabelByPts($item["key"]);
					//echo $label."\n";
					$giftcard_ids[] = Mage::helper('loyalty')->newTransaction( $customer_id , 2 , 51, $PointsSpent, 1, $label, $extra_id);
					$html.= '	<tr>'."\n";
					$html.= '		<td style="padding:2px">'.$item["val"].'</td>'."\n";
					$html.= '		<td style="padding:2px">'.$label.'</td>'."\n";
					$html.= '		<td style="padding:2px">'.$PointsSpent.'</td>'."\n";
					$html.= '	</tr>'."\n";
				}
				$html.= '</table>'."\n";
				
				// print_r($giftcard_ids);
				$model->setEdata(serialize($giftcard_ids));
				$model->save();
				
				Mage::getSingleton('core/session')->addSuccess($this->__('Westfield Gift Card request sent.'));
				
				// set the email template data.				
				$postObject = new Varien_Object();
				$postObject->setData( array() );
				
				$customer = Mage::getSingleton('customer/session')->getCustomer();
				$postObject->setCustomerFirstname($customer->getFirstname());
				$postObject->setCustomerLastname($customer->getLastname());
				$postObject->setCustomerEmail($customer->getEmail());
				$postObject->setTransactions($html);

				// echo $html;
				
				// shoud we send the customer an email informing them that about their request ?!
				if( Mage::getStoreConfig('loyalty/templates/email_template_giftcard_enabled') == 1)
				{
					// send email to customer
					try {
						$helper->sendEmail("giftcard_customer", $customer->getEmail(), $postObject);
					}
					catch (Exception $e) { 
						//
						// echo $e->getMessage();
					}
				}

				// send email to sales 
				
				// send email to customer
				try {
					$helper->sendEmail("giftcard_sales", Mage::getStoreConfig('contacts/email/recipient_email'), $postObject);
				}
				catch (Exception $e) { 
					//
					// echo $e->getMessage();
				}
				

			}
			else
			{
				// spending more points than available?!
				// maby douple post!
				Mage::getSingleton('core/session')->addError($this->__('You are trying to spend more points than are available! Please make sure you did not already post this form, and if you have encountered errors please contact the support team.'));
	
			}
			
		}
		else
		{
			Mage::getSingleton('core/session')->addError($this->__("You must read and agree with 4Cabling Loyalty Program terms and conditions."));
		}
		$this->_redirect('loyalty/westfield');

	}
	
	public function updateAction()
	{
		if($this->getRequest()->getParam("spend_points") != NULL)
		{
			// $quote = Mage::getSingleton('checkout/cart')->getQuote();
			$discount = $this->getRequest()->getParam("spend_points");
			$helper =  Mage::helper('loyalty');
			$my_points = $helper->getMyPoints();
			if($my_points >= $discount )
			{
				// we have the points to spend. ok!
				
				// let's check if we can spend as many as the user wants
				if($discount >= $helper->getPointsInCart())
				{
					$discount = $helper->getPointsInCart();
				}
				
				
				$session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
				$session->setData("spend_points", $discount);
				
				/*
				$quote = Mage::helper('checkout/cart')->getQuote();
				// ok we got that enough points to spend
				// add the "LP" product into our order
				$helper->_addProductToCart("LOYALTY.POINTS", $helper->getCurrencyForPoints( $discount ) , true);
				$quote->save();
				*/
			}
			
			
		}
		$this->_redirect('checkout/cart/');
	}
}