<?php
class Nowlive_Loyalty_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/loyalty?id=15 
    	 *  or
    	 * http://site.com/loyalty/id/15 	
    	 */
    	/* 
		$loyalty_id = $this->getRequest()->getParam('id');

  		if($loyalty_id != null && $loyalty_id != '')	{
			$loyalty = Mage::getModel('loyalty/loyalty')->load($loyalty_id)->getData();
		} else {
			$loyalty = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($loyalty == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$loyaltyTable = $resource->getTableName('loyalty');
			
			$select = $read->select()
			   ->from($loyaltyTable,array('loyalty_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$loyalty = $read->fetchRow($select);
		}
		Mage::register('loyalty', $loyalty);
		*/

			
			$this->loadLayout();     
			$this->renderLayout();
		}
		else
		{
			Mage::getSingleton('core/session')->addError($this->__('Restricted access, please login.'));
			$this->_redirect('/');	
		}
    }
	
	public function updateAction()
	{
		if($this->getRequest()->getParam("spend_points") != NULL)
		{
			// $quote = Mage::getSingleton('checkout/cart')->getQuote();
			$discount = $this->getRequest()->getParam("spend_points");
			$helper =  Mage::helper('loyalty');
			$my_points = $helper->getMyPoints();
			if($my_points >= $discount )
			{
				// we have the points to spend. ok!
				
				// let's check if we can spend as many as the user wants
				if($discount >= $helper->getPointsInCart())
				{
					$discount = $helper->getPointsInCart();
				}
				
				
				$session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
				$session->setData("spend_points", $discount);
				
				/*
				$quote = Mage::helper('checkout/cart')->getQuote();
				// ok we got that enough points to spend
				// add the "LP" product into our order
				$helper->_addProductToCart("LOYALTY.POINTS", $helper->getCurrencyForPoints( $discount ) , true);
				$quote->save();
				*/
			}
			
			
		}
		$this->_redirect('checkout/cart/');
	}
	
	public function referAction()
	{
		if($this->getRequest()->getParam("to_invite") != NULL)
		{
			$data = trim(ltrim(rtrim($this->getRequest()->getParam("to_invite"))));
			$lines = explode("\n", $data);
			$ok = array();
			foreach($lines as $email)
			{
				$email = ltrim(rtrim(trim($email)));
				// check if it's a valid email!
				if (  Mage::helper('loyalty')->checkEmailAddress($email) ) {

					// check if email is not already in the referral list
					if( !Mage::helper('loyalty')->hasBeenReferred($email) )
					{
						$ok[] = $email;
					}
					else
					{
						Mage::getSingleton('core/session')->addError($this->__('Email: '.$email.' already refered by you or someone else.'));
					}
				}
				else
				{
					Mage::getSingleton('core/session')->addError($this->__('Email: '.$email.' did not pass validation.'));
				}
			}
			
			foreach($ok as $email)
			{
				$newdata = array(
					"customer_id" => Mage::getSingleton('customer/session')->getCustomer()->getId(),
					"refering_email" => $email,
				);
				Mage::getModel('loyalty/referal')->setData( $newdata )->save();
				
				Mage::getSingleton('core/session')->addSuccess($this->__('Email: '.$email.' added.'));

			}
		}
		$this->_redirect('loyalty/index/');
	}
	
	
	
	
	public function testAction()
	{
		
		echo "[".Mage::helper('loyalty')->checkEmailAddress("zuzu@nowlive.ro")."]";
		 
		/*
		// first check if the customer is logged in
		if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
			// so we need to create a new points transaction to the order creating customer
			$maxPrice = 0;
			$order = Mage::getModel("sales/order")->load(2);
			foreach($order->getAllItems() as $item) {
				// Do what you want
				$maxPrice += $item->getPrice() * $item->getQtyOrdered();
			}
			
			$points = Mage::helper('loyalty')->getPointsToCurrency($maxPrice);
			
			Mage::helper('loyalty')->newTransaction(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				1,  		// new earning
				11, 		// making a purchase
				$points,	// point quanity
				1,			// pending
				"Points for order id ".$order->getIncrementId()
			);
			
			// we also need to award points to the customer who refered current customer if they do exist.
			Mage::helper('loyalty')->awardPointsForCase("referal_purchase", $order->getIncrementId());
			// and we also need to add a spending transaction if we spent some points.
			if($order->getLoyaltyAmount() * -1  > 0)
			{
				// convert currency to points
				$spent_pts = Mage::helper('loyalty')->getPointsForCurrency( $order->getLoyaltyAmount()* -1 );
				Mage::helper('loyalty')->newTransaction($referal_id, 2 , 50 , $spent_pts, 1, "Points for order id ".$order->getIncrementId(), $order->getIncrementId() );
			}
			
		}
		else
		{
			echo "do nothing!";
		}
		*/
	}
}