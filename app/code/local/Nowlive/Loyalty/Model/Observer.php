<?php
class Nowlive_Loyalty_Model_Observer {
	
	public function invoiceSaveAfter(Varien_Event_Observer $observer)
	{
		$invoice = $observer->getEvent()->getInvoice();
		if ($invoice->getBaseLoyaltyAmount()) {
			$order = $invoice->getOrder();
			$order->setLoyaltyAmountInvoiced($order->getLoyaltyAmountInvoiced() + $invoice->getLoyaltyAmount());
			$order->setBaseLoyaltyAmountInvoiced($order->getBaseLoyaltyAmountInvoiced() + $invoice->getBaseLoyaltyAmount());
		}
		return $this;
	}
	public function creditmemoSaveAfter(Varien_Event_Observer $observer)
	{
		/* @var $creditmemo Mage_Sales_Model_Order_Creditmemo */
		$creditmemo = $observer->getEvent()->getCreditmemo();
		if ($creditmemo->getLoyaltyAmount()) {
			$order = $creditmemo->getOrder();
			$order->setLoyaltyAmountRefunded($order->getLoyaltyAmountRefunded() + $creditmemo->getLoyaltyAmount());
			$order->setBaseLoyaltyAmountRefunded($order->getBaseLoyaltyAmountRefunded() + $creditmemo->getBaseLoyaltyAmount());
		}
		return $this;
	}
	public function updatePaypalTotal($evt){
		$cart = $evt->getPaypalCart();
		$cart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_SUBTOTAL,$cart->getSalesEntity()->getLoyaltyAmount());
	}
	
	
	public function orderPlaceAfter($event)
	{
		// first check if the customer is logged in
		if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
			// so we need to create a new points transaction to the order creating customer
			$maxPrice = 0;
			$order = Mage::getModel("sales/order")->load( $event->getEvent()->getOrder()->getId() );
			foreach($order->getAllItems() as $item) {
				// Do what you want
				$maxPrice += $item->getPrice() * $item->getQtyOrdered();
			}
			
			$points = Mage::helper('loyalty')->getPointsToCurrency($maxPrice);
			
			Mage::helper('loyalty')->newTransaction(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				1,  		// new earning
				11, 		// making a purchase
				$points,	// point quanity
				Mage::getStoreConfig('loyalty/loyalty/new_order_earn_status'), // approved
				"Points for order id ".$order->getIncrementId(), // extra
				$order->getIncrementId() // extras id
			);
			
			// we also need to award points to the customer who refered current customer if they do exist.
			Mage::helper('loyalty')->awardPointsForCase("referal_purchase", $order->getIncrementId());
			
			// and we also need to add a spending transaction if we spent some points.
			if($order->getLoyaltyAmount() * -1  > 0)
			{
				// convert currency to points
				$spent_pts = Mage::helper('loyalty')->getPointsForCurrency( $order->getLoyaltyAmount()* -1 );
				Mage::helper('loyalty')->newTransaction( Mage::getSingleton('customer/session')->getCustomer()->getId(), 2 , 50 , $spent_pts, Mage::getStoreConfig('loyalty/loyalty/new_order_spend_status'), "Points for order id ".$order->getIncrementId(), $order->getIncrementId() );
			}
		}
	}
	
	/*
	public function orderPlaceAfter2($event)
	{
		// first check if the customer is logged in
		if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
			// so we need to create a new points transaction to the order creating customer
			$maxPrice = 0;
			$order = Mage::getModel("sales/order")->load(2);
			foreach($order->getAllItems() as $item) {
				// Do what you want
				$maxPrice += $item->getPrice() * $item->getQtyOrdered();
			}
			
			$points = Mage::helper('loyalty')->getPointsToCurrency($maxPrice);
			
			Mage::helper('loyalty')->newTransaction(
				Mage::getSingleton('customer/session')->getCustomer()->getId(),
				1,  		// new earning
				11, 		// making a purchase
				$points,	// point quanity
				1,			// pending
				"Points for order id ".$order->getIncrementId()
			);
			
			// we also need to award points to the customer who refered current customer if they do exist.
			Mage::helper('loyalty')->awardPointsForCase("referal_purchase", $order->getIncrementId());

			// and we also need to add a spending transaction if we spent some points.
			if($order->getLoyaltyAmount() > 0)
			{
				// convert currency to points
				$spent_pts = Mage::helper('loyalty')->getPointsForCurrency( $order->getLoyaltyAmount() );
				$this->newTransaction($referal_id, 2 , 50 , $spent_pts, 1, "Points for order id ".$order->getIncrementId(), $order->getIncrementId() );
				// Mage::helper('loyalty')->awardPointsForCase("referal_purchase", $order->getIncrementId());
			}

		}
	}
	*/
	
	public function reviewSaveAfter($event)
	{
		// first check if the customer is logged in
		if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
			Mage::helper('loyalty')->awardPointsForCase("product_review", $event->getObject()->getReviewId());
		}
	}
	
	public function customerRegisterSuccess($event)
	{
		$customer = $event->getCustomer();

		// give the customer their own points for signing up
		$config_points = Mage::getStoreConfig('loyalty/loyalty/new_account');
		$config_status = Mage::getStoreConfig('loyalty/loyalty/new_account_status');
		
		Mage::helper('loyalty')->newTransaction($customer->getId(), 1 , 17, $config_points, $config_status, "Points for signing up");

		// award the referer the points
		Mage::helper('loyalty')->awardPointsForCase("friend_referal", $customer->getId());
	}
	
	public function newsletterSubscriberSaveAfter($observer)
	{
	
		$config_points = Mage::getStoreConfig('loyalty/loyalty/newsletter_signup');
		$config_status = Mage::getStoreConfig('loyalty/loyalty/newsletter_signup_status');
		$data = $observer->getEvent()->getSubscriber();

		Mage::Log("getSubscriberStatus [".$data->getSubscriberStatus()."] customer_id[".$data->getCustomerId()."]", null, "testing.log");
		if(!$this->hasNewsletterPoints($data->getCustomerId()))
		{
			Mage::helper('loyalty')->newTransaction( $data->getCustomerId() , 1 , 19, $config_points, $config_status, "Points for signing up to the newsletter", 0);
		}
	}
	
	public function hasNewsletterPoints($customer_id)
	{
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$table = $resource->getTableName('loyalty/transactions');
		
		$transaction_id = $readConnection->fetchOne( "SELECT t.transaction_id FROM `mage_loyalty_transactions` as t WHERE t.customer_id = '".$customer_id."';" );
		if($transaction_id > 0)
		{
			return true;
		}
		return false;

	}
}