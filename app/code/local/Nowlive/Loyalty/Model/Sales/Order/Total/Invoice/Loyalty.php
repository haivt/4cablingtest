<?php
class Nowlive_Loyalty_Model_Sales_Order_Total_Invoice_Loyalty extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Invoice $invoice)
	{
		$order = $invoice->getOrder();
		$feeAmountLeft = $order->getLoyaltyAmount() - $order->getLoyaltyAmountInvoiced();
		$baseLoyaltyAmountLeft = $order->getBaseLoyaltyAmount() - $order->getBaseLoyaltyAmountInvoiced();
		if (abs($baseLoyaltyAmountLeft) < $invoice->getBaseGrandTotal()) {
			$invoice->setGrandTotal($invoice->getGrandTotal() + $feeAmountLeft);
			$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseLoyaltyAmountLeft);
		} else {
			$feeAmountLeft = $invoice->getGrandTotal() * -1;
			$baseLoyaltyAmountLeft = $invoice->getBaseGrandTotal() * -1;

			$invoice->setGrandTotal(0);
			$invoice->setBaseGrandTotal(0);
		}
			
		$invoice->setLoyaltyAmount($feeAmountLeft);
		$invoice->setBaseLoyaltyAmount($baseLoyaltyAmountLeft);
		return $this;
	}
}
