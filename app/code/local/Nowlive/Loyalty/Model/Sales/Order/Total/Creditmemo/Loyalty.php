<?php
class Nowlive_Loyalty_Model_Sales_Order_Total_Creditmemo_Loyalty extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
	{
		$order = $creditmemo->getOrder();
		$feeAmountLeft = $order->getLoyaltyAmountInvoiced() - $order->getLoyaltyAmountRefunded();
		$basefeeAmountLeft = $order->getBaseLoyaltyAmountInvoiced() - $order->getBaseLoyaltyAmountRefunded();
		if ($basefeeAmountLeft > 0) {
			$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $feeAmountLeft);
			$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $basefeeAmountLeft);
			$creditmemo->setLoyaltyAmount($feeAmountLeft);
			$creditmemo->setBaseLoyaltyAmount($basefeeAmountLeft);
		}
		return $this;
	}
}
