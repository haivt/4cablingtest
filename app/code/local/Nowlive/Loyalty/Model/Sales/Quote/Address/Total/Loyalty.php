<?php
class Nowlive_Loyalty_Model_Sales_Quote_Address_Total_Loyalty extends Mage_Sales_Model_Quote_Address_Total_Abstract{
	protected $_code = 'loyalty';

	public function collect(Mage_Sales_Model_Quote_Address $address)
	{
		parent::collect($address);

		$this->_setAmount(0);
		$this->_setBaseAmount(0);

		$items = $this->_getAddressItems($address);
		if (!count($items)) {
			return $this; //this makes only address type shipping to come through
		}


		$quote = $address->getQuote();

		if(Nowlive_Loyalty_Model_Loyalty::canApply($address)){
			$exist_amount = $quote->getLoyaltyAmount();
			$fee = Nowlive_Loyalty_Model_Loyalty::getLoyalty();
			$balance = $fee - $exist_amount;
			// 			$balance = $fee;

			//$this->_setAmount($balance);
			//$this->_setBaseAmount($balance);

			$address->setLoyaltyAmount($balance);
			$address->setBaseLoyaltyAmount($balance);
				
			$quote->setLoyaltyAmount($balance);

			$address->setGrandTotal($address->getGrandTotal() + $address->getLoyaltyAmount());
			$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseLoyaltyAmount());
		}
	}

	public function fetch(Mage_Sales_Model_Quote_Address $address)
	{
		$amt = $address->getLoyaltyAmount();
		$address->addTotal(array(
				'code'=>$this->getCode(),
				'title'=>Mage::helper('loyalty')->__('Loyalty Points Discount'),
				'value'=> $amt
		));
		return $this;
	}
}