<?php
class Nowlive_Loyalty_Model_Loyalty extends Mage_Core_Model_Abstract
{
	public static $DISCOUNT_AMOUNT = 0;
	
	public static function getLoyalty(){
		// load the loyalty helper 
		$session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
		$helper = Mage::helper('loyalty');
		return $helper->getCurrencyForPoints( $session->getData("spend_points") ) * -1;
	}
	 
	public static function canApply($address){
		//put here your business logic to check if fee should be applied or not
		//if($address->getAddressType() == 'billing'){
		return true;
		//}
	}
	
	protected function _construct()
    {
        $this->_init('loyalty/transactions', 'transaction_id');
    }
	        
}