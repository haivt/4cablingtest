<?php

class Nowlive_Loyalty_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;
    const STATUS_CANCELED	= 3;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('loyalty')->__('Pending'),
            self::STATUS_DISABLED   => Mage::helper('loyalty')->__('Approved'),
            self::STATUS_CANCELED   => Mage::helper('loyalty')->__('Canceled'),
        );
    }
	
    static public function toOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('loyalty')->__('Pending'),
            self::STATUS_DISABLED   => Mage::helper('loyalty')->__('Approved'),
            self::STATUS_CANCELED   => Mage::helper('loyalty')->__('Canceled'),
        );
    }
}