<?php

class Nowlive_Loyalty_Model_Mysql4_Loyalty extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the loyalty_id refers to the key field in your database table.
        $this->_init('loyalty/loyalty', 'transaction_id');
    }
}