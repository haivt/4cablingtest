<?php

class Nowlive_Loyalty_Model_Mysql4_Giftcard extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the giftcard_id refers to the key field in your database table.
        $this->_init('loyalty/giftcard', 'giftcard_id');
    }
}