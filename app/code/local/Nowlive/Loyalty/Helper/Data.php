<?php

class Nowlive_Loyalty_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getFullTableName($t)
	{
		return Mage::getSingleton('core/resource')->getTableName($t);
	}
	
	public function getTransactionInfoTypesArray()
	{
		$types = array(
			1 => "Earn: Admin allocation.",
			11 => "Earn: Purchase.",
			12 => "Earn: Rating a product.",
			13 => "Earn: Refering a friend.",
			14 => "Earn: Liking or sharing a product or page.",
			15 => "Earn: Birthday.",
			16 => "Earn: Writing a review.",
			17 => "Earn: Creating an account.",
			18 => "Earn: Referer purchase.",
			19 => "Earn: Newsletter signup.",
			20 => "Earn: Sending product to friends.",
			50 => "Spend: Points spent on purchase.",
			51 => "Spend: Points spent on gift card.",
			52 => "Spend: Removed points for unsubscribing from the newsletter.",
		);
		
		return $types;
	}
	
	public function getTransactionTypesArray()
	{
		$types = array(
			1 => "Earn",
			2 => "Spend",
		);
		
		return $types;
	}
	
	public function getPointToCurrencyRatio()
	{
		return 0.1; // 1 point = 0.1 currency
	}

	public function getCurrencyToPointRatio()
	{
		return 10; // 1 currency = 10 points
	}

	
	public function getMyPoints()
	{
		// check if customer is logged in
		if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
			// get customer id
			$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
			
			$resource = Mage::getSingleton('core/resource');
			$readConnection = $resource->getConnection('core_read');
			$table = $resource->getTableName('loyalty/transactions');
			// sum up the "earned points" which have been approved!
			$earned = $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 1 and status = 2 and customer_id = ".$customer_id.";" );
			// sum up the "spent points" which have been approved!
			$spent = $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 2 and status = 2 and customer_id = ".$customer_id.";" );
			
			$spent_on_gift_cards_in_pending = 0 + $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 2 and status = 1 and info = 51 and customer_id = ".$customer_id.";" );
			
			$available = $earned - $spent - $spent_on_gift_cards_in_pending;
	
			return $available;
		}
		return false;
	}
	
	public function getMyPointsSetup()
	{
		
		if( Mage::getSingleton('customer/session')->isLoggedIn() )
		{
			// get customer id
			$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
			
			$resource = Mage::getSingleton('core/resource');
			$readConnection = $resource->getConnection('core_read');
			$table = $resource->getTableName('loyalty/transactions');
			
			$earned = 0 + $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 1 and status = 1 and customer_id = ".$customer_id.";" );
			$spent = 0 + $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 2 and status = 1 and customer_id = ".$customer_id.";" );
			
			$available = $earned - $spent;
			
			$retval = array(
				"earned_pending" => $earned,
				"spent_pending" => $spent,
				"available_pending" => $available,
			);

			// sum up the "earned points" which have been approved!
			$aearned = 0 + $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 1 and status = 2 and customer_id = ".$customer_id.";" );
			// sum up the "spent points" which have been approved!
			$aspent = 0 + $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 2 and status = 2 and customer_id = ".$customer_id.";" );
			
			// spent on gift cards
			$spent_on_gift_cards_in_pending = 0 + $readConnection->fetchOne( "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 2 and status = 1 and info = 51 and customer_id = ".$customer_id.";" );
			
			// echo "SELECT SUM(t.quantity) as cnt FROM `".$this->getFullTableName("loyalty_transactions")."` as t WHERE type = 2 and status = 2 and info = 51 and customer_id = ".$customer_id.";";
			$aavailable = $aearned - $aspent - $spent_on_gift_cards_in_pending;
			// echo "spent_on_gift_cards_in_pending[". $spent_on_gift_cards_in_pending."]";
	
			$retval["earned"] = $aearned;
			$retval["spent"] = $aspent;
			$retval["available"] = $aavailable;
	
			return $retval;
		}
	}
	
	public function getMyReferalsCollection()
	{
		$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
		
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$query = "SELECT * FROM ". $resource->getTableName('loyalty_referal')." WHERE customer_id = '".$customer_id."' ORDER BY `ref_id` DESC;";
		$results = $readConnection->fetchAll($query);
		return $results;
	}
	
	public function getMyFullTransactionHistoryCollection()
	{
		$resource = Mage::getSingleton('core/resource');
		$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
		
		$collection = Mage::getModel('loyalty/transactions')->getCollection();
		$collection->getSelect()
			->join( $resource->getTableName('customer_entity'), 'main_table.customer_id = '.$resource->getTableName('customer_entity').'.entity_id', array('customer_name' => 'email'))
			->where('customer_id = '.$customer_id)
			->order("transaction_id DESC");
		
		return $collection;
		
		/*
		
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$query = "SELECT * FROM ". $resource->getTableName('loyalty_referal')." WHERE customer_id = '".$customer_id."' ORDER BY `ref_id` DESC;";
		$results = $readConnection->fetchAll($query);
		return $results;
		*/
	}
	
	public function getMyTransactionHistoryCollectionWithFilter($filter_id)
	{
		$resource = Mage::getSingleton('core/resource');
		$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
		
		$collection = Mage::getModel('loyalty/transactions')->getCollection();
		$collection->getSelect()
			->join( $resource->getTableName('customer_entity'), 'main_table.customer_id = '.$resource->getTableName('customer_entity').'.entity_id', array('customer_name' => 'email'))
			->where('customer_id = '.$customer_id)
			->where('main_table.info = \''.$filter_id."'")
			->order("transaction_id DESC");
			
		// echo $collection->getSelect()->__toString();
		return $collection;
	}
	
	public function getPoints($price)
	{
		// process rules
		$points = $price;
		return floor($price);
	}
	
	public function getPointsInCart()
	{
		$maxPrice = 0;
		$cartItems = Mage::getModel("checkout/cart")->getItems();
		foreach($cartItems as $item) {
			// Do what you want
			$maxPrice += $item->getPrice()*$item->getQty();
		}
		
		$points = $this->getPointsForCurrency($maxPrice);
		return $points;
	}
	
	public function getPointsOnOrder()
	{
		
		
		return $this->getPointsInCart() * $this->getPointToCurrencyRatio();
	}
	
	public function getCurrencyForPoints($points)
	{
		return $this->getPointToCurrencyRatio() * $points;
	}
	
	public function getPointsForCurrency($currency)
	{
		return $this->getCurrencyToPointRatio() * $currency  ;
	}
	
	public function getPointsToCurrency($currency)
	{
		return $currency  ;
	}
	
	public function getCartUpdateUrl()
	{
		return Mage::getBaseUrl().'loyalty/index/update/';
	}
	
	public function getAccountRefUrl()
	{
		return Mage::getBaseUrl().'loyalty/index/refer/';
	}
	
	public function checkEmailAddress($email)
	{
		$regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
		if (preg_match($regex, $email)) {
			return true;
		} else { 
			return false;
		}
	}
	
	public function _addProductToCart($product_sku, $newPrice, $force_insertion = false)
	{
		$product_add = Mage::getModel('catalog/product');
		$product_id = $product_add->getIdBySku($product_sku);
		if (!$this->_checkProductInCart($product_sku))
		{
			if ($product_id)
			{
				try {
					$product_add->load($product_id);
					
					$cart = Mage::getSingleton('checkout/cart');
					$cart->addProduct($product_add, array('qty' => 1));
				} catch (Exception $e) {
					Mage::Log("_addProductToCart error => ".$e->getMessage());
				}
			}
		}
	}
	
	public function _checkProductInCart($sku)
	{
		$cartHelper = Mage::helper('checkout/cart');
		$items = $cartHelper->getCart()->getItems();
	
		foreach ($items as $_item)
		{
			$productId = $_item->getProductId();
			$products = Mage::getModel('catalog/product')->load($productId);

			$this->cartException = '1000000013702|2';
			
			$cart_ex_array = explode('|',$this->cartException);
			$dontTopUp = true;
	
			if ($_item->getQty() == $cart_ex_array[1] && $cart_ex_array[0] == $products->getSku())
			{
				$dontTopUp = false;
			} else {
				//cannot process...
			}
	
			//Mage::Log("_checkProductInCart->[".$sku."] now [".$products->getSku()."]");
			if($products->getSku() == $sku && $dontTopUp){
				return true;
			}
		}
		return false;
	}
	
	public function _removeProductInCart($product_sku)
	{
		if ($product_sku)
		{
			$product_add = Mage::getModel('catalog/product');
			$productId = $product_add->getIdBySku($product_sku);
	
			$cartHelper = Mage::helper('checkout/cart');
			$items = $cartHelper->getCart()->getItems();
	
			foreach ($items as $item) {
				if ($item->getProduct()->getId() == $productId) 
				{
					$itemId = $item->getItemId();
					$cartHelper->getCart()->removeItem($itemId);
				}
			}
		}
			
	}
	
	public function addLoyaltyToTotals($totals, $source)
	{
		$newTotals = array();
		
		foreach ($totals as $code => $total) {
			if ($code == 'grand_total' 
				&& (float) $source->getCustomSurchargeAmount() != 0) {
				$newTotals['custom_surcharge_amount'] = new Varien_Object(
					array(
						'code' => 'loyalty',
						'field' => 'loyalty',
						'value' => $source->getLoyaltyAmount(),
						'label' => $this->__('Loyalty Point Discount'),
					)
				);
			}
			$newTotals[$code] = $total;
		}
		
		return $newTotals;
	}
	
	public function formatLoyalty($rv)
	{
		return $rv;
	}
	
	public function hasBeenReferred($email)
	{
		$retval = false;
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$table = $resource->getTableName('loyalty/referal');
		
		$referal_id = $readConnection->fetchOne( "SELECT r.customer_id FROM `".$this->getFullTableName("loyalty_referal")."` as r WHERE r.refering_email = '".$email."';" );
		if($referal_id > 0)
		{	
			$retval = true;
		}
		return $retval;
	}
	
	public function newTransaction($customer_id, $type, $info, $points, $status = 1, $extra = null, $extra_id = null)
	{
		Mage::Log("newTransaction customer_id[".$customer_id."] type[".$type."] info[".$info."] points[".$points."] status[".$status."] extra[".$extra."] extra_id[".$extra_id."]", null, "testing.log");
		
		$data = array(
			"customer_id" => $customer_id,
			"type" => $type,
			"info" => $info,
			"quantity" => $points,
			"status" => $status,
			"extra" => $extra,
			"extras" => $extra_id,
		);
		
		$model = Mage::getModel('loyalty/transactions');		
		$model->setData($data);
		
		if ($model->getCreatedTime() == NULL || $model->getUpdateTime() == NULL) {
			$model->setCreatedTime(now())->setUpdateTime(now());
		}
		else {
			$model->setUpdateTime(now());
		}	
		
		$model->setDate(now());
		$model->save();
		return $model->getTransactionId(); 
	}
	
	public function awardPointsForCase($type, $extra)
	{
		Mage::Log("awardPointsForCase[".$type."] extra[".$extra."]", null, "testing.log");

		$customer = Mage::getSingleton('customer/session')->getCustomer();
		$config_points = Mage::getStoreConfig('loyalty/loyalty/'.$type);
		$config_status = Mage::getStoreConfig('loyalty/loyalty/'.$type.'_status');

		if($type == "referal_purchase") 
		{
			$config_points = Mage::getStoreConfig('loyalty/loyalty/referal_points');
			$config_status = Mage::getStoreConfig('loyalty/loyalty/referal_points_status');
			
			// get referal if they exist
			// $model = Mage::getModel('loyalty/referal')->getCollection();	
			// $model->getSelect()->where("refering_email = '".$customer->getEmail()."'");
			
			$resource = Mage::getSingleton('core/resource');
			$readConnection = $resource->getConnection('core_read');
			$table = $resource->getTableName('loyalty/referal');
			
			$referal_id = $readConnection->fetchOne( "SELECT r.customer_id FROM `".$this->getFullTableName("loyalty_referal")."` as r WHERE r.refering_email = '".$customer->getEmail()."';" );
			if($referal_id > 0)
			{
				// create the new transaction
				$this->newTransaction($referal_id, 1 /* earn*/ , 18 /* referal_purchase*/ , $config_points, $config_status, "Points for refering customer that placed order id ".$extra, $extra );
			}
		}
		else if($type == "product_review") // or ($type == "product_rating")
		{
			$this->newTransaction($customer->getId(), 1 , 16, $config_points, $config_status, "Points for writing a review id -> ".$extra, $extra );
		}
		else if($type == "new_account")
		{
			// [done in observer]
		}
		else if($type == "newsletter_signup")
		{
			// [done in observer]
		}
		else if($type == "send_product_to_friend")
		{
			
		}
		else if($type == "friend_referal")
		{
			// extra
			$customer = Mage::getModel("customer/customer")->load($extra);
			
			// get referal if they exist
			$resource = Mage::getSingleton('core/resource');
			$readConnection = $resource->getConnection('core_read');
			$table = $resource->getTableName('loyalty/referal');
			
			$referal_id = $readConnection->fetchOne( "SELECT r.customer_id FROM `".$this->getFullTableName("loyalty_referal")."` as r WHERE r.refering_email = '".$customer->getEmail()."';" );
			if($referal_id > 0)
			{
				// create the new transaction
				$this->newTransaction($referal_id, 1 , 13 , $config_points, $config_status, "Points for refering ".$customer->getEmail(), $extra );
			}
			else
			{
				Mage::Log($customer->getEmail()." was not refered by anyone.", null, "testing.log");
			}
		}
		else if($type == "facebook_sharing")
		{
			// get points quantity for current case
			
		}
		else if($type == "facebook_like")
		{
			// get points quantity for current case
			$this->newTransaction($customer->getId(), 1 , 14, $config_points, $config_status, "Points for liking or sharing on facebook [".$extra."]", $extra );			
		}
		else if($type == "twitter_share")
		{
			// get points quantity for current case
			$this->newTransaction($customer->getId(), 1 , 14, $config_points, $config_status, "Points for sharing on twitter [".$extra."]", $extra );			
		}
		else if($type == "google_plus_sharing")
		{
			// get points quantity for current case
			$this->newTransaction($customer->getId(), 1 , 14, $config_points, $config_status, "Points for sharing on google plus [".$extra."]", $extra );			
		}
		else if($type == "birthday")
		{
			// get points quantity for current case
			
		}
		
		return $type;
	}
	
	public function getWestfieldOptions()
	{
		return array(
			array("pts" => 50, "price" => 5, "label" => "$5 Westfield Gift Card (50 Points)"),
			array("pts" => 100, "price" => 10, "label" => "$10 Westfield Gift Card (100 Points)"),
			array("pts" => 200, "price" => 20, "label" => "$20 Westfield Gift Card (200 Points)"),
			array("pts" => 500, "price" => 50, "label" => "$50 Westfield Gift Card (500 Points)"),
			array("pts" => 1000, "price" => 100, "label" => "$100 Westfield Gift Card (1000 Points)"),
		);
	}
	
	public function getWestfieldLabelByPts($pts)
	{
		foreach( $this->getWestfieldOptions() as $option)
		{
			if($pts == $option["pts"])
			{
				return $option["label"];
			}
		}
	}
	
	public function sendEmail($template, $send_to, $postObject)
	{
				
		$emailTemplate = Mage::getModel("core/email_template"); // ->loadDefault('loyalty_email_template_'.$template);
		/* @var $mailTemplate Mage_Core_Model_Email_Template */
		$emailTemplate->setDesignConfig( array('area' => 'frontend') )
			->sendTransactional(
				Mage::getStoreConfig('loyalty/templates/email_template_'.$template),
				Mage::getStoreConfig('contacts/email/sender_email_identity'),
				$send_to,
				null,
				array('data' => $postObject)
			);

		if (!$emailTemplate->getSentSuccess()) {
			throw new Exception();
		}
	}
	
	
	
}