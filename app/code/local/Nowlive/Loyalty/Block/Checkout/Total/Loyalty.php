<?php
class Nowlive_Loyalty_Block_Checkout_Total_Loyalty extends Mage_Checkout_Block_Total_Default
{
    protected $_template = 'loyalty/checkout/total/loyalty.phtml';
	
    private $_key = 'loyalty_checkout_total_loyalty';
    private $_cache_tag = true;

    public function __construct()
    {
        $this->setHtmlId($this->_key);
       	parent::__construct();
    }

    public function _afterToHtml($html)
    {
		$modules = (array)Mage::getConfig()->getNode('modules')->children();
		if (array_key_exists('Ezapps_Zoom', $modules) && $modules['Ezapps_Zoom']->is('active') ) {
	
			if ($this->getCacheTag() && ((trim($html) != "" && Mage::helper('ezzoom')->punchStatus($this->_key) == 1) || Mage::helper('ezzoom')->punchStatus($this->_key) == 2)) 
			{
				$name = (Mage::helper('ezzoom')->getConfigData('zoom_lite') ? $this->getTemplate() : $this->getNameInLayout());
				$retval = Mage::helper('ezzoom')->renderHoleStart($this->_key, $name) . parent::_afterToHtml($html) . Mage::helper('ezzoom')->renderHoleEnd($this->_key, $name);
				return $retval;
			} 
			else
			{
				return parent::_afterToHtml($html);
			}
	
		} else return parent::_afterToHtml($html);

    }

    public function setCacheTag($status) {
		$this->_cache_tag = $status;	
		return $this;
    }

    public function getCacheTag() {
		return $this->_cache_tag;	
    }
}