<?php
class Nowlive_Loyalty_Block_Loyalty extends Mage_Core_Block_Template
{
    private $_key = 'loyalty';
    private $_cache_tag = true;

    public function __construct()
    {
        $this->setHtmlId($this->_key);
       	parent::__construct();
    }

    public function _afterToHtml($html)
    {
		$modules = (array)Mage::getConfig()->getNode('modules')->children();
		if (array_key_exists('Ezapps_Zoom', $modules) && $modules['Ezapps_Zoom']->is('active') ) {
	
			if ($this->getCacheTag() && ((trim($html) != "" && Mage::helper('ezzoom')->punchStatus($this->_key) == 1) || Mage::helper('ezzoom')->punchStatus($this->_key) == 2)) 
			{
				$name = (Mage::helper('ezzoom')->getConfigData('zoom_lite') ? $this->getTemplate() : $this->getNameInLayout());
				$retval = Mage::helper('ezzoom')->renderHoleStart($this->_key, $name) . parent::_afterToHtml($html) . Mage::helper('ezzoom')->renderHoleEnd($this->_key, $name);
				return $retval;
			} 
			else
			{
				return parent::_afterToHtml($html);
			}
	
		} else return parent::_afterToHtml($html);

    }

    public function setCacheTag($status) {
		$this->_cache_tag = $status;	
		return $this;
    }

    public function getCacheTag() {
		return $this->_cache_tag;	
    }
	
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getLoyalty()     
     { 
        if (!$this->hasData('loyalty')) {
            $this->setData('loyalty', Mage::registry('loyalty'));
        }
        return $this->getData('loyalty');
        
    }
	
	public function getCurrentPointAmmount()
	{
		return Mage::helper('loyalty')->getMyPoints();
	}
	
	public function showTopPoints()
	{
		$pts = $this->getCurrentPointAmmount();
		
		
		if($pts == 0)
		{
			$msg = $this->__("You have no points.");
		}
		else if($pts == 1)
		{
			$msg = $this->__("You have 1 point.");
		}
		else
		{
			$msg = str_replace("%", $pts, $this->__("You have % points."));
		}
		
		
		return '<a style="color:#000" href="'.Mage::getBaseUrl().'loyalty">'.$msg .'</a>';
	}
}