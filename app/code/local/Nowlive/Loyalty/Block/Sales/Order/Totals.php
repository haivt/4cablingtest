<?php
class Nowlive_Loyalty_Block_Sales_Order_Totals extends Mage_Sales_Block_Order_Totals // extends Mage_Core_Block_Template
{
    private $_key = 'loyalty_sales_order_totals';
    private $_cache_tag = true;

    public function __construct()
    {
        $this->setHtmlId($this->_key);
       	parent::__construct();
    }

    public function _afterToHtml($html)
    {
		$modules = (array)Mage::getConfig()->getNode('modules')->children();
		if (array_key_exists('Ezapps_Zoom', $modules) && $modules['Ezapps_Zoom']->is('active') ) {
	
			if ($this->getCacheTag() && ((trim($html) != "" && Mage::helper('ezzoom')->punchStatus($this->_key) == 1) || Mage::helper('ezzoom')->punchStatus($this->_key) == 2)) 
			{
				$name = (Mage::helper('ezzoom')->getConfigData('zoom_lite') ? $this->getTemplate() : $this->getNameInLayout());
				$retval = Mage::helper('ezzoom')->renderHoleStart($this->_key, $name) . parent::_afterToHtml($html) . Mage::helper('ezzoom')->renderHoleEnd($this->_key, $name);
				return $retval;
			} 
			else
			{
				return parent::_afterToHtml($html);
			}
	
		} else return parent::_afterToHtml($html);

    }

    public function setCacheTag($status) {
		$this->_cache_tag = $status;	
		return $this;
    }

    public function getCacheTag() {
		return $this->_cache_tag;	
    }
	
    /**
     * Get label cell tag properties
     *
     * @return string
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * Get order store object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * Get totals source object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }

    /**
     * Get value cell tag properties
     *
     * @return string
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

	protected function _initTotals()
	{
		parent::_initTotals();
		
		$newTotals = Mage::helper('loyalty')->addLoyaltyToTotals(	$this->_totals, $this->getSource() 	);
		
		$this->_totals = $newTotals;

		return $this;
	}

	
    /**
     * Initialize reward points totals
     *
     * @return Enterprise_Reward_Block_Sales_Order_Total
     */
	 /*
    public function initTotals()
    {

		Mage::Log("initTotals", null, "tests.log");
        if ((float) $this->getOrder()->getBaseLoyaltyAmount()) {
            $source = $this->getSource();
            $value  = $source->getLoyaltyAmount();

            $this->getParentBlock()->addTotal(new Varien_Object(array(
                'code'   => 'loyalty',
                'strong' => false,
                'label'  => Mage::helper('loyalty')->formatLoyalty($value),
                'value'  => $source instanceof Mage_Sales_Model_Order_Creditmemo ? - $value : $value
            )));
        }

        return $this;
    }
	*/
}
