<?php
class Nowlive_Loyalty_Block_Adminhtml_Loyalty extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_loyalty';
    $this->_blockGroup = 'loyalty';
    $this->_headerText = Mage::helper('loyalty')->__('Transaction History');
    $this->_addButtonLabel = Mage::helper('loyalty')->__('Add Points');
    parent::__construct();
  }
}