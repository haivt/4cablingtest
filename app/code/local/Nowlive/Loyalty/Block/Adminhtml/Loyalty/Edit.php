<?php

class Nowlive_Loyalty_Block_Adminhtml_Loyalty_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'loyalty';
        $this->_controller = 'adminhtml_loyalty';
        
        $this->_updateButton('save', 'label', Mage::helper('loyalty')->__('Save Transaction'));
        $this->_updateButton('delete', 'label', Mage::helper('loyalty')->__('Delete Transaction'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('loyalty_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'loyalty_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'loyalty_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('loyalty_data') && Mage::registry('loyalty_data')->getId() ) {
            return Mage::helper('loyalty')->__("Edit Transaction '%s'", $this->htmlEscape(Mage::registry('loyalty_data')->getId()));
        } else {
            return Mage::helper('loyalty')->__('Add Transaction');
        }
    }
}