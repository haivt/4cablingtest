<?php

class Nowlive_Loyalty_Block_Adminhtml_Loyalty_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('loyalty_form', array('legend'=>Mage::helper('loyalty')->__('Transaction Data')));
		 
	 
	 	$collection = Mage::getModel('customer/customer')->getCollection(); 
	 	$customers = array();
    	foreach($collection as $item)
		{ 
			if($item->getParent == NULL){
				$customers[] = array(
				'value'     => $item->getId(),
				'label'     => $item->getEmail(),
				);
			}
		}
		
		$fieldset->addField('customer_id', 'select', array(
			'label'     => Mage::helper('loyalty')->__('Customer'),
			'class'     => 'required-entry',
			'required'  => true,
			'name'      => 'customer_id',
			'values'    => $customers,
		));
		
		
		$fieldset->addField('type', 'select', array(
			'label'     => Mage::helper('loyalty')->__('Type'),
			'name'      => 'type',
			'values'    => Mage::helper('loyalty')->getTransactionTypesArray(),
		));
		
		$fieldset->addField('info', 'select', array(
			'label'     => Mage::helper('loyalty')->__('Type'),
			'name'      => 'info',
			'values'    => Mage::helper('loyalty')->getTransactionInfoTypesArray(),
		));
		
		$fieldset->addField('quantity', 'text', array(
			'label'     => Mage::helper('loyalty')->__('Points'),
			'name'      => 'quantity',
			'class'     => 'required-entry',
		));

		$fieldset->addField('date', 'text', array(
			'label'     => Mage::helper('loyalty')->__('Date and Time'),
			'name'      => 'date',
			'class'     => 'required-entry',
			'disabled'  => true,
		));

		$fieldset->addField('extra', 'textarea', array(
			'label'     => Mage::helper('loyalty')->__('Extra Info'),
			'name'      => 'extra',
			'class'     => 'required-entry',
		));
		
		$fieldset->addField('status', 'select', array(
			'label'     => Mage::helper('loyalty')->__('Status'),
			'name'      => 'status',
			'values'    => Mage::getSingleton('loyalty/status')->getOptionArray(),
		));
		
		if ( Mage::getSingleton('adminhtml/session')->getLoyaltyData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getLoyaltyData());
			Mage::getSingleton('adminhtml/session')->setLoyaltyData(null);
		} elseif ( Mage::registry('loyalty_data') ) {
			$form->setValues(Mage::registry('loyalty_data')->getData());
		}
		return parent::_prepareForm();
	}
}