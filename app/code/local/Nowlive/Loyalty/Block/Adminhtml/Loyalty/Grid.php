<?php
class Nowlive_Loyalty_Block_Adminhtml_Loyalty_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
		parent::__construct();
		$this->setId('loyaltyGrid');
		$this->setDefaultSort('transaction_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		if (Mage::registry('preparedFilter')) {
			$this->setDefaultFilter( Mage::registry('preparedFilter') );
		}
  }

  protected function _prepareCollection()
  {
	  $resource = Mage::getSingleton('core/resource');
      $collection = Mage::getModel('loyalty/transactions')->getCollection();
      $collection->getSelect()
    		->join( $resource->getTableName('customer_entity'), 'main_table.customer_id = '.$resource->getTableName('customer_entity').'.entity_id', array('email' => 'email'));
	  
	  $this->setCollection($collection);
	  
	  // $collection->printLogQuery(true);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('transaction_id', array(
          'header'    => Mage::helper('loyalty')->__('ID'),
          'align'     => 'right',
          'width'     => '50px',
          'index'     => 'transaction_id',
      ));

      $this->addColumn('customer_id', array(
          'header'    => Mage::helper('loyalty')->__('Customer Email'),
          'align'     => 'left',
          'index'     => 'email',
      ));
	  
      $this->addColumn('type', array(
          'header'    => Mage::helper('loyalty')->__('Type'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'type',
          'type'      => 'options',
          'options'   => Mage::helper('loyalty')->getTransactionTypesArray(),
      ));

      $this->addColumn('quantity', array(
          'header'    => Mage::helper('loyalty')->__('Points'),
          'align'     => 'left',
		  'width'     => '80px',
          'index'     => 'quantity',
      ));
	  
      $this->addColumn('info', array(
          'header'    => Mage::helper('loyalty')->__('Info'),
          'align'     => 'left',
          'index'     => 'info',
          'type'      => 'options',
          'options'   => Mage::helper('loyalty')->getTransactionInfoTypesArray(),
      ));
	  
	  
      $this->addColumn('extra', array(
          'header'    => Mage::helper('loyalty')->__('Extra Info'),
          'align'     => 'left',
          'index'     => 'extra',
      ));
	  
      $this->addColumn('date', array(
          'header'    => Mage::helper('loyalty')->__('Date'),
          'align'     => 'left',
          'index'     => 'date',
      ));


      $this->addColumn('status', array(
          'header'    => Mage::helper('loyalty')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => Mage::getSingleton('loyalty/status')->getOptionArray(),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('loyalty')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('loyalty')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('loyalty')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('loyalty')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('transaction_id');
        $this->getMassactionBlock()->setFormFieldName('loyalty');
		
        $statuses = Mage::getSingleton('loyalty/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('loyalty')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('loyalty')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }
 
}