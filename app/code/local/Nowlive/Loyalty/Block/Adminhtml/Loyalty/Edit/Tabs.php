<?php

class Nowlive_Loyalty_Block_Adminhtml_Loyalty_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('loyalty_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('loyalty')->__('Transaction Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('loyalty')->__('Transaction Data'),
          'title'     => Mage::helper('loyalty')->__('Transaction Data'),
          'content'   => $this->getLayout()->createBlock('loyalty/adminhtml_loyalty_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}