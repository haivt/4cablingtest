<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getPostUrl()
    {
        return $this->_getUrl('mailcall/post');
    }
}