<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('infinitymailcall')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('infinitymailcall')->__('Disabled')
        );
    }
}