<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Showreadytimes
{
    protected $_showHours=array('');
    
    public function toOptionArray()
    {
        return array(
            '1'    => Mage::helper('infinitymailcall')->__('Fixed Time'),
            '2'    => Mage::helper('infinitymailcall')->__('Current Time + x hours')
            
        );
    }
}