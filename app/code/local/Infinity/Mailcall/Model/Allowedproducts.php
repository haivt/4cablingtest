<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Allowedproducts
{
    public function toOptionArray()
    {
        return array(
            array('value'=>0, 'label'=>Mage::helper('infinitymailcall')->__('All products')),
            array('value'=>1, 'label'=>Mage::helper('infinitymailcall')->__('Specific Products')),
        );
    }
}