<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Showlogos
{
    
    public function toOptionArray()
    {
        return array(
            'witblack'    => Mage::helper('infinitymailcall')->__('Black'),
            'witblue'    => Mage::helper('infinitymailcall')->__('Blue'),
            'witwhite'    => Mage::helper('infinitymailcall')->__('White'),
            'witcarblack'    => Mage::helper('infinitymailcall')->__('Car black'),
            'witcarblue'    => Mage::helper('infinitymailcall')->__('Car blue'),
            'witcarwhite'    => Mage::helper('infinitymailcall')->__('Car white')       
            
        );
    }
}