<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Showlocations
{
    protected $_showHours=array('');
    
    public function toOptionArray()
    {
        return array(
            '2065'    => Mage::helper('infinitymailcall')->__('Sydney'),
            '3144'    => Mage::helper('infinitymailcall')->__('Melbourne'),  
            'all'    => Mage::helper('infinitymailcall')->__('Sydney and Melbourne'), 
            
        );
    }
}