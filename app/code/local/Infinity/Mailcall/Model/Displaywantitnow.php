<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Displaywantitnow
{
    public function toOptionArray()
    {
        return array(
            0    => Mage::helper('infinitymailcall')->__('Not offer Want It Now'),
            1  => Mage::helper('infinitymailcall')->__('Use standard Want It Now rates')
        );
        
    }
}