<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Showhours
{
    protected $_showHours=array('');
    
    public function toOptionArray()
    {
        $methods = Mage::helper('payment')->getStoreMethods(Mage::app()->getStore()->getId());
        $options = array();
        foreach ($methods as $method)
        {
            array_unshift($options, array(
                'value' => $method->getCode(),
                'label' => $method->getTitle(),
            ));
        }
        array_unshift($options, array(
                'value' => '',
                'label' => Mage::helper('infinitymailcall')->__('Allow All Payment Methods'),
            ));
        return $options;
    }
}