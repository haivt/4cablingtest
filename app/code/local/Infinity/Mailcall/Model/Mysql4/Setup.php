<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Mysql4_Setup extends Mage_Eav_Model_Entity_Setup
{
    public function _construct()
    {    
        // Note that the export_id refers to the key field in your database table.
        $this->_init('infinity/infinitymailcall');
    }
}