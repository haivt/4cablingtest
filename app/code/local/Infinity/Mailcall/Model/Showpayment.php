<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Model_Showpayment
{
    
    public function toOptionArray()
    {
        $methods = Mage::helper('payment')->getStoreMethods(Mage::app()->getStore()->getId());
        $options = array();
        foreach ($methods as $method)
        {
            $mCode=$method->getCode();
            $mTitle=trim(strip_tags($method->getTitle()));
            $mTitle=empty($mTitle)?$mCode:$mTitle;
            array_unshift($options, array(
                'value' => $mCode,
                'label' => $mCode,
            ));
        }
        array_unshift($options, array(
                'value' => '',
                'label' => Mage::helper('infinitymailcall')->__('Allow All Payment Methods'),
            ));
        return $options;
    }
}