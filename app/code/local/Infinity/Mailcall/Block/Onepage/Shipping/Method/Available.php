<?php
/**
 * Infinity Mailcall
 *
 * @category    Infinity
 * @package     Infinity_Mailcall
 * @copyright   Copyright (c) 2011 Infinity Technologies (http://www.infinitytechnologies.com.au)
 * @author      Jason.zhang <jason.zhang@infinitytesting.com.au>
 */

class Infinity_Mailcall_Block_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
    
    public function getCarrierName($carrierCode)
    {
        if($carrierCode=='infinitymailcall'){
            $mailcall_url=$this->getUrl('media/mailcall',array('_secure'=>true));
            $mailcall_url=str_replace('/index.php','',$mailcall_url);
            $logo='<img alt="'.Mage::helper('infinitymailcall')->__('Want it now').'" src="'.$mailcall_url.Mage::getStoreConfig('carriers/'.$carrierCode.'/display_logos').'.png">';
                       //commented and changed by Mike @ Mailcall 02/08/2012
                        //return $logo;
                        echo $logo;
        }else{
            if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title')) {
                //commented and changed by Mike @ Mailcall 02/08/2012
                //return $logo;
                echo $name;
            }
        }
        //commented and changed by Mike @ Mailcall 02/08/2012
        //return $carrierCode;
    }

}