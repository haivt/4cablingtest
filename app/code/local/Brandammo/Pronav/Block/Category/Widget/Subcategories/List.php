<?php
class Brandammo_Pronav_Block_Category_Widget_Subcategories_List
extends Mage_Catalog_Block_Widget_Link {
	/**
	 * Initialize entity model
	 */
	protected function _construct() {
		parent::_construct();
		$this -> _entityResource = Mage::getResourceSingleton('catalog/category');
	}

	/**
	 * Prepare anchor text using passed text as parameter.
	 * If anchor text was not specified get entity name from DB.
	 *
	 * @return string
	 */
	public function getAnchorText() {
		if(!$this -> _anchorText && $this -> _entityResource) {
			if(!$this -> getData('anchor_text')) {
				$idPath = explode('/', $this -> _getData('id_path'));
				if(isset($idPath[1])) {
					$id = $idPath[1];
					if($id) {
						$this -> _anchorText = $this -> _entityResource -> getAttributeRawValue($id, 'name', Mage::app() -> getStore());
					}
				}
			} else {
				$this -> _anchorText = $this -> getData('anchor_text');
			}
		}

		return $this -> _anchorText;
	}
	
	
	public function getLevels() {
		return $this -> getData('levels'); 
		
	}
	
	public function getColumns() {
		return $this -> getData('columns'); 
		
	}
	
	public function getSubcategories () {
		$idPath = explode('/', $this -> _getData('id_path'));
		if(isset($idPath[1])) {
			$id = $idPath[1];
			if($id) {
				$cat = Mage::getModel('catalog/category')->load($id);
				/*Returns comma separated ids*/
				$subcats = $cat->getAllChildren();
				$subIds = explode(',',$subcats);
				//var_dump($subIds);
				return $subIds;
			}
			return array();
		}
		return array();
	}
	
	public function getSortedSubcategories()
    {
        $idPath = explode('/', $this -> _getData('id_path'));
		if(isset($idPath[1])) {
			$id = $idPath[1];
			if($id) {
				//return Mage::getModel('catalog/category')->getCollection()->addFieldToFilter('parent_id', $id)->addAttributeToSort('name', 'ASC');
				
				$cat = Mage::getModel('catalog/category')->load($id)->getAllChildren();
				/*Returns comma separated ids*/
				$subIds = explode(',',$cat);
				
				$categories = array();
				foreach($subIds as $subId) {
			    	$category = Mage::getModel('catalog/category')->load($subId);
			    	$categories[$category->getName()] = $category->getId();
				}
				ksort($categories, SORT_STRING);
				//var_dump($categories);
				return $categories;
			}
			return array();
		}
		return array();        
    }
	
	private function _removeStoreFromUrls($url) {
		//var_dump($url);
		return preg_replace('%(\?___store=\w{0,})%', '', $url);
	}

}