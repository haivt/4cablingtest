<?php

/**
 * Product:       Xtento_OrderExport (1.1.12)
 * ID:            Local Deploy
 * Packaged:      2013-02-16T17:35:07+01:00
 * Last Modified: 2013-02-11T16:33:10+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/Export/Data/Shared/Items.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_OrderExport_Model_Export_Data_Shared_Items extends Xtento_OrderExport_Model_Export_Data_Abstract
{
    public function getConfiguration()
    {
        // Init cache
        if (!isset($this->_cache['product_attributes'])) {
            $this->_cache['product_attributes'] = array();
        }
        // Return config
        return array(
            'name' => 'Item information',
            'category' => 'Shared',
            'description' => 'Export ordered/invoiced/shipped/refunded items of entity.',
            'enabled' => true,
            'apply_to' => array(Xtento_OrderExport_Model_Export::ENTITY_ORDER, Xtento_OrderExport_Model_Export::ENTITY_INVOICE, Xtento_OrderExport_Model_Export::ENTITY_SHIPMENT, Xtento_OrderExport_Model_Export::ENTITY_CREDITMEMO, Xtento_OrderExport_Model_Export::ENTITY_QUOTE),
        );
    }

    public function getExportData($entityType, $collectionItem)
    {
        // Set return array
        $returnArray = array();
        $this->_writeArray = & $returnArray['items'];
        // Fetch fields to export
        $object = $collectionItem->getObject();
        $order = $collectionItem->getOrder();
        $items = $object->getAllItems();
        if (empty($items)) {
            return $returnArray;
        }

        // Export item information
        $itemCount = 0;
        $totalQty = 0;
        foreach ($items as $item) {
            // Check if this product type should be exported
            if ($item->getProductType() && $this->getProfile() && in_array($item->getProductType(), explode(",", $this->getProfile()->getExportFilterProductType()))) {
                continue; // Product type should be not exported
            }
            // Export general item information
            $this->_writeArray = & $returnArray['items'][];
            $origWriteArray = & $this->_writeArray;
            $itemCount++;
            if ($entityType == Xtento_OrderExport_Model_Export::ENTITY_ORDER) {
                $itemQty = $item->getQtyOrdered();
                $itemQty = round($itemQty);
            } else {
                $itemQty = $item->getQty();
                $itemQty = round($itemQty);
            }
            $totalQty += $itemQty;
            $this->writeValue('qty_ordered', $itemQty);
            $this->writeValue('qty', $itemQty);

            $this->writeValue('item_number', $itemCount);
            $this->writeValue('order_product_number', $itemCount); // Legacy
            foreach ($item->getData() as $key => $value) {
                if ($key == 'qty_ordered' || $key == 'qty') continue;
                $this->writeValue($key, $value);
            }
            // Add fields of order item for invoice exports
            if ($entityType !== Xtento_OrderExport_Model_Export::ENTITY_ORDER && $entityType !== Xtento_OrderExport_Model_Export::ENTITY_QUOTE) {
                $this->_writeArray['order_item'] = array();
                $this->_writeArray =& $this->_writeArray['order_item'];
                if ($item->getOrderItemId()) {
                    $orderItem = Mage::getModel('sales/order_item')->load($item->getOrderItemId());
                    if ($orderItem->getId()) {
                        foreach ($orderItem->getData() as $key => $value) {
                            $this->writeValue($key, $value);
                        }
                    }
                }
            }
            // Add fields of parent item
            if ($item->getParentItem()) {
                $this->_writeArray['parent_item'] = array();
                $this->_writeArray =& $this->_writeArray['parent_item'];
                foreach ($item->getParentItem()->getData() as $key => $value) {
                    $this->writeValue($key, $value);
                }
            }
            // @todo: add "child field" (of this data type) so product attribute export can be disabled to speed up the export
            // Export product attributes
            $this->_writeArray['product_attributes'] = array();
            $this->_writeArray = & $this->_writeArray['product_attributes'];
            if (isset($this->_cache['product_attributes'][$order->getStoreId()]) && isset($this->_cache['product_attributes'][$order->getStoreId()][$item->getProductId()])) {
                // "cached"
                foreach ($this->_cache['product_attributes'][$order->getStoreId()][$item->getProductId()] as $attributeCode => $value) {
                    $this->writeValue($attributeCode, $value);
                }
            } else {
                $product = Mage::getModel('catalog/product')->setStoreId($order->getStoreId())->load($item->getProductId());
                if ($product->getId()) {
                    foreach ($product->getAttributes(null, true) as $productAttribute) {
                        $value = $productAttribute->getFrontend()->getValue($product);
                        $this->writeValue($productAttribute->getAttributeCode(), $value);
                        $this->_cache['product_attributes'][$order->getStoreId()][$item->getProductId()][$productAttribute->getAttributeCode()] = $value;
                    }
                }
            }

            $this->_writeArray = & $origWriteArray;
            // Export product options
            if ($options = $item->getProductOptions()) {
                // Export custom options
                if (isset($options['options'])) {
                    $this->_writeArray['custom_options'] = array();
                    foreach ($options['options'] as $customOption) {
                        $optionValues = explode(",", $customOption['option_value']);
                        $optionCount = 0;
                        foreach ($optionValues as $optionValue) {
                            $values = Mage::getModel('catalog/product_option_value')->load($optionValue);
                            if ($values->getOptionId()) {
                                $optionCount++;
                                $this->_writeArray = & $origWriteArray['custom_options'][];
                                $this->writeValue('name', $customOption['label']);
                                $this->writeValue('value', $customOption['value']);
                                $this->writeValue('sku', $values->getSku());
                            }
                        }
                        if ($optionCount === 0) {
                            $this->_writeArray = & $origWriteArray['custom_options'][];
                            $this->writeValue('name', $customOption['label']);
                            $this->writeValue('value', $customOption['value']);
                            $this->writeValue('sku', '');
                        }
                    }
                }
                // Export $options["attributes_info"].. maybe?
            }
        }
        $this->_writeArray = & $returnArray;
        $this->writeValue('export_total_qty_ordered', $totalQty);

        // Done
        return $returnArray;
    }
}