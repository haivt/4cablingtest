<?php

/**
 * Product:       Xtento_OrderExport (1.1.12)
 * ID:            Local Deploy
 * Packaged:      2013-02-16T17:35:07+01:00
 * Last Modified: 2013-01-08T21:51:08+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/Export/Data/Abstract.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

abstract class Xtento_OrderExport_Model_Export_Data_Abstract extends Mage_Core_Model_Abstract implements Xtento_OrderExport_Model_Export_Data_Interface
{
    protected $_cache;
    protected $_writeArray;

    protected function _construct()
    {
        $this->initConfiguration($this->getConfiguration());
    }

    protected function initConfiguration($configuration)
    {
        foreach ($configuration as $key => $value) {
            $this->setData($key, $value);
        }
    }

    /*
     * Is getDependsModule an installed module/extension?
     */
    public function confirmDependency()
    {
        if (!$this->getDependsModule()) {
            return true;
        }
        return Mage::helper('xtcore/utils')->isExtensionInstalled($this->getDependsModule());
    }

    protected function writeValue($field, $value, $customWriteArray = false)
    {
        if (!is_object($value)) {
            if (($field !== NULL && !is_array($value) && $value !== NULL && $value !== '') || ($this->getShowEmptyFields() && !is_array($value))) {
                if (!$customWriteArray) {
                    $this->_writeArray[$field] = $value;
                } else {
                    $this->_writeArray[$customWriteArray][$field] = $value;
                }
            } else if (is_array($value)) {
                foreach ($value as $k => $v) {
                    if (!is_array($v)) $this->writeValue($k, $v, $field);
                }
            }
        }
    }
}