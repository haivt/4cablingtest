<?php

/**
 * Product:       Xtento_OrderExport (1.1.12)
 * ID:            Local Deploy
 * Packaged:      2013-02-16T17:35:07+01:00
 * Last Modified: 2013-02-06T18:35:35+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/Output/Xml.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_OrderExport_Model_Output_Xml extends Xtento_OrderExport_Model_Output_Abstract
{
    public function convertData($exportArray)
    {
        if (!@class_exists('XMLWriter')) {
            Mage::throwException(Mage::helper('xtento_orderexport')->__('The XMLWriter class could not be found. This means your PHP installation is missing XMLWriter features. You cannot export XML/XSL types without XMLWriter. Please get in touch with your hoster or server administrator to add XMLWriter features.'));
        }
        // Some libxml settings
        $useInternalXmlErrors = libxml_use_internal_errors(true);
        if (function_exists('libxml_disable_entity_loader')) {
            $loadXmlEntities = libxml_disable_entity_loader(true);
        }
        libxml_clear_errors();

        #ini_set('xdebug.var_display_max_depth', 5);
        #Zend_Debug::dump($exportArray); die();
        $xmlWriter = Mage::getModel('xtento_orderexport/output_xml_writer');
        $xmlWriter->fromArray($exportArray);
        $outputXml = $xmlWriter->getDocument();
        if (libxml_get_last_error() !== FALSE) {
            $this->_throwXmlException(Mage::helper('xtento_orderexport')->__("Something is wrong with the internally processed XML markup. Please contact XTENTO."));
        }
        // Handle output if the profiles output format is directly the master XML format
        $profile = $this->getProfile();
        if ($profile->getOutputType() == 'xml') {
            // Output all fields into a XML file
            $filename = $this->_replaceFilenameVariables($profile->getFilename(), $exportArray);
            $charsetEncoding = $profile->getEncoding();
            $outputXml = $this->_changeEncoding($outputXml, $charsetEncoding);
            $outputData[$filename] = $outputXml;
        } else {
            // We use the output for the XSL Template
            $outputData[] = $outputXml;
        }

        // Reset libxml settings
        libxml_use_internal_errors($useInternalXmlErrors);
        if (function_exists('libxml_disable_entity_loader')) {
            libxml_disable_entity_loader($loadXmlEntities);
        }
        // Return data
        return $outputData;
    }
}