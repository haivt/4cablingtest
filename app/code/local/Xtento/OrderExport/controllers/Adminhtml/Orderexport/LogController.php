<?php

/**
 * Product:       Xtento_OrderExport (1.1.12)
 * ID:            Local Deploy
 * Packaged:      2013-02-16T17:35:07+01:00
 * Last Modified: 2013-01-23T13:32:49+01:00
 * File:          app/code/local/Xtento/OrderExport/controllers/Adminhtml/Orderexport/LogController.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_OrderExport_Adminhtml_OrderExport_LogController extends Xtento_OrderExport_Controller_Abstract
{
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function downloadAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $model = Mage::getModel('xtento_orderexport/log');
        $model->load($id);

        if ($id && !$model->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('xtento_orderexport')->__('This log entry does not exist anymore.'));
            return $this->_redirectReferer();
        }

        $filesNotFound = 0;
        $exportedFiles = array();
        $savedFiles = $model->getFiles();
        if (empty($savedFiles)) {
            Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('xtento_orderexport')->__('There is nothing to download. No files have been saved with this export.'));
            return $this->_redirectReferer();
        }
        $savedFiles = explode("|", $savedFiles);

        $baseFilenames = array();
        foreach ($savedFiles as $filePath) {
            array_push($baseFilenames, basename($filePath));
        }
        $baseFilenames = array_unique($baseFilenames);

        foreach ($baseFilenames as $filename) {
            $filePath = Mage::helper('xtento_orderexport/export')->getExportBkpDir() . $id . '_' . $filename;
            $data = @file_get_contents($filePath);
            if ($data === FALSE && !$this->getRequest()->getParam('force', false)) {
                $filesNotFound++;
                Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('xtento_orderexport')->__('File not found in local backup directory: %s', $filePath));
                if ($filesNotFound == count($baseFilenames)) {
                    return $this->_redirectReferer();
                }
            }
            $exportedFiles[$filename] = $data;
        }
        if ($filesNotFound > 0 && $filesNotFound !== count($baseFilenames) && !$this->getRequest()->getParam('force', false)) {
            Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('xtento_orderexport')->__('One or more files of this export have been deleted from the local backup directory. Please click <a href="%s">here</a> to download the remaining existing files.', Mage::helper('adminhtml')->getUrl('*/*/*', array('id' => $id, 'force' => true))));
            return $this->_redirectReferer();
        }

        return $this->_prepareFileDownload($exportedFiles);
    }

    public function deleteAction()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $model = Mage::getModel('xtento_orderexport/log');
        $model->load($id);

        if ($id && !$model->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('xtento_orderexport')->__('This log entry does not exist anymore.'));
            return $this->_redirectReferer();
        }

        try {
            $this->_deleteFilesFromFilesystem($model);
            $model->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('xtento_orderexport')->__('Log entry has been successfully deleted.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        return $this->_redirectReferer();
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('log');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('xtento_orderexport')->__('Please select log entries to delete.'));
            return $this->_redirect('*/*/');
        }

        try {
            foreach ($ids as $id) {
                $model = Mage::getModel('xtento_orderexport/log');
                $model->load($id);
                $this->_deleteFilesFromFilesystem($model);
                $model->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($ids)));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        return $this->_redirect('*/*/');
    }

    private function _deleteFilesFromFilesystem($model)
    {
        $savedFiles = $model->getFiles();
        if (empty($savedFiles)) {
            return false;
        }
        $savedFiles = explode("|", $savedFiles);

        $baseFilenames = array();
        foreach ($savedFiles as $filePath) {
            array_push($baseFilenames, basename($filePath));
        }
        $baseFilenames = array_unique($baseFilenames);

        foreach ($baseFilenames as $filename) {
            $filePath = Mage::helper('xtento_orderexport/export')->getExportBkpDir() . $model->getId() . '_' . $filename;
            @unlink($filePath);
        }
        return true;
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/orderexport')
            ->_title(Mage::helper('xtento_orderexport')->__('Sales Export'))->_title(Mage::helper('xtento_orderexport')->__('Execution Log'));
        return $this;
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/orderexport/log');
    }
}