<?php

/**
 * Product:       Xtento_OrderExport (1.1.12)
 * ID:            Local Deploy
 * Packaged:      2013-02-16T17:35:07+01:00
 * Last Modified: 2012-12-21T13:38:19+01:00
 * File:          app/code/local/Xtento/OrderExport/Block/Adminhtml/Tools/Import.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_OrderExport_Block_Adminhtml_Tools_Import extends Mage_Adminhtml_Block_Template
{
}