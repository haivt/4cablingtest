<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin Config Package && Theme*/
$config = new Mage_Core_Model_Config();
$config->saveConfig('design/package/name','ultimo');
$config->saveConfig('design/theme/template','4cabling');
$config->saveConfig('design/theme/skin','4cabling');
$config->saveConfig('design/theme/layout','4cabling');
$config->saveConfig('design/theme/default','4cabling');
/*End Config Themes*/

$installer->endSetup();