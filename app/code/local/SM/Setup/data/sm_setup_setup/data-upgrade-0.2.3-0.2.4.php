<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$_content = <<<EOD
    {{block type="cms/block" block_id="feature_banner"}}
    {{block type="catalog/product_new"name="home.catalog.product.new" alias="product_homepage" template="catalog/product/new.phtml" block_name="NEW PRODUCTS"}}
    <div class="clearer"></div>
    {{block type="cms/block" block_id="custom_banner"}}
EOD;

$homePage = Mage::getModel('cms/page')->load('home_page');
if ($homePage->getId()) {
    $homePage->setContent($_content)->save();
}

$installer->endSetup();