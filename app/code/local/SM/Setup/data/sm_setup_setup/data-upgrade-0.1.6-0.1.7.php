<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin update static block block_footer_row2_column5*/

$content = <<<EOD
    <div class="feature-banner">
	<div class="grid12-4 no-gutter item first">
		<div class="feature feature-icon-hover indent large">
			<h6 class="above-heading">shop server racks</h6>
			<a href="#">
				<img src="{{skin url='images/banners/f01.png'}}" alt="Customizable design" />
			</a>
		</div>
    </div>
    <div class="grid12-4 no-gutter item">
        <div class="feature feature-icon-hover indent large">
            <h6 class="above-heading">shop cat5 & cat6 cables</h6>
			<a href="#">
                <img src="{{skin url='images/banners/f01.png'}}" alt="Responsive Layout" />
            </a>
        </div>
    </div>
    <div class="grid12-4 no-gutter item last">
        <div class="feature feature-icon-hover indent large">
            <h6 class="above-heading">shop cable management</h6>
			<a href="#">
                <img src="{{skin url='images/banners/f01.png'}}" alt="Mega Menu" />
            </a>
        </div>
    </div>
</div>

EOD;

$block = Mage::getModel('cms/block')->load('feature_banner');
if ($block->getId()) {
    $block->setContent($content)->save();
}

$content = <<<EOD
    <div class="nested-container">
	<div class="page-banners grid-container">
		<div class="grid12-6 banner">
			<a href="#">
				<img class="fade-on-hover" src="{{skin url="images/slideshow/banner/cms01.png"}}" alt="Magento discount code" title="Use discount code/coupon on cart page" />
			</a>
		</div>
		<div class="grid12-3 banner fade-on-hover hide-below-768">
			<a href="#">
				<img class="fade-on-hover" src="{{skin url="images/slideshow/banner/cms02.png"}}" alt="Magento discount code" title="Use discount code/coupon on cart page" />
			</a>
		</div>
		<div class="grid12-3 banner fade-on-hover hide-below-768 ">
			<a href="#">
				<img class="fade-on-hover" src="{{skin url="images/slideshow/banner/cms03.png"}}" alt="Magento discount code" title="Use discount code/coupon on cart page" />
			</a>
		</div>
	</div>
</div>
EOD;

$block = Mage::getModel('cms/block')->load('custom_banner');
if ($block->getId()) {
    $block->setContent($content)->save();
}

$installer->endSetup();