<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Update static block block_header_top_right*/
$_model = Mage::getModel('cms/block')->load('block_header_top_right');
if(is_object($_model)){
    if($_model->getId()){
        $_model->setIsActive(1)->setStores(array('2'))->save();
    }
}
/*End create static block fourcabling_footer_link*/

/*Update content for static block block_footer_social*/
$_block_footer_social = <<<EOD
        <ul class="social">
            <li class="googleplus-icon"><a href="https://plus.google.com/114263226243582689318" target="_blank">googleplus</a></li>
            <li class="linkedin-icon"><a href="http://www.linkedin.com/company/4cabling" target="_blank">linkedin</a></li>
            <li class="twitter-icon"><a href="https://twitter.com/4Cabling" target="_blank">twitter</a></li>
            <li class="facebook-icon"><a href="https://www.facebook.com/4Cabling" target="_blank">facebook</a></li>
        </ul>
EOD;

$_blockModel = Mage::getModel('cms/block')->load('block_footer_social');
if(is_object($_blockModel)){
    if($_blockModel->getId()){
        $_blockModel->setContent($_block_footer_social)->save();
    }
}
/*End static block block_footer_social*/
$installer->endSetup();