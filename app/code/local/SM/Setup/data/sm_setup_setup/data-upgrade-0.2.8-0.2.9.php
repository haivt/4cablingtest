<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$content = <<<EOD
<img src="{{skin url='images/slideshow/GWP-MAR-2014-Homepage_banner.jpg'}}" alt="" />
EOD;
$block = Mage::getModel('cms/block')->load('block_slide21');
if ($block->getId()) {
    $block->setContent($content)->save();
}

$content = <<<EOD
<img src="{{skin url='images/slideshow/CAT5andCAT6_mini.jpg'}}" alt="" />
EOD;

$block = Mage::getModel('cms/block')->load('block_slide22');
if ($block->getId()) {
    $block->setContent($content)->save();
}

$content = <<<EOD
<img src="{{skin url='images/slideshow/9.90ITSBACK.jpg'}}" alt="" />
EOD;

$block = Mage::getModel('cms/block')->load('block_slide23');
if ($block->getId()) {
    $block->setContent($content)->save();
}

$content = <<<EOD
<img src="{{skin url='images/slideshow/sound-step-lightning-giveaway-big.jpg'}}" alt="" />
EOD;

$block = Mage::getModel('cms/block')->load('block_slide24');
if ($block->getId()) {
    $block->setContent($content)->save();
}

$content = <<<EOD
<img src="{{skin url='images/slideshow/LoyaltySlide_mini.jpg'}}" alt="" />
EOD;
$staticBlock = array(
    'title' => 'Home slide 25',
    'identifier' => 'block_slide25',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('block_slide25');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$Config = new Mage_Core_Model_Config();
$Config ->saveConfig('ultraslideshow/general/blocks', "block_slide21,block_slide22,block_slide23,block_slide24,block_slide25","stores",2);

$installer->endSetup();