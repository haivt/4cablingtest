<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin update static block block_footer_row2_column5*/

$_content = <<<EOD
    {{block type="cms/block" block_id="feature_banner"}}
    {{block type="catalog/product_new"name="home.catalog.product.new" alias="product_homepage" template="catalog/product/new.phtml" block_name="NEW PRODUCTS" products_count="4"}}
    <div class="clearer"></div>
    {{block type="cms/block" block_id="custom_banner"}}
EOD;
    $pagedata = Array (
        'title' => 'Home page',
        'root_template' => 'one_column',
        'identifier' => 'home_page',
        'content' => $_content,
        'is_active' => 1,
        'stores' => array(0),
        'sort_order' => 0
    );
    $homePage = Mage::getModel('cms/page')->load('home_page');
    if (!$homePage->getId()) {
        $homePage->setData($pagedata)->save();
    }

    $Config = new Mage_Core_Model_Config();
    $Config ->saveConfig('web/default/cms_home_page', "home_page");

$content = <<<EOD
    <p><a class="fade-on-slideshow-hover" title="Click to see all features" href="{{store url='ultimo-responsive-magento-theme'}}"> <img src="{{skin url='images/slideshow/main.gif'}}" alt="Customizable Magento Theme" /></a></p>
EOD;
$staticBlock = array(
    'title' => 'Slide 1',
    'identifier' => 'block_slide1',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('block_slide1');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
    <a class="banner fade-on-hover" href="{{store url='about-magento-demo-store'}}">
	<img src="{{skin url='images/slideshow/banner/right01.gif'}}" alt="Sample banner" />
    </a>
    <a class="banner fade-on-hover" href="{{store url='about-magento-demo-store'}}">
	<img src="{{skin url='images/slideshow/banner/right02.gif'}}" alt="Sample banner" />
    </a>
    <a class="banner fade-on-hover" href="{{store url='about-magento-demo-store'}}">
        <img src="{{skin url='images/slideshow/banner/right03.gif'}}" alt="Sample banner" />
    </a>
    <a class="banner fade-on-hover" href="{{store url='about-magento-demo-store'}}">
        <img src="{{skin url='images/slideshow/banner/right04.gif'}}" alt="Sample banner" />
    </a>
EOD;
$staticBlock = array(
    'title' => 'Slideshow banners',
    'identifier' => 'block_slideshow_banners',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('block_slideshow_banners');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
    <div class="grid12-4">
	<div class="feature feature-icon-hover indent large">
		<a href="#">
			<img src="{{skin url='images/banners/pro01.gif'}}" alt="Customizable design" />
		</a>
		<h6 class="above-heading">Customizable design</h6>
	</div>
    </div>
    <div class="grid12-4">
        <div class="feature feature-icon-hover indent large">
            <a href="#">
                <img src="{{skin url='images/banners/pro02.gif'}}" alt="Responsive Layout" />
            </a>
            <h6 class="above-heading">12-column grid</h6>
        </div>
    </div>
    <div class="grid12-4">
        <div class="feature feature-icon-hover indent large">
            <a href="#">
                <img src="{{skin url='images/banners/pro03.gif'}}" alt="Mega Menu" />
            </a>
            <h6 class="above-heading">Customizable drop-down menu</h6>
        </div>
    </div>
    <div class="clearer"></div>
EOD;
$staticBlock = array(
    'title' => 'Feature banner',
    'identifier' => 'feature_banner',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('feature_banner');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->setTitle("Feature banner")->save();
}

$content = <<<EOD
    <div class="nested-container">
	<div class="page-banners grid-container">
                <div class="grid12-6 banner">
				<img class="fade-on-hover" src="{{skin url="images/slideshow/banner/cms01.gif"}}" alt="Magento discount code" title="Use discount code/coupon on cart page" />
                           <p class='custom_banner_title'>
                                    <span>This February</span></br>
                                    <span>get rollin's with a</span></br>
                                    <span>STM jet roller</span></br>
                                    <span>wheeled laptop bag</span></br>
                           </p>
                           <p class='custom_banner_value'>Get it free with all orders </br> about $300.</p>
		</div>
		<div class="grid12-3 banner fade-on-hover hide-below-768">
                        <p class='custom_banner_price'><span class="price_int">$9</span class='price_unit'><span>.90</span></p>
                        <p class='text_flat_rate'><span>FLAT RATE</span></br><span>SHIPPING</span></p>
		</div>
		<div class="grid12-3 banner fade-on-hover hide-below-768">
                        <p class='text_trade_outlet'><span>TRADE OUTLET</span></p>
                        <p class='text_address'><span>17 / 31 MADDOX STREET</span></br><span>PO BOX 7330</span></br><span>ALEXANDRIA, NWS 2015</span></p>
                        <p class='text_calendar'><span><b>MON - FRI:</b></span><span>7.30AM - 5.00PM</span></br><span><b>SAT:</b></span><span>9.00AM - 1.00PM</span></p>
		</div>
	</div>
</div>

EOD;
$staticBlock = array(
    'title' => 'Custom Banner',
    'identifier' => 'custom_banner',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('custom_banner');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$installer->endSetup();