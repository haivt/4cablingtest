<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin Update new category name*/
$_oldCat = array(
    'rca-audio-cables' => '> RCA Audio Cables',
    'toslink-cables' => '> Toslink Cables',
    '3-5mm-stereo-cables' => '> 3.5mm Stereo Cables',
    'xlr-leads' => '> XLR Leads',
);
foreach($_oldCat as $urlKey => $_catName){
    $_catModel = Mage::getModel('catalog/category')->loadByAttribute('url_key',$urlKey);
    if(is_object($_catModel)){
        if($_catModel->getId()){
            $_catModel->setName($_catName)->save();
        }
    }

}
/*End Update new category name*/
$installer->endSetup();