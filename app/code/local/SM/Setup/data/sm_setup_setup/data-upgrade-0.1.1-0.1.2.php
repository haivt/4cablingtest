<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Create Static Block block_header_top_left*/
$block_header_top_left = <<<EOD
<div class="hide-below-768" title="You can put here a phone number or some additional help info"><span class="icon i-telephone no-bg-color"></span>Call Us: <span class="number-telephone">+61 2 9565 4077</span></div>
EOD;

$block_header_top_right = <<<EOD
<div class="links-wrapper-separators hide-below-480">
	<ul class="links">
		<li class="first">
			<a href="{{store direct_url ='about'}}" title="About">About</a>
		</li>
		<li>
			<a href="{{store url='government'}}" title="Government">Government</a>
		</li>
		<li>
			<a href="{{store url='media'}}" title="Media">Media</a>
		</li>
		<li>
			<a href="{{store url='faq'}}" title="FAQ">FAQ</a>
		</li>
		<li class="last">
			<a href="{{store url='contacts'}}" title="Contact">Contact</a>
		</li>
	</ul>
</div>
EOD;

$_staticArr = array(
    'block_header_top_left' => array(
        'title' => 'Header top left',
        'content' => $block_header_top_left
    ),
    'block_header_top_right' => array(
        'title' => 'Header top right',
        'content' => $block_header_top_right,
    )
);

foreach($_staticArr as $_identifier => $content){
    $_static = array(
        'identifier' => $_identifier,
        'title'      => $content['title'],
        'content'    => $content['content'],
        'is_active'  => 1,
        'stores'    => array(0)
    );
    $_model = Mage::getModel('cms/block')->load($_identifier);
    if(!$_model->getId()){
        $_model->setData($_static)->save();
    }else{
        $_model->setContent($content['content'])->save();
    }
}
/*End Create Static Block block_header_top_left */
$installer->endSetup();