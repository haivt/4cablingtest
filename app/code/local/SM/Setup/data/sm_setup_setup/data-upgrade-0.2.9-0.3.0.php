<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$cfg = new Mage_Core_Model_Config();
$cfg->saveConfig("amshopby/general/use_custom_ranges",0);
$cfg->saveConfig("amshopby/general/price_type",3);
$cfg->saveConfig("amshopby/general/slider_type",0);
$cfg->saveConfig("amshopby/general/price_from_to",1);
$installer->endSetup();