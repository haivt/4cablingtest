<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$content = <<<EOD
<img src="{{skin url='images/slideshow/home_slide01.jpg'}}" alt="" />
EOD;
$staticBlock = array(
    'title' => 'Home slide 21',
    'identifier' => 'block_slide21',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('block_slide21');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
<img src="{{skin url='images/slideshow/home_slide02.jpg'}}" alt="" />
EOD;
$staticBlock = array(
    'title' => 'Home slide 22',
    'identifier' => 'block_slide22',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('block_slide22');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
<img src="{{skin url='images/slideshow/home_slide03.jpg'}}" alt="" />
EOD;
$staticBlock = array(
    'title' => 'Home slide 23',
    'identifier' => 'block_slide23',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('block_slide23');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$content = <<<EOD
<img src="{{skin url='images/slideshow/home_slide04.jpg'}}" alt="" />
EOD;
$staticBlock = array(
    'title' => 'Home slide 24',
    'identifier' => 'block_slide24',
    'content' => $content,
    'is_active' => 1,
    'stores' => array(0)
);
$block = Mage::getModel('cms/block')->load('block_slide24');
if (!$block->getId()) {
    Mage::getModel('cms/block')->setData($staticBlock)->save();
} else {
    $block->setContent($content)->save();
}

$Config = new Mage_Core_Model_Config();
$Config ->saveConfig('ultraslideshow/general/blocks', "block_slide21,block_slide22,block_slide23,block_slide24","stores",2);

$installer->endSetup();