<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$cfg = new Mage_Core_Model_Config();
$cfg ->saveConfig('ultimo/category_grid/column_count', 3,"stores",2);
$installer->endSetup();