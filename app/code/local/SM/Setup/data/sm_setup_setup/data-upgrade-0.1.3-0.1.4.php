<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin update static block block_footer_row2_column5*/

$_content = <<<EOD
    <div class=" grid12-12">
        <div class="std">
            <div class="grid12-9 no-gutter">

                <div class="grid12-3">
                    <div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
                        <h6 class="block-title heading">Products</h6>

                        <div class="block-content" style="display: none;">

                            <ul class="bullet">
                                <li><a href="{{store direct_url='cable.html'}}">Cables</a></li>
                                <li><a href="{{store direct_url='server-racks-network-cabinets.html'}}">Server Racks</a></li>
                                <li><a href="{{store direct_url='cable-management.html'}}">Cable Management</a></li>
                                <li><a href="{{store direct_url='networking.html'}}">Networking</a></li>
                                <li><a href="{{store direct_url='fibre-products.html'}}">Fibre Products</a></li>
                                <li><a href="{{store direct_url='power.html'}}">Power</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="grid12-3">
                    <div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
                        <h6 class="block-title heading">Clients</h6>

                        <div class="block-content" style="display: none;">

                            <ul class="bullet">
                                <li><a href="{{store direct_url='trade'}}">Trade</a></li>
                                <li><a href="{{store direct_url='resellers'}}">Resellers</a></li>
                                <li><a href="{{store direct_url='education'}}">Education</a></li>
                                <li><a href="{{store direct_url='government'}}">Government</a></li>
                                <li><a href="{{store direct_url='installation'}}">Installation</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="grid12-3">
                    <div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
                        <h6 class="block-title heading">Help</h6>

                        <div class="block-content" style="display: none;">

                            <ul class="bullet">
                                <li><a href="{{store direct_url='shipping'}}">Shipping</a></li>
                                <li><a href="{{store direct_url='sales/guest/form/'}}">Orders & Returns</a></li>
                                <li><a href="{{store direct_url='terms.html'}}">Terms & Conditions</a></li>
                                <li><a href="{{store direct_url='privacy'}}">Privacy</a></li>
                                <li><a href="{{store direct_url='cookies'}}">Cookies</a></li>
                                <li><a href="{{store direct_url='catalog/seo_sitemap/category/'}}">Sitemap</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="grid12-3">
                    <div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
                        <h6 class="block-title heading">Company</h6>

                        <div class="block-content" style="display: none;">

                            <ul class="bullet">
                                <li><a href="{{store direct_url ='about.html'}}">About Us</a></li>
                                <li><a href="{{store direct_url='contacts'}}">Contact</a></li>
                                <li><a href="{{store direct_url='faq'}}">FAQ</a></li>
                                <li><a href="{{store direct_url='loyaltyprogram'}}">Loyatly Program</a></li>
                                <li><a href="{{store direct_url='media.html'}}">Media</a></li>
                                <li><a href="{{store direct_url='testimonials'}}">Testimonials</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

            </div>
            <div class="grid12-3 no-right-gutter">
                <div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
                    <h6 class="block-title heading">Retail Store</h6>

                    <div class="block-content" style="display: none;">

                        <div class="feature indent first feature-icon-hover">
                            <p class="no-margin">17/31 Maddox Street<br>PO BOX 7330<br>Alexandria, NSW 2015</p>
                        </div>
                        <div class="feature indent feature-icon-hover">
                            <p class="no-margin">MON - FRI: 7.30AM - 5.00PM<br>SAT:9.00AM - 1.00AM</p>
                        </div>
                        <div class="feature indent feature-icon-hover">
                            <p class="no-margin">Tel: +61 2 9565 4077<br>Fax: +61 2 9565 4088</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

EOD;

$_cmsModel = Mage::getModel('cms/block')->load('block_footer_row2_column5');
if(!$_cmsModel->getId()){
    $_static = array(
        'identifier' => 'block_footer_row2_column5',
        'content'    => $_content,
        'title'     => 'Footer - row 2 column 5',
        'is_active' => 1,
        'stores'    => array(0)
    );
    $_cmsModel->setData($_static)->save();
}else{
    $_cmsModel->setContent($_content)->save();
}

/*End update static block block_footer_row2_column5*/


/*Begin Create static block block_footer_social*/
    $_block_footer_social = <<<EOD
        <ul class="social">
            <li class="facebook-icon"><a href="https://www.facebook.com/4Cabling" target="_blank">facebook</a></li>
            <li class="twitter-icon"><a href="https://twitter.com/4Cabling" target="_blank">twitter</a></li>
            <li class="linkedin-icon"><a href="http://www.linkedin.com/company/4cabling" target="_blank">linkedin</a></li>
            <li class="googleplus-icon"><a href="https://plus.google.com/114263226243582689318" target="_blank">googleplus</a></li>
        </ul>
EOD;
    $_static = array(
        'identifier' => 'block_footer_social',
        'content'    => $_block_footer_social,
        'title'     => 'Footer - Social',
        'is_active' => 1,
        'stores'    => array(0)
    );
    $_model = Mage::getModel('cms/block')->load('block_footer_social');
    if(!$_model->getId()){
        $_model->setData($_static)->save();
    }else{
        $_model->setContent($_block_footer_social)->save();
    }

/*Begin Create static block block_footer_social*/

/*End update static block block_footer_row2_column5*/


/*Begin Create static block block_footer_payment*/
$block_footer_payment = <<<EOD
<img src="{{skin url='images/payments.gif'}}" alt="Payment methods" title="Payment methods"/>
EOD;
$_static = array(
    'identifier' => 'block_footer_payment',
    'content'    => $block_footer_payment,
    'title'     => 'Footer - Payment Method',
    'is_active' => 1,
    'stores'    => array(0)
);
$_model = Mage::getModel('cms/block')->load('block_footer_payment');
if(!$_model->getId()){
    $_model->setData($_static)->save();
}else{
    $_model->setContent($block_footer_payment)->save();
}

/*Begin Create static block block_footer_social*/

/*Begin update copyright*/
    $_cfg = new Mage_Core_Model_Config();
    $_cfg->saveConfig('design/footer/copyright','Copyright &copy; 2014 Cabling Group. All rights reserved.');
/*End update copyright*/

/*Begin Create static block block_header_top_right*/
$block_header_top_right = <<<EOD
<div class="links-wrapper-separators hide-below-480">
	<ul class="links">
		<li class="first">
			<a href="{{store direct_url ='about.html'}}" title="About">About</a>
		</li>
		<li>
			<a href="{{store url='government'}}" title="Government">Government</a>
		</li>
		<li>
			<a href="{{store url='media.html'}}" title="Media">Media</a>
		</li>
		<li>
			<a href="{{store url='faq'}}" title="FAQ">FAQ</a>
		</li>
		<li class="last">
			<a href="{{store url='contacts'}}" title="Contact">Contact</a>
		</li>
	</ul>
</div>
EOD;

$_staticModel = Mage::getModel('cms/block')->load('block_header_top_right');
if($_staticModel->getId()){
    $_staticModel->setContent($block_header_top_right)->save();
}
/*End Create static block block_header_top_right*/

$installer->endSetup();