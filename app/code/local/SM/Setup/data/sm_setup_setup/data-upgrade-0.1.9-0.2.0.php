<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$Config = new Mage_Core_Model_Config();
$Config ->saveConfig('ultraslideshow/banners/banners', "block_slideshow_banners","stores",2);

$installer->endSetup();