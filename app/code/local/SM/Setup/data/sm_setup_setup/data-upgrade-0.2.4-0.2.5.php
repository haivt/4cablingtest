<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
/*Begin Config Keep Image Aspect Ratio*/
$cfg = new Mage_Core_Model_Config();
$cfg->saveConfig('cloudzoom/images/aspect_ratio',0);
$cfg->saveConfig('cloudzoom/images/main_width',276);
$cfg->saveConfig('cloudzoom/images/main_height',276);
/*End Config Keep Image Aspect Ratio*/

/*Begin config Base Skin URL && Base Media URL*/
$cfg->saveConfig('web/unsecure/base_skin_url','{{unsecure_base_url}}skin/','stores',2);
$cfg->saveConfig('web/unsecure/base_media_url','{{unsecure_base_url}}media/','stores',2);
/*End config Base Skin URL && Base Media URL*/
$installer->endSetup();