<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$Config = new Mage_Core_Model_Config();
$Config ->saveConfig('ultimo/category_grid/column_count', 4,"stores",2);

$installer->endSetup();