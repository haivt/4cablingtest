<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin Set Translations for theme*/
$cfg = new Mage_Core_Model_Config();
$cfg->saveConfig('design/theme/locale','4cabling');
/*End Create Static Block block_header_top_left */

/*Begin Set don't show Home Link in Menu*/
$cfg->saveConfig('ultramegamenu/mainmenu/home',0);
/*Begin Set don't show Home Link in Menu*/
$installer->endSetup();