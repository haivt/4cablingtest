<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin update static block for category*/
$_urlCat = array(
    'cable'=> array('computer-cables','cat5-patch-leads','cat6-patch-leads','cable-rolls-boxes','video-cables','audio-cables','power-cables','charging-cables','usb-cables')
);
$_rightBlock = <<<EOD
<img class="menu-banner-right" src="{{skin url='images/right-banner.png'}}" alt="" />
EOD;

$_topBlock = <<<EOD
<img class="menu-banner-top" src="{{skin url='images/banner.png'}}" alt="" />
EOD;


foreach($_urlCat as $_keyCat => $_subCat){
    $_catModel = Mage::getModel('catalog/category')->loadByAttribute('url_key',$_keyCat);
    if(is_object($_catModel)){
        if($_catModel->getId()){
            $_catModel->setUmmCatBlockProportions('5/1')->setUmmCatBlockRight($_rightBlock)->save();
        }
    }

    foreach($_subCat as $_subUrlKey){
        $_catModel = Mage::getModel('catalog/category')->loadByAttribute('url_key',$_subUrlKey);
        if(is_object($_catModel)){
            if($_catModel->getId()){
                $_catModel->setUmmCatBlockTop($_topBlock)->save();
            }
        }
    }
}
/*End update static block for category*/

$installer->endSetup();