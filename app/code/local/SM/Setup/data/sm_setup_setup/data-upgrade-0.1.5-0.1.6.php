<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin config Style of the Main Menu & Custom Block - Align Right*/
$cfg = new Mage_Core_Model_Config();
$cfg->saveConfig("ultramegamenu/mainmenu/classic_menu",0);
$cfg->saveConfig("ultramegamenu/mainmenu/wide_menu",1);
$cfg->saveConfig("ultramegamenu/mainmenu/custom_block_floating",1);
/*End config Style of the Main Menu & Custom Block - Align Right*/

/*Begin config Show SKU & Related Products Layout*/
$cfg->saveConfig("ultimo/product_page/sku",1);
$cfg->saveConfig("ultimo/product_page/related_template",'catalog/product/list/related_multi_custom.phtml');
$cfg->saveConfig("ultimo/product_page/related_position",30);
/*End config Show SKU*/

/*Begin update static block block_product_banner*/
$block_product_banner = <<<EOD
<img src="{{skin url='images/banner_1.jpg'}}" title="" />
<img src="{{skin url='images/banner_2.jpg'}}" title="" />
EOD;
$_static = array(
    'identifier' => 'block_product_banner',
    'content'    => $block_product_banner,
    'title'      => "Block Product Banner",
    'is_active'  => 1,
    'stores'     => array(0)
);

$_model = Mage::getModel('cms/block')->load('block_product_banner');
if(is_object($_model)){
    if(!$_model->getId()){
        $_model->setData($_static)->save();
    }else{
        $_model->setContent($block_product_banner)->save();
    }
}


/*Begin update static block block_product_secondary_bottom*/

$installer->endSetup();