<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
/*Begin create static block fourcabling_footer_link*/
$_fourcabling_footer_link = <<<EOD
<div class=" grid12-12">
	<div class="std">
		<div class="grid12-9 no-gutter">

			<div class="grid12-3 no-first-gutter">
				<div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
					<h6 class="block-title heading">Products</h6>

					<div class="block-content" style="display: none;">

						<ul class="bullet">
							<li><a href="{{store direct_url='cable.html'}}">Cables</a></li>
							<li><a href="{{store direct_url='server-racks-network-cabinets.html'}}">Server Racks</a></li>
							<li><a href="{{store direct_url='cable-management.html'}}">Cable Management</a></li>
							<li><a href="{{store direct_url='networking.html'}}">Networking</a></li>
							<li><a href="{{store direct_url='fibre-products.html'}}">Fibre Products</a></li>
							<li><a href="{{store direct_url='power.html'}}">Power</a></li>
						</ul>

					</div>
				</div>
			</div>

			<div class="grid12-3">
				<div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
					<h6 class="block-title heading">Clients</h6>

					<div class="block-content" style="display: none;">

						<ul class="bullet">
							<li><a href="{{store direct_url='trade'}}">Trade</a></li>
							<li><a href="{{store direct_url='resellers'}}">Resellers</a></li>
							<li><a href="{{store direct_url='education'}}">Education</a></li>
							<li><a href="{{store direct_url='government'}}">Government</a></li>
							<li><a href="{{store direct_url='installation'}}">Installation</a></li>
						</ul>

					</div>
				</div>
			</div>

			<div class="grid12-3">
				<div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
					<h6 class="block-title heading">Help</h6>

					<div class="block-content" style="display: none;">

						<ul class="bullet">
							<li><a href="{{store direct_url='shipping'}}">Shipping</a></li>
							<li><a href="{{store direct_url='sales/guest/form/'}}">Orders & Returns</a></li>
							<li><a href="{{store direct_url='terms.html'}}">Terms & Conditions</a></li>
							<li><a href="{{store direct_url='privacy'}}">Privacy</a></li>
							<li><a href="{{store direct_url='cookies'}}">Cookies</a></li>
							<li><a href="{{store direct_url='catalog/seo_sitemap/category/'}}">Sitemap</a></li>
						</ul>

					</div>
				</div>
			</div>

			<div class="grid12-3">
				<div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
					<h6 class="block-title heading">Company</h6>

					<div class="block-content" style="display: none;">

						<ul class="bullet">
							<li><a href="{{store direct_url ='about.html'}}">About Us</a></li>
							<li><a href="{{store direct_url='contacts'}}">Contact</a></li>
							<li><a href="{{store direct_url='faq'}}">FAQ</a></li>
							<li><a href="{{store direct_url='loyaltyprogram'}}">Loyatly Program</a></li>
							<li><a href="{{store direct_url='media.html'}}">Media</a></li>
							<li><a href="{{store direct_url='testimonials'}}">Testimonials</a></li>
						</ul>

					</div>
				</div>
			</div>

		</div>
		<div class="grid12-3 no-right-gutter">
			<div class="collapsible mobile-collapsible"><span class="opener">&nbsp;</span>
				<h6 class="block-title heading">Retail Store</h6>

				<div class="block-content" style="display: none;">

					<div class="feature indent first feature-icon-hover">
						<p class="no-margin">17/31 Maddox Street<br>PO BOX 7330<br>Alexandria, NSW 2015</p>
					</div>
					<div class="feature indent feature-icon-hover">
						<p class="no-margin">MON - FRI: 7.30AM - 5.00PM<br>SAT:9.00AM - 1.00AM</p>
					</div>
					<div class="feature indent feature-icon-hover">
						<p class="no-margin">Tel: +61 2 9565 4077<br>Fax: +61 2 9565 4088</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
EOD;

$_staticBlock = array(
    'title' => '4Cabling - Footer Link',
    'identifier' => 'fourcabling_footer_link',
    'content'   => $_fourcabling_footer_link,
    'is_active' => 1,
    'stores'    => array(2)
);

$_model = Mage::getModel('cms/block')->load('fourcabling_footer_link');
if(is_object($_model)){
    if(!$_model->getId()){
        $_model->setData($_staticBlock)->save();
    }else{
        $_model->setContent($_fourcabling_footer_link)->save();
    }
}

/*End create static block fourcabling_footer_link*/
$installer->endSetup();