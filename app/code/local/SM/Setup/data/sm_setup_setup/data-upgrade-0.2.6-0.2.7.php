<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/*Begin Merge css for store 4cabling*/
$cfg = new Mage_Core_Model_Config();
$cfg->saveConfig('dev/css/merge_css_files',1,'stores',2);
/*End Merge css for store 4cabling*/

$installer->endSetup();