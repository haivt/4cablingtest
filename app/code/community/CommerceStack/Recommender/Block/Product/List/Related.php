<?php

class CommerceStack_Recommender_Block_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{
    protected $_linkSource = array('useLinkSourceManual', 'useLinkSourceCommerceStack'); // from most to least authoritative

    protected function _prepareData()
    {
        $limit = Mage::getStoreConfig('recommender/relatedproducts/numberofrelatedproducts');
        
        /* @var $product Mage_Catalog_Model_Product */
        $product = Mage::registry('product');
        
        // A bit of a hack, but return an empty collection if user selected 0 recommendations to show in config
        if($limit < 1)
        {
            $this->_itemCollection = $product->getRelatedProductCollection();
            $this->_itemCollection->load();
            $this->_itemCollection->clear();
            return $this;
        }

        $unionLinkedItemCollection = null;
        foreach($this->_linkSource as $linkSource)
        {
            $numRecsToGet = $limit;
            if(!is_null($unionLinkedItemCollection))
            {
                $numRecsToGet = $limit - count($unionLinkedItemCollection);
            }
            
            if($numRecsToGet > 0)
            {
                // Set link source to manual or automated CommerceStack recommendations
                $linkModel = $product->getLinkInstance();
                $linkModel->{$linkSource}();
                
                $linkedItemCollection = $product->getRelatedProductCollection()
                ->addAttributeToSelect('required_options')
                ->setGroupBy()
                ->setPositionOrder()
                ->addStoreFilter();
                
                $linkedItemCollection->getSelect()->limit($numRecsToGet);
                
                if(!is_null($unionLinkedItemCollection))
                {
                    $linkedItemCollection->addExcludeProductFilter($unionLinkedItemCollection->getAllIds());
                }
                
                Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($linkedItemCollection,
                    Mage::getSingleton('checkout/session')->getQuoteId()
                );
                $this->_addProductAttributesAndPrices($linkedItemCollection);
        
        //        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($this->_itemCollection);
                Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($linkedItemCollection);
        
                $linkedItemCollection->load();
                
                if(is_null($unionLinkedItemCollection))
                {
                    $unionLinkedItemCollection = $linkedItemCollection;
                }
                else
                {
                    // Add new source linked items to existing union of linked items
                    foreach($linkedItemCollection as $linkedProduct)
                    {
                        $unionLinkedItemCollection->addItem($linkedProduct);
                    }
                }
            }
        }

        // Get category of current product
        $currentCategory = null;
        if(count($unionLinkedItemCollection) < $limit)
        {
            $currentCategory = Mage::registry('current_category');
            if(is_null($currentCategory))
            {
                // This could be a recently viewed or a search page. Try to get category collection and arbitrarily use first
                /* @var $currentProduct Mage_Catalog_Model_Product */
                $currentProduct = Mage::registry('current_product');
                $currentCategory = $currentProduct->getCategoryCollection();
                $currentCategory = $currentCategory->getFirstItem();
            }
        }
        
        $useCategoryFilter = !is_null($currentCategory);
        while(count($unionLinkedItemCollection) < $limit)
        {
            // We still don't have enough recommendations. Fill out the remaining with randoms.
            $numRecsToGet = $limit - count($unionLinkedItemCollection); 
             
            $randCollection = Mage::getResourceModel('catalog/product_collection');
            Mage::getModel('catalog/layer')->prepareProductCollection($randCollection);
            $randCollection->getSelect()->order('rand()');
            $randCollection->addStoreFilter();
            $randCollection->setPage(1, $numRecsToGet);
            $randCollection->addIdFilter(array_merge($unionLinkedItemCollection->getAllIds(), array($product->getId())), true);
            
            if($useCategoryFilter)
            {
                $randCollection->addCategoryFilter($currentCategory);
            }
            
            foreach($randCollection as $linkedProduct)
            {
                $unionLinkedItemCollection->addItem($linkedProduct);
            }
            
            if(!$useCategoryFilter) break; // We tried everything
            
            // Go up a category level for next iteration
            $currentCategory = $currentCategory->getParentCategory();
            if(is_null($currentCategory->getId())) $useCategoryFilter = false;
            
        }
        
        $this->_itemCollection = $unionLinkedItemCollection;
        
        
        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }
}