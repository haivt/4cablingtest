<?php
/**
 * Appmerce - Applications for Ecommerce
 * http://ww.appmerce.com
 *
 * @extension   eWAY Hosted Payment (AU/UK/NZ), XML+CVN (AU), Rapid API
 * @type        Payment method
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Magento Commerce
 * @package     Appmerce_Eway
 * @copyright   Copyright (c) 2011-2013 Appmerce (http://www.appmerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Appmerce_Eway_Model_Api_Rapid extends Appmerce_Eway_Model_Cc
{
    protected $_code = 'eway_rapid';
    protected $_formBlockType = 'eway/form_rapid';

    // Magento features
    protected $_canAuthorize = false;
    protected $_canCapture = true;
    protected $_canRefund = true;
    protected $_canRefundPartial = true;
    protected $_canCreateBillingAgreement = true;
    protected $_canManageBillingAgreements = true;

    // Actions
    const ACTION_PROCESS_PAYMENT = 'ProcessPayment';

    // Response Modes
    const RESPONSE_MODE_REDIRECT = 'Redirect';
    const RESPONSE_MODE_RETURN = 'Return';

    /**
     * Return checkout session
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Return order process instance
     *
     * @return Appmerce_Ogone_Model_Process
     */
    public function getProcess()
    {
        return Mage::getSingleton('eway/process');
    }

    /**
     * Get config action to process initialization
     *
     * @return string
     */
    public function getConfigPaymentAction()
    {
        $paymentAction = $this->getConfigData('payment_action');
        return empty($paymentAction) ? true : $paymentAction;
    }

    /**
     * Check refund availability
     *
     * @return bool
     */
    public function canRefund()
    {
        // UK does not support refunds
        if ($this->getConfigData('subscription') == Appmerce_Eway_Model_Config::COUNTRY_UK) {
            $this->_canRefund = false;
        }
        return $this->_canRefund;
    }

    /**
     * Refund specified amount for payment
     *
     * @param Varien_Object $payment
     * @param float $amount
     * @return Mage_Payment_Model_Abstract
     */
    public function refund(Varien_Object $payment, $amount)
    {
        if (!$this->canRefund()) {
            Mage::throwException(Mage::helper('eway')->__('Refund action is not available.'));
        }

        // Refund through eWAY
        if ($payment->getRefundTransactionId() && $amount > 0) {
            $order = $payment->getOrder();
            if ($order->getId()) {

                // Build gateway URL and $directFields XML message
                $refundFields = $this->getRefundFields($order);
                $xmlMessage = $this->createXmlMessage($refundFields);

                // Send request, receive response
                $type = Appmerce_Eway_Model_Config::TYPE_REFUND;
                $gateway = $this->getGatewayUrl($type);
                $request = $this->curlPostXml($gateway, $xmlMessage);
                $response = new SimpleXMLElement($request);

                // Debug
                if ($this->getConfigData('debug_flag')) {
                    Mage::getModel('eway/api_debug')->setDir('out')->setUrl('')->setData('data', print_r($refundFields, true))->save();
                    Mage::getModel('eway/api_debug')->setDir('in')->setUrl('')->setData('data', $request)->save();
                }

                $transactionId = (string)$response->ewayTrxnNumber;
                $responseCode = substr((string)$response->ewayTrxnError, 0, 2);
                $note = $this->getConfig()->getResponseMessage($responseCode);

                // Switch response
                switch ((string)$response->ewayTrxnStatus) {
                    case Appmerce_Eway_Model_Api::STATUS_FALSE :
                        Mage::throwException(Mage::helper('eway')->__('eWAY refund failed.'));
                        break;

                    case Appmerce_Eway_Model_Api::STATUS_TRUE :
                    default :
                }
            }
            else {
                Mage::throwException(Mage::helper('eway')->__('Invalid order for refund.'));
            }
        }
        else {
            Mage::throwException(Mage::helper('eway')->__('Invalid transaction for refund.'));
        }

        return $this;
    }

    /**
     * Generates array of fields for redirect form
     *
     * @return array
     */
    public function getRefundFields($order)
    {
        if (empty($order)) {
            if (!($order = $this->getOrder())) {
                return array();
            }
        }

        $storeId = $order->getStoreId();
        $paymentMethodCode = $order->getPayment()->getMethod();

        $refundFields = array();
        $refundFields['ewayCustomerID'] = substr($this->getConfigData('customer_id', $storeId), 0, 8);
        $amount = number_format($order->getBaseGrandTotal(), 2, '.', '');
        $refundFields['ewayTotalAmount'] = round($amount * 100);
        $refundFields['ewayCardExpiryMonth'] = substr($order->getPayment()->getCcExpMonth(), 0, 2);
        $refundFields['ewayCardExpiryYear'] = substr($order->getPayment()->getCcExpYear(), 2, 2);
        $refundFields['ewayOriginalTrxnNumber'] = substr($order->getPayment()->getLastTransId(), 0, 16);
        $refundFields['ewayOption1'] = '';
        $refundFields['ewayOption2'] = '';
        $refundFields['ewayOption3'] = '';
        $refundFields['ewayRefundPassword'] = substr($this->getConfigData('refund_password', $storeId), 0, 20);
        $refundFields['ewayCustomerInvoiceRef'] = substr($order->getIncrementId(), 0, 50);

        return $refundFields;
    }

    /**
     * Capture payment
     *
     * @param Varien_Object $payment
     * @param float $amount
     * @return Mage_Payment_Model_Abstract
     */
    public function capture(Varien_Object $payment, $amount)
    {
        if ($amount <= 0) {
            Mage::throwException(Mage::helper('eway')->__('Invalid amount for capture.'));
        }
        $payment->setAmount($amount);
        $order = $payment->getOrder();

        $accessFields = $this->getAccessFields($order);

        // Build SOAP request
        $soapClient = new SoapClient($this->getRapidGatewayUrl('soap'), array(
            'trace' => 1,
            'authentication' => SOAP_AUTHENTICATION_BASIC,
            'login' => $this->getConfigData('api_key'),
            'password' => $this->getConfigData('rapid_password'),
			'proxy_host' => '10.10.100.254',
			'proxy_port' => '9128'
        ));

        // SOAP Response
        try {
            $response = $soapClient->CreateAccessCode(array('request' => $accessFields));
        }
        catch (SoapFault $e) {
            Mage::throwException($e->faultstring);
        }

        // Debug
        if ($this->getConfigData('debug_flag')) {
            Mage::getModel('eway/api_debug')->setDir('out')->setUrl('checkout/onepage')->setData('data', print_r($accessFields, true))->save();
            Mage::getModel('eway/api_debug')->setDir('in')->setUrl('checkout/onepage')->setData('data', print_r($response, true))->save();
        }

        // If we have a valid Access Code, we do a Curl Post for the
        // Card Data we already collected
        $response = $response->CreateAccessCodeResult;
        if (isset($response->AccessCode) && !empty($response->AccessCode)) {
            $accessCode = (string)$response->AccessCode;

            // curlPost the Card Fields
            $cardFields = $this->getCardFields($order, $accessCode);
            $queryString = http_build_query($cardFields, '', '&');
            $this->curlPost($response->FormActionURL, $queryString);
            $this->captureProcess($payment, $response->AccessCode);
        }
        else {
            $note = Mage::helper('eway')->__('Failed to access eWAY. Please contact the merchant.') . ' ' . $this->buildNote($response, (string)$response->Errors);
            Mage::throwException($note);
        }

        $payment->setSkipTransactionCreation(true);
        return $this;
    }

    /**
     * Return Process
     */
    public function captureProcess(Varien_Object $payment, $accessCode)
    {
        if ($accessCode) {
            $resultFields = $this->getResultFields($accessCode);

            // Build SOAP request
            $soapClient = new SoapClient($this->getRapidGatewayUrl('soap'), array(
                'trace' => 1,
                'authentication' => SOAP_AUTHENTICATION_BASIC,
                'login' => $this->getConfigData('api_key'),
                'password' => $this->getConfigData('rapid_password'),
				'proxy_host' => '10.10.100.254',
				'proxy_port' => '9128'
            ));

            // SOAP Response
            try {
                $response = $soapClient->GetAccessCodeResult(array('request' => $resultFields));
            }
            catch (SoapFault $e) {
                Mage::throwException($e->faultstring);
            }

            // Debug
            if ($this->getConfigData('debug_flag')) {
                Mage::getModel('eway/api_debug')->setDir('out')->setUrl('checkout/onepage')->setData('data', print_r($resultFields, true))->save();
                Mage::getModel('eway/api_debug')->setDir('in')->setUrl('checkout/onepage')->setData('data', print_r($response, true))->save();
            }

            // Process the final access response!
            $response = $response->GetAccessCodeResultResult;
            $gatewayTransactionId = (string)$response->TransactionID;

            // Build response note for backend
            $fraud = 0;
            $note = $this->buildNote($response, (string)$response->ResponseCode, $fraud);

            // Switch response
            switch ((string)$response->TransactionStatus) {
                case 1 :
                    $this->_addTransaction($payment, $gatewayTransactionId, Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE, array('is_transaction_closed' => 0), array($this->_realTransactionIdKey => $gatewayTransactionId), $note);
                    break;

                default :
                    Mage::throwException(Mage::helper('eway')->__('Please check your card details and try again.'));
            }

            $payment->setLastTransId($gatewayTransactionId);
            return $this;
        }
    }

    /**
     * Generates array of fields for redirect form
     *
     * @return array
     */
    public function getAccessFields($order)
    {
        if (empty($order)) {
            if (!($order = $this->getOrder())) {
                return array();
            }
        }

        $storeId = $order->getStoreId();
        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();
        if (!$shippingAddress || !is_object($shippingAddress)) {
            $shippingAddress = $billingAddress;
        }
        $paymentMethodCode = $order->getPayment()->getMethod();

        $accessFields = array();

        // Basics
        $accessFields['RedirectUrl'] = $this->getConfig()->getRapidUrl('redirect', $storeId);
        $accessFields['Method'] = self::ACTION_PROCESS_PAYMENT;
        $accessFields['CustomerIP'] = Mage::helper('eway')->getRealIpAddr();

        // Payment
        $accessFields['Payment']['TotalAmount'] = number_format($order->getBaseGrandTotal(), 2, '.', '') * 100;
        $accessFields['Payment']['InvoiceNumber'] = substr($order->getIncrementId(), 0, 64);
        $accessFields['Payment']['InvoiceDescription'] = substr($this->getConfig()->getOrderDescription($order), 0, 64);
        $accessFields['Payment']['InvoiceReference'] = substr($order->getIncrementId(), 0, 64);

        // Customer
        $accessFields['Customer']['Reference'] = substr($order->getCustomerId(), 0, 50);
        $accessFields['Customer']['Title'] = $this->getConfig()->getGenderCode($order->getCustomerGender());
        $accessFields['Customer']['FirstName'] = substr($billingAddress->getFirstname(), 0, 50);
        $accessFields['Customer']['LastName'] = substr($billingAddress->getLastname(), 0, 50);
        $accessFields['Customer']['CompanyName'] = substr($billingAddress->getCompany(), 0, 50);
        $accessFields['Customer']['Street1'] = substr($billingAddress->getStreet(1), 0, 50);
        $accessFields['Customer']['Street2'] = substr($billingAddress->getStreet(2), 0, 50);
        $accessFields['Customer']['City'] = substr($billingAddress->getCity(), 0, 50);
        $accessFields['Customer']['State'] = substr($billingAddress->getState(), 0, 50);
        $accessFields['Customer']['PostalCode'] = substr($billingAddress->getPostcode(), 0, 30);
        $accessFields['Customer']['Country'] = strtolower(substr($billingAddress->getCountry(), 0, 2));
        $accessFields['Customer']['Email'] = substr($billingAddress->getEmail(), 0, 50);

        // Phone & fax cause trouble if they are not valid,...
        // $accessFields['Customer']['Phone'] = substr($billingAddress->getTelephone(), 0, 50);
        // $accessFields['Customer']['Fax'] = substr($billingAddress->getFax(), 0, 50);

        // Line items,...
        // @todo implement line items (+shipping and +-discount)
        // $accessFields['Items']['LineItem']['SKU'] = '';
        // $accessFields['Items']['LineItem']['Description'] = '';
        // $accessFields['Items']['LineItem']['Quantity'] = '';
        // $accessFields['Items']['LineItem']['UnitCost'] = '';
        // $accessFields['Items']['LineItem']['Tax'] = '';
        // $accessFields['Items']['LineItem']['Total'] = '';

        // Shipping address
        // $accessFields['ShippingAddress']['ShippingMethod'] = '';
        $accessFields['ShippingAddress']['FirstName'] = substr($shippingAddress->getFirstname(), 0, 50);
        $accessFields['ShippingAddress']['LastName'] = substr($shippingAddress->getLastname(), 0, 50);
        $accessFields['ShippingAddress']['Street1'] = substr($shippingAddress->getStreet(1), 0, 50);
        $accessFields['ShippingAddress']['Street2'] = substr($shippingAddress->getStreet(2), 0, 50);
        $accessFields['ShippingAddress']['City'] = substr($shippingAddress->getCity(), 0, 50);
        $accessFields['ShippingAddress']['State'] = substr($shippingAddress->getState(), 0, 50);
        $accessFields['ShippingAddress']['PostalCode'] = substr($shippingAddress->getPostcode(), 0, 30);
        $accessFields['ShippingAddress']['Country'] = strtolower(substr($shippingAddress->getCountry(), 0, 2));
        $accessFields['ShippingAddress']['Email'] = substr($shippingAddress->getEmail(), 0, 50);

        // Phone & fax cause trouble if they are not valid,...
        // $accessFields['ShippingAddress']['Fax'] = substr($shippingAddress->getFax(), 0, 50);
        // $accessFields['ShippingAddress']['Phone'] = substr($shippingAddress->getTelephone(), 0, 50);

        // Custom Fields
        // $accessFields['Options']['OptionName']['Value'] = substr('', 0, 255);
        return $accessFields;
    }

    /**
     * Get billing agreement
     */
    public function getBillingAgreementId($order)
    {
        $storeId = $order->getStoreId();
        $customerId = $order->getCustomerId();
        $methodCode = $order->getPayment()->getMethod();

        $db = Mage::getSingleton('core/resource')->getConnection('core_read');
        $tableName = Mage::getSingleton('core/resource')->getTableName('sales_billing_agreement');
        $result = $db->fetchOne('SELECT reference_id FROM ' . $tableName . ' WHERE customer_id = "' . $customerId . '" AND store_id = "' . $storeId . '" AND method_code = "' . $methodCode . '" AND status = "active"');

        return $result;
    }

    /**
     * Generates array of card fields for SOAP
     *
     * @return array
     */
    public function getCardFields($order, $accessCode, $response = false)
    {
        if (empty($order)) {
            if (!($order = $this->getOrder())) {
                return array();
            }
        }

        $cardFields = array();
        $cardFields['EWAY_ACCESSCODE'] = $accessCode;

        // Expiration date mm/yy
        $month = $order->getPayment()->getCcExpMonth();
        $mm = (string)$month < 10 ? '0' . $month : $month;
        $yy = substr((string)$order->getPayment()->getCcExpYear(), 2, 2);

        $cardFields['EWAY_CARDNAME'] = $order->getPayment()->getCcOwner();
        $cardFields['EWAY_CARDNUMBER'] = $order->getPayment()->getCcNumber();
        $cardFields['EWAY_CARDEXPIRYMONTH'] = $mm;
        $cardFields['EWAY_CARDEXPIRYYEAR'] = $yy;
        $cardFields['EWAY_CARDCVN'] = $order->getPayment()->getCcCid();

        return $cardFields;
    }

    /**
     * Get gateway Url
     *
     * @return string
     */
    public function getRapidGatewayUrl($type, $security = false, $mode = false)
    {
        $gateways = $this->getConfig()->getRapidGateways();
        $test = $this->getConfigData('test_flag') ? 'sandbox' : 'live';

        switch ($type) {
            case 'soap' :
            case 'rest' :
                $url = $gateways[$test][$type];
                break;

            case 'http' :
            case 'rpc' :
                $url = $gateways[$test][$type][$security];
                break;

            default :
        }
        return $url;
    }

    /**
     * Generates array of card fields for SOAP
     *
     * @return array
     */
    public function getResultFields($accessCode)
    {
        $resultFields = array();
        $resultFields['AccessCode'] = $accessCode;
        return $resultFields;
    }

    /**
     * Build $note with error message
     */
    public function buildNote($response, $codes, &$fraud = FALSE)
    {
        $note = '';
        $responseCodes = explode(',', $codes);

        // Main response message
        if (isset($response->ResponseMessage)) {
            $note .= Mage::helper('eway')->__('Response Message: %s', (string)$response->ResponseMessage);
        }

        // Error messages
        foreach ($responseCodes as $code) {
            if (substr($code, 0, 1) == 'F') {
                ++$fraud;
            }
            $responseMessage = $this->getConfig()->getResponseMessage($code);
            $note .= '<br /> - ' . Mage::helper('eway')->__('%s (%s).', $responseMessage, $code);
        }

        // Beagle score
        if (isset($response->BeagleScore)) {
            $note .= '<br />' . Mage::helper('eway')->__('Beagle Score: %s', (string)$response->BeagleScore);
        }

        return $note;
    }

    /**
     * Add payment transaction
     *
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param string $transactionId
     * @param string $transactionType
     * @param array $transactionDetails
     * @param array $transactionAdditionalInfo
     * @return null|Mage_Sales_Model_Order_Payment_Transaction
     */
    protected function _addTransaction(Mage_Sales_Model_Order_Payment $payment, $transactionId, $transactionType, array $transactionDetails = array(), array $transactionAdditionalInfo = array(), $message = false)
    {
        $message = $message . '<br />';
        $payment->setTransactionId($transactionId);
        $payment->resetTransactionAdditionalInfo();
        foreach ($transactionDetails as $key => $value) {
            $payment->setData($key, $value);
        }
        foreach ($transactionAdditionalInfo as $key => $value) {
            $payment->setTransactionAdditionalInfo($key, $value);
        }
        $transaction = $payment->addTransaction($transactionType, null, false, $message);
        foreach ($transactionDetails as $key => $value) {
            $payment->unsetData($key);
        }
        $payment->unsLastTransId();

        /**
         * It for self using
         */
        $transaction->setMessage($message);

        return $transaction;
    }

    /**
     * Reset assigned data in payment info model
     *
     * @param Mage_Payment_Model_Info
     * @return Mage_Paygate_Model_Authorizenet
     */
    private function _clearAssignedData($payment)
    {
        $payment->setCcOwner(null)->setCcNumber(null)->setCcCid(null)->setCcExpMonth(null)->setCcExpYear(null)->setCcSsIssue(null)->setCcSsStartMonth(null)->setCcSsStartYear(null);
        return $this;
    }

}
