<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product api
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Ajzele_Mapy_Model_Mapy_Catalog_Product_Api extends Mage_Catalog_Model_Product_Api
{
	
    /**
     * Retrieve list of products with basic info (id, sku, type, set, name)
     *
     * @param null|object|array $filters
     * @param string|int $store
     * @return array
     */
    public function newitems($filters = null, $store = null)
    {
		
		$_rootcatID = Mage::app()->getStore()->getRootCategoryId();
		
		$collection = Mage::getResourceModel('catalog/product_collection')
		->joinField('category_id','catalog/category_product','category_id','product_id=entity_id',null,'left');
		
		if(isset( $filters["category_id"] ))
		{
			$collection->addAttributeToFilter('category_id', array('in' => $filters["category_id"]));
		}
		

		$collection->addAttributeToSelect('*');
		$collection->load();
		
        $result = array();
        foreach ($collection as $product) {

			$p = $this->_getProduct($product->getId(), $store);

            $res = array(
                'product_id' => $product->getId(),
                'sku'        => $product->getSku(),
                'name'       => $product->getName(),
                'image'      => $product->getImage(),
                'url_key'    => $product->getUrlKey(),
                'set'        => $product->getAttributeSetId(),
                'type'       => $product->getTypeId(),
                'category_ids' => $product->getCategoryIds(),
                'website_ids'  => $product->getWebsiteIds(),
				"description"  => $product->getDescription(),
				"price" 	   => $product->getPrice(),
				"tax_class_id" => $product->getTaxClassId(),
            );
			
			/*
			foreach ($p->getTypeInstance(true)->getEditableAttributes($p) as $attribute) {
				if ($this->_isAllowedAttribute($attribute, $attributes)) {
					$res[$attribute->getAttributeCode()] = $product->getData($attribute->getAttributeCode());
				}
			}
			*/
			
			 $result[] = $res;
        }
		
		return $result;
    }
	
    /**
     * Create new product.
     *
     * @param string $type
     * @param int $set
     * @param array $productData
     * @return int
     */
    public function create($type, $set, $sku, $productData)
    {
		$resultText = "";
		
        if (!$type || !$set || !$sku) {
            $this->_fault('data_invalid');
        }

        $product = Mage::getModel('catalog/product');
        /* @var $product Mage_Catalog_Model_Product */
        $product->setStoreId($this->_getStoreId())
            ->setAttributeSetId($set)
            ->setTypeId($type)
            ->setSku($sku);

        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }
		
		if (isset($productData['image']) && $productData['image'] != "")
		{
			$visibility = array (
			'thumbnail',
			'small_image',
			'image'
			);
			
			if(file_exists(Mage::getBaseDir('media') . DS . 'import' . $productData['image']))
			{
				$product->addImageToMediaGallery(Mage::getBaseDir('media') . DS . 'import' . $productData['image'],$visibility,false,false);
			}
			else
			{
				$resultText = " IMAGE ERROR! Image did not exist in file system for this product";
			}
		}

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute)
                && isset($productData[$attribute->getAttributeCode()])) {
                $product->setData(
                    $attribute->getAttributeCode(),
                    $productData[$attribute->getAttributeCode()]
                );
            }
        }

        $this->_prepareDataForSave($product, $productData);

        if (is_array($errors = $product->validate())) {
            $this->_fault('data_invalid', implode("\n", $errors));
			Mage::log($this->_fault('data_invalid', implode("\n", $errors)));
        }

        try {
			$product->setIsMassupdate(true); //added for performace by TK
			$product->setExcludeUrlRewrite(true); //added for performace by TK
            
			$product->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
		
		// update stock
		$this->updateStock($product->getId(), $productData);
		
        return $product->getId() . $resultText;
    }
	
    /**
     * Update product data
     *
     * @param int|string $productId
     * @param array $productData
     * @param string|int $store
     * @return boolean
     */
    public function update($productId, $productData, $store = null, $identifierType = null)
    {
		$resultText = "";
	
        $product = $this->_getProduct($productId, $store, $identifierType);

        if (!$product->getId()) {
            $this->_fault('not_exists');
        }

        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute)
                && isset($productData[$attribute->getAttributeCode()])) {
                $product->setData(
                    $attribute->getAttributeCode(),
                    $productData[$attribute->getAttributeCode()]
                );
            }
        }
				
		if (isset($productData['image']) && $productData['image'] != "")
		{
			$visibility = array (
			'thumbnail',
			'small_image',
			'image'
			);
			
			//First we check if the image exists in the filesystem!
			if(file_exists(Mage::getBaseDir('media') . DS . 'import' . $productData['image']))
			{
				$addImage = true;
				
				foreach ($product->getMediaGalleryImages() as $_image)
				{
					$filepath = explode("/",$_image->getFile());
					
					$file = substr("/".end($filepath),0,-4); //We add a "/" at the beginning for strpos to work properly.
					
					//Since Magento can add the pesky _1 _2 _3 to the end of the files, we hack a bit to get around this.
					//Not so good if the files actually are named something_1.jpg but screw it! Otherwise, we will get a new image for each update!
					
					$newimage = $productData['image'];
					$newimage = substr($newimage,0,-4);
					
					$exists = strpos($file,substr($newimage,1));
					
					if($exists != false)
					{
						$addImage = false;
					}				
				}			
				
				if($addImage)
				{
					$product->addImageToMediaGallery(Mage::getBaseDir('media') . DS . 'import' . $productData['image'],$visibility,false,false);
				}
			}
			else
			{
				$resultText = " IMAGE ERROR! Image did not exist in file system for this product";
			}
			
			//Second we check to make sure that the image is not already assigned to the product.
			
			

		}

        $this->_prepareDataForSave($product, $productData);

        try {
            if (is_array($errors = $product->validate())) {
                $this->_fault('data_invalid', implode("\n", $errors));
				Mage::log($this->_fault('data_invalid', implode("\n", $errors)));
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
			Mage::log($this->_fault('data_invalid', implode("\n", $errors)));
        }

        try {
            $product->save();
			$result = true;
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
			Mage::log($this->_fault('data_invalid', implode("\n", $errors)));
			$result = false;
        }
		
		// update stock
		$this->updateStock($product->getId(), $productData);

        return $result . $resultText;
    }
	
	private function updateStock($productId, $data)
    {
        $product = Mage::getModel('catalog/product');

        if ($newId = $product->getIdBySku($productId)) {
            $productId = $newId;
        }

        $product->setStoreId($this->_getStoreId())
            ->load($productId);

        if (!$product->getId()) {
            $this->_fault('not_exists');
        }

        if (!$stockData = $product->getStockData()) {
            $stockData = array();
        }

        if (isset($data['qty'])) {
            $stockData['qty'] = $data['qty'];
        }

        if (isset($data['is_in_stock'])) {
            $stockData['is_in_stock'] = $data['is_in_stock'];
        }

        if (isset($data['manage_stock'])) {
            $stockData['manage_stock'] = $data['manage_stock'];
        }

        if (isset($data['use_config_manage_stock'])) {
            $stockData['use_config_manage_stock'] = $data['use_config_manage_stock'];
        }

        $product->setStockData($stockData);

        try {
            $product->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('not_updated', $e->getMessage());
        }

        return true;
    }

	public function updateProductStocks($stockdata)
	{
		//We get the products from stockdata
		//return $stockdata;
		
		foreach($stockdata as $st)
		{
		 // Check if SKU exists
		
			try
			{
			$product = Mage::getModel('catalog/product')->loadByAttribute('sku',$st['sku']); //$st['sku'] 
			 
				if ($product) 
					{
						 $productId = $product->getId();
						 $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
						 $stockItemId = $stockItem->getId();
						 $stock = array();
						 
						 if (!$stockItemId) {
						 $stockItem->setData('product_id', $product->getId());
						 $stockItem->setData('stock_id', 1); 
						 } else {
						 $stock = $stockItem->getData();
						 }
						 
						 $stockItem->setData('qty', $st['qty']); //$dataline['qty']
						 $stockItem->setData('is_in_stock',$st['is_in_stock']); //$dataline['is_in_stock']
						 
								 
						 $stockItem->save();
						 
						 unset($stockItem);
						 unset($product);
						 
						 $result .= "<br />Stock updated: " . $st['sku'];
					}
				else
				$result .= "<br />Product SKU not found: " . $st['sku'];
					
			}
				 
			catch(exception $e)
			{
				$result .= $e;
			}
		}
		
		return $result;
	}
	
	public function massCreateProducts($products)
	{
		//We get the products products
		//return $result;
		$result = array();
		
		foreach($products as $prod)
		{
		 // Check if SKU exists
		
			try
			{
				$prodid = $this->create($prod['type'], $prod['set_id'], $prod['sku'], $prod['productData']);
				$resulttext = "<br />" . date("H:i:s") . " Product sku: " . $prod['sku'] . " created in magento with id: " . $prodid;	
				
				$result[] = array('sku' => $prod['sku'],
								'insertresult' => 'OK',
								'resulttext' => $resulttext,
								'productData' => $prod['productData']
								);
			}
				 
			catch(exception $e)
			{
				$resulttext = "<p>Error creating product: " . $prod['sku'] . "</p>Error:<br/>" . $e;
				$result[] = array('sku' => $prod['sku'],
								'insertresult' => 'ERROR',
								'resulttext' => $resulttext,
								'productData' => $prod['productData']
								);
			}
		}
		
		return $result;
	}
	
	
	//This is used when we need to force updates of all data (this also creates products if they don't exist...)
	public function massforceupdates($products)
	{
		//We get the products products
		//return $result;
		$result = array();
		
		foreach($products as $prod)
		{
			
			// Check if SKU exists
		    $product = $this->_getProduct($prod['sku'], $store, $identifierType);

			if (!$product->getId()) 
			{
				//The product doesn't exist, we try to create it!
				try
				{
					$prodid = $this->create($prod['type'], $prod['set_id'], $prod['sku'], $prod['productData']);
					$resulttext = "<br />" . date("H:i:s") . " Product sku: " . $prod['sku'] . " created in magento with id: " . $prodid;	
					
					$result[] = array('sku' => $prod['sku'],
									'insertresult' => 'OK',
									'resulttext' => $resulttext,
									'method' => 'INSERT',
									'productData' => $prod['productData']
									);
				}
				 
				catch(exception $e)
				{
					$resulttext = "<p>Error creating product: " . $prod['sku'] . "</p>Error:<br/>" . $e;
					$result[] = array('sku' => $prod['sku'],
									'insertresult' => 'ERROR',
									'resulttext' => $resulttext,
									'productData' => $prod['productData']
									);
				}
			}
			else
			{
				//The product exists, we try to force update it!
				
				try
				{
					$updated = $this->update($prod['sku'], $prod['productData']);
					$resulttext = "<br />" . date("H:i:s") . " UPDATE for product sku: " . $prod['sku'] . ", result: " . $updated;	
					
					$result[] = array('sku' => $prod['sku'],
									'insertresult' => 'OK',
									'resulttext' => $resulttext,
									'method' => 'UPDATE',
									'productData' => $prod['productData']
									);
				}
				 
				catch(exception $e)
				{
					$resulttext = "<p>Error updating product: " . $prod['sku'] . "</p>Error:<br/>" . $e;
					$result[] = array('sku' => $prod['sku'],
									'insertresult' => 'ERROR',
									'resulttext' => $resulttext,
									'productData' => $prod['productData']
									);
				}
			}
		
		}
		
		return $result;
	}
	
}
