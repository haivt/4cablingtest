<?php
/**
 * Customer groups api
 *
 * @author     Micky Socaci
 */

class Ajzele_Mapy_Model_Mapy_Customer_Group_Api extends Mage_Customer_Model_Group_Api
{
    /**
     * Retrieve groups
     *
     * @return array
     */
    public function getCollection()
    {
        $collection = Mage::getModel('customer/group')->getCollection();

        $result = array();
        foreach ($collection as $group) {
            $result[] = $group->toArray();
        }
        return $result;
    }
	
    /**
     * Update customer group data
     *
     * @param int $groupId
     * @param array $groupData
     * @return boolean
     */
    public function update($groupId, $groupData)
    {
        $group = Mage::getModel('customer/group')->load($groupId);
        if (!$group->getId()) {
            $this->_fault('not_exists');
        }
		foreach($groupData as $key => $value )
		{
			$group->setData($key, $value);
		}
        $group->save();
        return true;
    }
	
	
	
	/**
     * Create new customer group
     *
     * @param array $groupData
     * @return int
     */
    public function create($groupData)
    {
		if(!$this->groupExists($groupData['customer_group_code']))
		{
			try {
				$group = Mage::getModel('customer/group')->setData($groupData)->save();
			} catch (Mage_Core_Exception $e) {
				$this->_fault('data_invalid', $e->getMessage());
			}
			return $group->getId();
		}
		else
		{
			$this->_fault("exists");
			return 0;
		}
    }

	/**
     * Group exists?
     *
     * @param array $customer_group_code
     * @return array
     */
	 
	public function groupExists($customer_group_code)
	{
        $collection = Mage::getModel('customer/group')->getCollection();
		$collection->addFieldToFilter('customer_group_code', array('in' => $customer_group_code));
		$data = $collection->toArray();
		if( $data['totalRecords'] == 0 )
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/**
     * Delete customer group 
     *
     * @param int $groupId
     * @return bool
     */
	public function delete($groupId)
	{
		$g = Mage::getModel('customer/group')->load($groupId);
		try {
			//bypass "Cannot complete this operation from non-admin area."
			Mage::register('isSecureArea', true);
			$g->delete();
			Mage::unregister('isSecureArea');
			return true;
		}
		catch (Exception $e) {
			return false;
		}
	}
	
}