<?
// Ajzele_Mapy_Model_Mapy_Customer_Api 

class Ajzele_Mapy_Model_Mapy_Customer_Api extends Mage_Customer_Model_Customer_Api // Mage_Customer_Model_Customer
{

    /**
     * Create new customer
     *
     * @param array $customerData
     * @return int
     */
    public function create($customerData)
    {
        $customerData = $this->_prepareData($customerData);
        try {
            $customer = Mage::getModel('customer/customer')
                ->setData($customerData)
                ->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
		
		
		if(Mage::getSingleton("Igence_Uie_Helper_Data")->getKey("ic_send_new_customer_email") == "true")
		{
			$sent = $customer->sendNewAccountEmail('registered', '', $customerData['store_id']); //$customer->getSendemailStoreId());
		}
		
		// return array("id" => $customer->getId(), "ic_send_new_customer_email" => Mage::getSingleton("Igence_Uie_Helper_Data")->getKey("ic_send_new_customer_email"), "sent" => $sent );
        return $customer->getId();
    }
	
	/**
     * Get customer id by email address
     *
     * @param string $customer_email
     * @param int $website_id
     * @return int
     */

	public function idByEmail($customer_email, $website_id = 1)
	{
		$customer = Mage::getModel('customer/customer')->setWebsiteId( (int) $website_id );
		$customer = $customer->loadByEmail($customer_email);
		if(!$customer->getId()) {
			return 0;
		}
		return $customer->getId();
	}

    /**
     * Update customer data
     *
     * @param int $customerId
     * @param array $customerData
     * @return boolean
     */
	 
    public function update($customerId, $customerData)
    {
		$customerData = $this->_prepareData($customerData);
	
        $customer = Mage::getModel('customer/customer')->load($customerId);
		
        if (!$customer->getId()) {
            $this->_fault('not_exists');
            return false;
        }
		
		$old_email = $customer->getEmail();

        foreach ($this->getAllowedAttributes($customer) as $attributeCode=>$attribute) {
            if (isset($customerData[$attributeCode])) {
                $customer->setData($attributeCode, $customerData[$attributeCode]);
            }
        }
		
        if(isset($customerData['password']))
		{
            $customer->setPassword($customerData['password']);
		}
		
		if(
			Mage::getSingleton("Igence_Uie_Helper_Data")->getKey("ic_send_new_customer_email") == "true"
			&& ( isset($customerData['email']) && $old_email != $customerData['email'])
		)
		{
			$sent = $customer->sendNewAccountEmail('registered', '', $customer->getStoreId() );  //$customer->getSendemailStoreId());
		}
		
        $customer->save();
        return true;
    }

}
?>