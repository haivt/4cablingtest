<?php
/**
 * @category    Ajzele
 * @package     Ajzele_Mapy
 * @copyright   Copyright (c) Branko Ajzele (http://activecodeline.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog category api
 *
 * @category   Ajzele
 * @package    Ajzele_Mapy
 * @author     Branko Ajzele <ajzele@gmail.com>
 */
class Ajzele_Mapy_Model_Catalog_Category_Api extends Mage_Catalog_Model_Category_Api
{
    /**
     * Retrieve category ids with images
     *
     * @param string|int $store
     * @return array
     */
	 
    public function treeWithDetails($parentId = null, $store = null)
    {
        if (is_null($parentId) && !is_null($store)) {
            $parentId = Mage::app()->getStore($this->_getStoreId($store))->getRootCategoryId();
        } elseif (is_null($parentId)) {
            $parentId = 1;
        }

        /* @var $tree Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Tree */
        $tree = Mage::getResourceSingleton('catalog/category_tree')
            ->load();

        $root = $tree->getNodeById($parentId);

        if($root && $root->getId() == 1) {
            $root->setName(Mage::helper('catalog')->__('Root'));
        }

        $collection = Mage::getModel('catalog/category')->getCollection()
            ->setStoreId($this->_getStoreId($store))
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('image')
			->addAttributeToSelect('url_key')
            ->addAttributeToSelect('is_active');

        $tree->addCollectionData($collection, true);

        return $this->_nodeWithMoreDetailsToArray($root);
    }
	 

    /**
     * Convert node to array
     *
     * @param Varien_Data_Tree_Node $node
     * @return array
     */
    protected function _nodeWithMoreDetailsToArray(Varien_Data_Tree_Node $node)
    {
        // Only basic category data
        $result = array();
        $result['category_id'] = $node->getId();
        $result['parent_id']   = $node->getParentId();
        $result['name']        = $node->getName();
        $result['image']       = $node->getImage();
        $result['url_key']     = $node->getUrlKey();		
        $result['is_active']   = $node->getIsActive();
        $result['position']    = $node->getPosition();
        $result['level']       = $node->getLevel();
        $result['children']    = array();

        foreach ($node->getChildren() as $child) {
            $result['children'][] = $this->_nodeWithMoreDetailsToArray($child);
        }

        return $result;
    }


    /**
     * Retrieve category ids with erp internal ids
     *
     * @param string|int $store
     * @return array
     */
    public function categoryWithErp($store = null)
    {
        $collection = Mage::getModel('catalog/category')->getCollection()
            ->setStoreId($this->_getStoreId($store))
            ->addAttributeToSelect('name')
			->addAttributeToSelect('erp_internal_id')
            ->addAttributeToSelect('is_active')
            ->addAttributeToSelect('is_anchor')
            ->addAttributeToSelect('url_key')
            ->addAttributeToSelect('description')
            ->addAttributeToSelect('meta_title')
            ->addAttributeToSelect('meta_description')
            ->addAttributeToSelect('display_mode')
            ->addAttributeToSelect('landing_page')
            ->addAttributeToSelect('available_sort_by')
            ->addAttributeToSelect('default_sort_by');

		
        $result = array();
        foreach ($collection as $category) 
		{
            /* @var $category Mage_Catalog_Model_Category */
			if($category->getParentId() != 0)
			{
				$erp = $category->getDataUsingMethod('erp_internal_id');
				if($erp !== null)
				{
					$result[$erp] = array(
						'category_id' => $category->getId(),
						'parent_id'   => $category->getParentId(),
						'name'        => $category->getName(),
						'is_active'   => $category->getIsActive(),
						'is_anchor'	  => $category->getIsAnchor(),
						'position'    => $category->getPosition(),
						'level'       => $category->getLevel(),
						'url_key'	  => $category->getUrlKey(),
						'description'	 	=> $category->getDescription(),
						'image'	 			=> $category->getImage(),
						'meta_title'	 	=> $category->getMetaTitle(),
						'meta_description'	=> $category->getMetaDescription(),
						'path_in_store'	  	=> $category->getPathInStore(),
						'display_mode'	  	=> $category->getDisplayMode(),
						'landing_page'	  	=> $category->getLandingPage(),
						'available_sort_by'	=> implode(",",$category->getAvailableSortBy()),
						'default_sort_by'	=> $category->getDefaultSortBy(),
						'custom_design'	  	=> $category->getCustomDesign(),
						'custom_design_apply'   => $category->getCustomDesignApply(),
						'custom_design_from' 	=> $category->getCustomDesignFrom(),
						'custom_design_to'	 	=> $category->getCustomDesignTo(),
						'page_layout'	  		=> $category->getPageLayout(),
						'custom_layout_update'	=> $category->getCustomLayoutUpdate(),
						
						'erp_internal_id' => $erp
					);
				}
			}
        }
        return $result;
    }

}