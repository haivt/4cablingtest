<?php
/**
 * @category    Ajzele
 * @package     Ajzele_Mapy
 * @copyright   Copyright (c) Branko Ajzele (http://activecodeline.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product attribute api
 *
 * @category   Ajzele
 * @package    Ajzele_Mapy
 * @author     Branko Ajzele <ajzele@gmail.com>
 */
class Ajzele_Mapy_Model_Catalog_Product_Attribute_Api extends Mage_Catalog_Model_Product_Attribute_Api
{
	public function addManufacturer($manufacturer)
	{
		$result = "Starting insert";
		$addManufacturer = true;
		
		$attributeId = Mage::getResourceModel('eav/entity_attribute')
    					->getIdByCode('catalog_product','manufacturer');
		
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
		$attributeOptions = $attribute->getSource()->getAllOptions();
		
		/*$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setCodeFilter('manufacturer')
                        ->getFirstItem();
        */
		
		//return $attributeInfo;
		
		//$attributeOptions = $attributeInfo->getSource()->getAllOptions(false);
		foreach($attributeOptions as $arr)
		{
			if(in_array($manufacturer, $arr))
			{
				$addManufacturer = false;
				break;
			}
		}
		
		if($addManufacturer == true)
		{
			//add new manufacturer
			$option['attribute_id'] = $attributeId; //manufacturer
			$option['value']['any_key_that_resolves_to_zero'][0] = $manufacturer;

			$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
			$setup->addAttributeOption($option);
			
			$result = "<br/>OK, added: " . $manufacturer;
		}
		else
		{
			$result = "<br/>Not added, already exists: " . $manufacturer;
		}
		
		return $result;
		
	}
	
	public function massAddManufacturer($manufacturers)
	{
		$result = "";
		
		foreach ($manufacturers as $m)
		{
			$result .= $this->addManufacturer($m);
		}
		
		return $result;
	}

}