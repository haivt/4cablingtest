<?php

class Ewave_Temando_Model_Observer
{
    
    /**
     * Handles sales_order_place_after.
     */
    public function createTemandoShipment(Varien_Event_Observer $observer)
    {
        $order = $observer->getOrder();
        /* @var $order Mage_Sales_Model_Order */

        $__t = explode('_', $order->getShippingMethod());
        if ($__t[0] != 'temando') {
            return;
        }

        if (!Mage::helper('temando')->getConfigData('options/show_name_time')) {
            $order->setShippingDescription(Mage::helper('temando')->getConfigData('options/shown_name'))->save();
        }

        $selected_quote_id = preg_replace('#^[^_]*_#', '', $order->getShippingMethod());
        $selected_options = preg_replace('#^([^_]*_){2}#', '', $order->getShippingMethod());
        
        $selected_quote = Mage::getModel('temando/quote')
            ->load($selected_quote_id);
        /* @var $selected_quote Ewave_Temando_Model_Quote */
        
        $temando_shipment = Mage::getModel('temando/shipment');
        /* @var $temando_shipment Ewave_Temando_Model_Shipment */
        
        switch ($selected_quote->getCarrier()->getCarrierId()) {
            case Ewave_Temando_Model_Carrier::FLAT_RATE:
            case Ewave_Temando_Model_Carrier::FREE:
                // save cheapest quote as admin selected quote
                $cheapest_quote = $this->loadQuotes($order, Mage::helper('temando')->getOrigin());
                $temando_shipment
                    ->setAdminSelectedQuoteId($cheapest_quote->getId())
                    ->setAnticipatedCost($cheapest_quote->getTotalPrice());
                break;
            default:
                // save which quote was selected by customer
                $temando_shipment
                    ->setCustomerSelectedQuoteId($selected_quote->getId())
                    ->setCustomerSelectedOptions($selected_options);
                
                $selected_quote = $temando_shipment->getSelectedQuotePermutation();
                
                $temando_shipment
                    ->setCustomerSelectedQuoteDescription($selected_quote->getDescription(true/*Mage::helper('temando')->getConfigData('options/show_carrier_names')*/))
                    ->setAdminSelectedQuoteId($selected_quote->getId())
                    ->setAnticipatedCost($selected_quote->getTotalPrice());
                if ($selected_options == 'temando_10000') {
                    $temando_shipment->setCustomerSelectedQuoteDescription('Flat Rate / Free Shipping');
                }

                break;
        }

        $email = $order->getShippingAddress()->getEmail();
        if(!$email) {
            $email = $order->getCustomerEmail();
        }

        $temando_shipment
            ->setOrderId($order->getId())
            ->setStatus(Ewave_Temando_Model_System_Config_Source_Shipment_Status::PENDING)
            ->setDestinationContactName($order->getShippingAddress()->getName())
            ->setDestinationCompanyName($order->getShippingAddress()->getCompany())
            ->setDestinationStreet(trim(join(' ', $order->getShippingAddress()->getStreet())))
            ->setDestinationRegion($order->getShippingAddress()->getRegionCode())
            ->setDestinationPhone($order->getShippingAddress()->getTelephone())
            ->setDestinationEmail($email)
            ->setDestinationCountry($order->getShippingAddress()->getCountryId())
            ->setDestinationPostcode($order->getShippingAddress()->getPostcode())
            ->setDestinationCity($order->getShippingAddress()->getCity())
            ->setReadyTime('AM')
//            ->setReadyDate(date('Y-m-d', Mage::helper('temando')->getReadyDate()))
            ->save();
            
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            if ($item->getFreeShipping()) {
                $has_free = true;
                continue;
            }

            /* @var $order Mage_Sales_Model_Order */
            
            $product = Mage::getModel('catalog/product')
                ->load($item->getProductId());
            /* @var $product Mage_Catalog_Model_Product */

            if ($product->isVirtual()) {
                continue;
            }

            $box = Mage::getModel('temando/box');
            /* @var $box Ewave_Temando_Model_Box */
            Mage::helper('temando')->applyTemandoParamsToProductByItem($item, $product);
            $box
                ->setShipmentId($temando_shipment->getId())
                ->setComment($product->getName())
                ->setQty($item->getQty())
                ->setValue($item->getRowTotal())
                ->setLength($product->getTemandoLength())
                ->setWidth($product->getTemandoWidth())
                ->setHeight($product->getTemandoHeight())
                ->setMeasureUnit(Mage::helper('temando')->getConfigData('units/measure'))
                ->setWeight($product->getWeight())
                ->setWeightUnit(Mage::helper('temando')->getConfigData('units/weight'))
                ->setPackaging($product->getTemandoPackaging())
                ->setFragile($product->getTemandoFragile())
                ->save();
        }
    }
    
    protected function loadQuotes($order, $origin)
    {
        // Load quotes
        $allowed_carriers = explode(',', Mage::getStoreConfig('carriers/temando/allowed_methods'));
        
        $request = Mage::getModel('temando/api_request');
        /* @var $request Ewave_Temando_Model_Api_Request */
        $request
            ->setUsername(Mage::helper('temando')->getConfigData('general/username'))
            ->setPassword(Mage::helper('temando')->getConfigData('general/password'))
            ->setSandbox(Mage::helper('temando')->getConfigData('general/sandbox'))
            ->setOrigin(
                $origin->getCountry(),
                $origin->getPostcode(),
                $origin->getCity(),
                $origin->getType())
            ->setDestination(
                $order->getDestCountryId(),
                $order->getDestPostcode(),
                $order->getDestCity(),
                $order->getDestStreet())
            ->setItems($order->getAllItems())
            ->setReady()
            ->setAllowedCarriers($allowed_carriers);
        
        // reset gets the first element of the returned array
        return reset($request->getCheapestQuotes());
    }

    public function hookCartSaveAddress($observer)
    {
        $post = $observer->getControllerAction()->getRequest()->getPost();
        if (Mage::getStoreConfig('carriers/temando/active') && isset($post['country_id']) && ('AU' == $post['country_id']) && isset($post['region_id']) && isset($post['estimate_city']) && isset($post['estimate_postcode']) && isset($post['pcs'])) {
            $data = array(
                'country_id' => $post['country_id'],
                'region_id' => $post['region_id'],
                'city' => $post['estimate_city'],
                'postcode' => $post['estimate_postcode'],
                'pcs' => $post['pcs'],
            );
            Mage::getSingleton('customer/session')->setData('estimate_product_shipping', new Varien_Object($data));
        }
    }
    
}
