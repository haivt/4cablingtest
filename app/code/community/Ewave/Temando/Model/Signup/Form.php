<?php

/**
 * @method string getUsername()
 * @method string getPassword()
 * @method string getPasswordConfirm()
 * @method string getClientType()
 * @method string getCompanyName()
 * @method string getCompanyContactPerson()
 * @method string getFirstname()
 * @method string getSurname()
 * @method string getDob()
 * @method string getPhone1()
 * @method string getPhone2()
 * @method string getEmail()
 * @method string getStreetAddress()
 * @method string getStreetSuburb()
 * @method string getStreetCity()
 * @method string getStreetState()
 * @method string getStreetPostcode()
 * @method string getStreetCountry()
 * @method string getPostalAddress()
 * @method string getPostalSuburb()
 * @method string getPostalCity()
 * @method string getPostalState()
 * @method string getPostalPostcode()
 * @method string getPostalCountry()
 *
 */
class Ewave_Temando_Model_Signup_Form extends Mage_Core_Model_Abstract
{

    const AFFILATE_USERNAME = 'EWAVE2';
    const AFFILATE_PASSWORD = 'ewave1476';
    const AFFILATE_PROMO =  'A0199';

    public function submit()
    {
        /*if (Mage::helper('temando')->getConfigData('general/username')) {
            $password = $this->getPassword();
            $password_confirm = $this->getPasswordConfirm();
            $temando_data = $this->getClient();
            if ($temando_data) {
                $this->setData($temando_data);
            }

            $this->setPassword($password);
            $this->setPasswordConfirm($password_confirm);
            $this->setUsername(Mage::helper('temando')->getConfigData('general/username'));
        }*/
        $request = $this->_toRequestArray();
        $api = Mage::getModel('temando/api_client');
        /* @var $api Ewave_Temando_Model_Api_Client */

        $temando_data = false;
        $username = self::AFFILATE_USERNAME;
        $password = self::AFFILATE_PASSWORD;
        $sandbox = false;
        $api->connect($username, $password, $sandbox);

        try {
//            echo "<pre>"; print_r($request);
            /*if (Mage::helper('temando')->getConfigData('general/username')) {

                if (!$temando_data) {
                    throw new Exception('Cannot get client information for existing client');
                }

                $request['client']['id'] = $temando_data['client_id'];
                unset($request['loginDetails']);
                unset($request['promotionCode']);
                $result = $api->updateClient($request);
            } else {*/
                $result = $api->createClient($request);
            /*}*/

            Mage::getModel('core/config')->saveConfig('temando/general/username', $this->getUsername(), 'default', 0);
            Mage::getModel('core/config')->saveConfig('temando/general/password', $this->getPassword(), 'default', 0);
            Mage::getModel('core/config')->saveConfig('temando/general/sandbox', 0, 'default', 0);
//            echo "<pre>"; print_r($result); exit;
        } catch (SoapFault $fault) {
//            echo print_r($fault->getMessage()); exit;
            switch ($fault->faultcode) {
                case 'Err_Format18':
                    $message = Mage::helper('temando')->__('Sorry, the username you have chosen is already in use. Please try a different username.');
                    throw Mage::getModel('temando/signup_form_exception')->addError($message);
                
                case 'Err_Format19':
                    $message = Mage::helper('temando')->__('The "Date of Birth" field represents a date of birth that is after the current date.');
                    throw Mage::getModel('temando/signup_form_exception')->addError($message);
                    
                case 'Err_Format':
                    $patterns = array(
                        '# \(within the \'.*\' type\)#',
                        "#'postalCountry'#",
                        "#'postalState'#",
                        "#'postalCode'#",
                        "#'streetCountry'#",
                        "#'streetState'#",
                        "#'streetCode'#",
                    );
                    
                    $replacements = array(
                        '',
                        'Postal Address "Country"',
                        'Postal Address "State"',
                        'Postal Address "Postcode"',
                        'Street Address "Country"',
                        'Street Address "State"',
                        'Street Address "Postcode"',
                    );
                    $message = preg_replace($patterns, $replacements, $fault->faultstring);
                    throw Mage::getModel('temando/signup_form_exception')->addError($message);
                    
                default:
                    throw $fault;
            }
        }
        
        return $result;
    }
    
    protected function _validate()
    {
        $errors = array();
        
        if (!Zend_Validate::is($this->getUsername(), 'NotEmpty')) {
            $errors[] = '"Login ID" is a required field.';
        } else if (false && !Zend_Validate::is($this->getUsername(), 'StringLength', array('max' => 50))) {
            $errors[] = 'Please use less than 50 characters in the "Login ID" field.'.$this->getUsername();
        }
        
        if (!Zend_Validate::is($this->getPassword(), 'NotEmpty')) {
            $errors[] = '"Password" is a required field.';
        } else if (!Zend_Validate::is($this->getPassword(), 'StringLength', array('min' => 6, 'max' => 20))) {
            $errors[] = 'Please choose a password between six and 20 characters in length.';
        } else if ($this->getPassword() !== $this->getPasswordConfirm()) {
            $errors[] = 'Please make sure your passwords match.';
        }

        /*if (Mage::helper('temando')->getConfigData('general/username')) {
            return true;
        }*/
        switch ($this->getClientType()) {
            case Ewave_Temando_Model_System_Config_Source_Client_Type::COMPANY:
                if (!Zend_Validate::is($this->getCompanyNo(), 'NotEmpty')) {
                    $errors[] = '"Company No." is a required field.';
                }
                break;
            
            case Ewave_Temando_Model_System_Config_Source_Client_Type::INDIVIDUAL:
                if (Zend_Validate::is($this->getDob(), 'NotEmpty')){
                    // date of birth isn't empty, check it's valid
                    if (!Zend_Validate::is($this->getDob(), 'Date', array('format' => 'yyyy-MM-dd'))) {
                        $errors[] = 'Please enter a valid date in the "Date of Birth" field.';
                    } else if (time() - strtotime($this->getDob()) < 86400) {
                        $errors[] = 'The "Date of Birth" field represents a date of birth that is after the current date.';
                    }
                }
                break;
            
            default:
                $errors[] = 'Please select an option for "Client Type".';
                break;
        }
        
        
        if ($errors) {
            $ex = Mage::getModel('temando/signup_form_exception');
            /* @var $ex Ewave_Temando_Model_Signup_Form_Exception */
            foreach ($errors as $message) {
                $ex->addError($message);
            }
            throw $ex;
        }
        
        return true;
    }
    
    protected function _toRequestArray()
    {
        $this->_validate();
        
        $request = array(
            'loginDetails' => array(
                'loginId' => $this->getUsername(),
            	'password' => $this->getPassword(),
            ),
            'client' => array(
                'individualCompany' => $this->getClientType(),
                'streetCountry' => 'AU',
                'phone1' => $this->getPhone1(),
                'email' => $this->getEmail(),
            ),
            'promotionCode' => self::AFFILATE_PROMO,
        );

        if ($this->getPhone2()) {
            $request['client']['phone2'] = $this->getPhone2();
        }
        if ($this->getStreetAddress()) {
            $request['client']['streetAddress'] = $this->getStreetAddress();
        }
        if ($this->getStreetSuburb()) {
            $request['client']['streetSuburb'] = $request['client']['streetCity'] = $this->getStreetSuburb();
        }

        if ($this->getStreetState()) {
            $request['client']['streetState'] = $this->getStreetState();
        }
        if ($this->getStreetPostcode()) {
            $request['client']['streetCode'] = $this->getStreetPostcode();
        }
        if ($this->getPostalAddress()) {
            $request['client']['postalAddress'] = $this->getPostalAddress();
        }
        if ($this->getPostalSuburb()) {
            $request['client']['postalSuburb'] = $request['client']['postalCity'] = $this->getPostalSuburb();
        }

        if ($this->getPostalState()) {
            $request['client']['postalState'] = $this->getPostalState();
        }
        if ($this->getPostalPostcode()) {
            $request['client']['postalCode'] = $this->getPostalPostcode();
        }
        
        $add_country =
            $this->getPostalAddress() ||
            $this->getPostalSuburb() ||
            $this->getPostalCity() ||
            $this->getPostalState() ||
            $this->getPostalPostcode();
        
        if ($add_country) {
            $request['client']['postalCountry'] = 'AU';
        }
        
                    
        
        switch ($this->getClientType()) {
            case Ewave_Temando_Model_System_Config_Source_Client_Type::COMPANY:
                $request['client']['companyName'] = $this->getCompanyName();
                $request['client']['companyContactPerson'] = $this->getCompanyContactPerson();
                $request['client']['companyNo'] = $this->getCompanyNo();
                break;
                
            case Ewave_Temando_Model_System_Config_Source_Client_Type::INDIVIDUAL:
                $request['client']['individualSurname'] = $this->getSurname();
                $request['client']['individualFirstname'] = $this->getFirstname();
                if ($this->getDob()) {
                    $request['client']['individualDateOfBirth'] = $this->getDob();
                }
                break;
        }
        
        return $request;
    }

    public function getClient(/*$login, $pass*/)
    {
//        return null;
        $username = self::AFFILATE_USERNAME;
        $password = self::AFFILATE_PASSWORD;
        $sandbox = false;
        $api = Mage::getModel('temando/api_client');
        /* @var $api Ewave_Temando_Model_Api_Client */
        $api->connect($username, $password, $sandbox);
        $login = Mage::helper('temando')->getConfigData('general/username');
        $pass = Mage::helper('temando')->getConfigData('general/password');
        if (!$login || !$pass) {
            return null;
        }

        try {
            $result = $api->getClient(
                array(
//                     'clientId' => 13454,
                     'loginDetails' => array(
                         'loginId'  => $login,
                         'password' => $pass
                     ),
                )
            );
            $fields = array(
                'id' => 'client_id',
                'individualCompany' => 'client_type',
                'individualSurname' => 'surname',
                'individualFirstname' => 'firstname',
                'streetAddress' => 'street_address',
                'streetSuburb' => 'street_suburb',
//                'streetCity' => '',
                'streetState' => 'street_state',
                'streetCode' => 'street_postcode',
//                'streetCountry' => AU,
                'postalAddress' => 'postal_address',
                'postalSuburb' => 'postal_suburb',
//                'postalCity' => 'postal_address',
                'postalState' => 'postal_state',
                'postalCode' => 'postal_postcode',
//                'postalCountry' => AU,
                'phone1' => 'phone1',
                'phone2' => 'phone2',
                'email' => 'email',
                'companyName' => 'company_name',
                'companyContactPerson' => 'company_contact_person',
                'companyNo' => 'company_no',
                'individualDateOfBirth' => 'dob'
            );
            $return = array(
                'username' => $login,
                'password' => $pass,
                'password_confirm' => $pass,
            );
            if ($result->client) {
                foreach ($result->client as $k => $v) {
                    if (isset($fields[$k])) {
                        $return[$fields[$k]] = (string)$v;
                    }
                }
            }

            return $return;
        } catch (Exception $e) {
            return null;
        }
    }
    
}
