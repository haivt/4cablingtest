<?php

class Ewave_Temando_Model_Signup_Form_Exception extends Exception
{
    
    protected $_errors;
    
    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct();
    }
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    public function addError($message)
    {
        if (!is_array($this->_errors)) {
            $this->_errors = array();
        }
        $this->_errors[] = $message;
        
        return $this;
    }
    
}
