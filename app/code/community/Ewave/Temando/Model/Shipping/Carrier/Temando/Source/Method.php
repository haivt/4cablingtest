<?php

class Ewave_Temando_Model_Shipping_Carrier_Temando_Source_Method
{
    
    public function toOptionArray()
    {
        $options = array();
        
        $carriers = Mage::getModel('temando/carrier')->getCollection();
        foreach ($carriers as $carrier) {
            $options[] = array('value' => $carrier->getCarrierId(), 'label' => $carrier->getCompanyName());
        }
        
        return $options;
    }


    /**
     * Gets all the options in the key => value type array.
     *
     * @return array
     */
    public function getOptions($please_select = false)
    {
        if ($please_select) {
            $options = array(null => '--Please Select--');
        }

        $carriers = Mage::getModel('temando/carrier')->getCollection();
        foreach ($carriers as $carrier) {
            $options[$carrier->getCarrierId()] = $carrier->getCompanyName();
        }

        return $options;
    }
}
