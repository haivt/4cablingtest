<?php

class Ewave_Temando_Block_Adminhtml_Signup_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    
    protected $_blockGroup = 'temando';
    protected $_controller = 'adminhtml_signup';
    
    public function __construct()
    {
        parent::__construct();

        $this->removeButton('delete');
        if (!Mage::helper('temando')->canSignUp()) {
            $this->removeButton('reset');
            $this->removeButton('back');
            $this->removeButton('save');
        }
    }
    
    public function getHeaderText()
    {
        return Mage::helper('temando')->__('Sign Up');
    }
    
}
