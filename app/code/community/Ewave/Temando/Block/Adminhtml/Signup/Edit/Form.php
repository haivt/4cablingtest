<?php

class Ewave_Temando_Block_Adminhtml_Signup_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data',
            )
        );

        $this->setForm($form);

        $form_data = Mage::registry('temando_sign_up_form_data');
        if (!Mage::helper('temando')->canSignUp()) {
            $form_data['username'] = Mage::helper('temando')->getConfigData('general/username');
            $form_data['password'] = Mage::helper('temando')->getConfigData('general/password');
        }
/*        if (!$form_data) {
            $form_data = Mage::getModel('temando/signup_form')->getClient();
        }*/

        $helper = Mage::helper('temando');
        /* @var $helper Ewave_Temando_Helper_Data */


        // LOGIN DETAILS
        $fieldset = $form->addFieldset('login', array(
            'legend'    => $helper->__('Login Details')
        ));

        $fieldset->addField('username', 'text', array(
            'name'      => 'username',
            'label'     => $helper->__('Login ID'),
            'class'     => 'validate-max-50',
            'required'  => true,
//            'value'     => $form_data['username'],
            'disabled'  => Mage::helper('temando')->canSignUp()?false:true,
        ));
        $fieldset->addField('password', 'password', array(
            'name'      => 'password',
            'label'     => $helper->__('Password'),
            'class'     => 'required-entry validate-min-6 validate-max-20',
            'required'  => true,
//            'value'     => $form_data['password'],
            'disabled'  => Mage::helper('temando')->canSignUp()?false:true,
        ));
        if (Mage::helper('temando')->canSignUp()) {
            $fieldset->addField('password_confirm', 'password', array(
                'name'      => 'password_confirm',
                'label'     => $helper->__('Confirm Password'),
                'class'     => 'validate-cpassword',
                'required'  => true,
    //            'value'     => $form_data['password_confirm'],
            ));
            $fieldset->addField('client_type', 'select', array(
                'name'      => 'client_type',
                'label'     => $helper->__('Type of member'),
                'class'     => 'validate-select',
                'required'  => true,
                'options'   => Mage::getModel('temando/system_config_source_client_type')
                                ->getOptions(true),
    //            'value'     => $form_data['client_type'],
            ));


            // COMPANY DETAILS
            $fieldset = $form->addFieldset('fieldset_client_type_' . strtolower(Ewave_Temando_Model_System_Config_Source_Client_Type::COMPANY), array(
                'legend'    => $helper->__('Company Details'),
                'class'     => 'fieldset-client-type',
            ));

            $fieldset->addField('company_name', 'text', array(
                'name'      => 'company_name',
                'label'     => $helper->__('Company Name'),
                'class'     => 'validate-max-100',
    //            'value'     => $form_data['company_name'],
            ));
            $fieldset->addField('company_contact_person', 'text', array(
                'name'      => 'company_contact_person',
                'label'     => $helper->__('Contact Person'),
                'class'     => 'validate-max-100',
    //            'value'     => $form_data['company_contact_person'],
            ));
            $fieldset->addField('company_no', 'text', array(
                'name'      => 'company_no',
                'label'     => $helper->__('Company No.'),
                'class'     => 'validate-max-30',
                'required'  => true,
    //            'value'     => $form_data['company_no'],
            ));


            // INDIVIDUAL DETAILS
            $fieldset = $form->addFieldset('fieldset_client_type_' . strtolower(Ewave_Temando_Model_System_Config_Source_Client_Type::INDIVIDUAL), array(
                'legend'    => $helper->__('Individual Details'),
                'class'     => 'fieldset-client-type',
            ));

            $fieldset->addField('firstname', 'text', array(
                'name'      => 'firstname',
                'label'     => $helper->__('First Name'),
                'class'     => 'validate-max-50',
    //            'value'     => $form_data['firstname'],
            ));
            $fieldset->addField('surname', 'text', array(
                'name'      => 'surname',
                'label'     => $helper->__('Surname'),
                'class'     => 'validate-max-50',
    //            'value'     => $form_data['surname'],
            ));
            $fieldset->addField('dob', 'date', array(
                'name'      => 'dob',
                'label'     => $helper->__('Date of Birth'),
                'format'    => 'yyyy-MM-dd',
                'image'     => $this->getSkinUrl('images/grid-cal.gif'),
    //            'value'     => $form_data['dob'],
            ));


            // CONTACT
            $fieldset = $form->addFieldset('fieldset_contact', array(
                'legend'    => $helper->__('Contact Details'),
            ));
            $fieldset->addField('phone1', 'text', array(
                'name'      => 'phone1',
                'label'     => $helper->__('Phone 1'),
                'class'     => 'validate-max-50',
    //            'value'     => $form_data['phone1'],
            ));
            $fieldset->addField('phone2', 'text', array(
                'name'      => 'phone2',
                'label'     => $helper->__('Phone 2'),
                'class'     => 'validate-max-50',
    //            'value'     => $form_data['phone2'],
            ));
            $fieldset->addField('email', 'text', array(
                'name'      => 'email',
                'label'     => $helper->__('Email'),
                'class'     => 'validate-max-500 validate-email',
    //            'value'     => $form_data['email'],
            ));


            // ADDRESSES
            foreach (array('Street', 'Postal') as $address_type) {
                $fieldset = $form->addFieldset('fieldset_address_' . strtolower($address_type), array(
                    'legend' => $helper->__($address_type . ' Address'),
                ));


                $fieldset->addField(strtolower($address_type) . '_address', 'text', array(
                    'name'      => strtolower($address_type) . '_address',
                    'label'     => $helper->__('Street'),
                    'class'     => 'validate-max-100',
    //                'value'     => $form_data[strtolower($address_type) . '_address'],
                ));
                $fieldset->addField(strtolower($address_type) . '_suburb', 'text', array(
                    'name'      => strtolower($address_type) . '_suburb',
                    'label'     => $helper->__('Suburb'),
                    'class'     => 'validate-max-50',
    //                'value'     => $form_data[strtolower($address_type) . '_suburb'],
                ));
    /*
                $fieldset->addField(strtolower($address_type) . '_city', 'text', array(
                    'name'      => strtolower($address_type) . '_city',
                    'label'     => $helper->__('City'),
                    'class'     => 'validate-max-50',
                    'value'     => $form_data[strtolower($address_type) . '_city'],
                ));
    */
                $fieldset->addField(strtolower($address_type) . '_state', 'select', array(
                    'name'      => strtolower($address_type) . '_state',
                    'label'     => $helper->__('State'),
                    'options'   => Mage::getModel('temando/system_config_source_regions')->getOptions(true),
    //                'value'     => $form_data[strtolower($address_type) . '_state'],
                ));
                $fieldset->addField(strtolower($address_type) . '_postcode', 'text', array(
                    'name'      => strtolower($address_type) . '_postcode',
                    'label'     => $helper->__('Postcode'),
                    'class'     => 'validate-max-50 validate-digits',
    //                'value'     => $form_data[strtolower($address_type) . '_postcode'],
                ));
            }
        } else {
            if (!is_array($form_data)) {
                $form_data = array();
            }

            $form_data['username'] = Mage::helper('temando')->getConfigData('general/username');
        }

        if ($form_data) {
            $form->setValues($form_data);
        }
        $form->setUseContainer(true);
        return parent::_prepareForm();
    }
    
}
