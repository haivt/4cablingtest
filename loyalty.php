<?php
// Include Magento application
require_once ( "app/Mage.php" );
umask(0);

// Initialize Magento
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';
$app = Mage::app($mageRunCode, $mageRunType);

$session = Mage::getSingleton("core/session", array("name" => "frontend"));
// $customer = Mage::getSingleton('customer/session', array("name" => "frontend"));
// $customer = $session->getCustomer();

// echo $session['visitor_data']['customer_id'];
// echo $customer->getId();
// print_r($session);
// echo $_POST['xrefid'];

$allowed = array("facebook_like", "twitter_share", "google_plus_sharing");
if(in_array($_POST['type'], $allowed) && isset($_POST['url']) && is_numeric($_POST['xrefid']) )
{
	
	$config_points = Mage::getStoreConfig('loyalty/loyalty/'.$type);
	$config_status = Mage::getStoreConfig('loyalty/loyalty/'.$type.'_status');
	$extra = $_POST['url'];
	
	$helper = $app->getHelper('loyalty');
	// echo $helper->awardPointsForCase($_POST['type'], $_POST['url']);
		$type = $_POST['type'];
		if($type == "facebook_like")
		{
			// get points quantity for current case
			$helper->newTransaction( $_POST['xrefid'] , 1 , 14, 10, 1, "Points for liking or sharing on facebook [".$extra."]", $extra );			
		}
		else if($type == "twitter_share")
		{
			// get points quantity for current case
			$helper->newTransaction( $_POST['xrefid'] , 1 , 14, 10, 1, "Points for sharing on twitter [".$extra."]", $extra );			
		}
		else if($type == "google_plus_sharing")
		{
			// get points quantity for current case
			$helper->newTransaction( $_POST['xrefid'] , 1 , 14, 10, 1, "Points for sharing on google plus [".$extra."]", $extra );			
		}
	
}
else
{
	if(isset($_GET['debug']) && ($_GET['debug'] == "true"))
	{
		echo $mageRunCode." / ".$mageRunType;
	}
}

// echo Mage::helper('loyalty')->awardPointsForCase("facebook_like", $url = "http://google.com/");

// echo json_encode( $_POST );
// Mage::Log("loyalty [".implode($_POST)."]", null, "loyalty.log");
?>